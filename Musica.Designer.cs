﻿namespace Comercializadora
{
    partial class Musica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lblGuitarra = new System.Windows.Forms.Label();
            this.lblTeclados = new System.Windows.Forms.Label();
            this.lblBateria = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxBateria = new System.Windows.Forms.PictureBox();
            this.pictureBoxTeclado = new System.Windows.Forms.PictureBox();
            this.pictureBoxGuitarra = new System.Windows.Forms.PictureBox();
            this.lblAudio = new System.Windows.Forms.Label();
            this.btnRegresar = new System.Windows.Forms.Button();
            this.groupBoxMenuMusica = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBateria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTeclado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGuitarra)).BeginInit();
            this.groupBoxMenuMusica.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Musica";
            // 
            // lblGuitarra
            // 
            this.lblGuitarra.AutoSize = true;
            this.lblGuitarra.Font = new System.Drawing.Font("Century Gothic", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGuitarra.Location = new System.Drawing.Point(80, 143);
            this.lblGuitarra.Name = "lblGuitarra";
            this.lblGuitarra.Size = new System.Drawing.Size(96, 15);
            this.lblGuitarra.TabIndex = 3;
            this.lblGuitarra.Text = "Guitarras y Bajos";
            // 
            // lblTeclados
            // 
            this.lblTeclados.AutoSize = true;
            this.lblTeclados.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblTeclados.Font = new System.Drawing.Font("Century Gothic", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTeclados.Location = new System.Drawing.Point(325, 143);
            this.lblTeclados.Name = "lblTeclados";
            this.lblTeclados.Size = new System.Drawing.Size(55, 15);
            this.lblTeclados.TabIndex = 4;
            this.lblTeclados.Text = "Teclados";
            // 
            // lblBateria
            // 
            this.lblBateria.AutoSize = true;
            this.lblBateria.Font = new System.Drawing.Font("Century Gothic", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBateria.Location = new System.Drawing.Point(70, 287);
            this.lblBateria.Name = "lblBateria";
            this.lblBateria.Size = new System.Drawing.Size(124, 15);
            this.lblBateria.TabIndex = 7;
            this.lblBateria.Text = "Bateria y Percusiones ";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources.audi;
            this.pictureBox1.Location = new System.Drawing.Point(300, 181);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 103);
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBoxBateria
            // 
            this.pictureBoxBateria.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxBateria.Image = global::Comercializadora.Properties.Resources.dru;
            this.pictureBoxBateria.Location = new System.Drawing.Point(77, 181);
            this.pictureBoxBateria.Name = "pictureBoxBateria";
            this.pictureBoxBateria.Size = new System.Drawing.Size(103, 103);
            this.pictureBoxBateria.TabIndex = 6;
            this.pictureBoxBateria.TabStop = false;
            this.pictureBoxBateria.Click += new System.EventHandler(this.pictureBoxBateria_Click);
            // 
            // pictureBoxTeclado
            // 
            this.pictureBoxTeclado.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxTeclado.Image = global::Comercializadora.Properties.Resources.pi;
            this.pictureBoxTeclado.Location = new System.Drawing.Point(300, 37);
            this.pictureBoxTeclado.Name = "pictureBoxTeclado";
            this.pictureBoxTeclado.Size = new System.Drawing.Size(101, 103);
            this.pictureBoxTeclado.TabIndex = 5;
            this.pictureBoxTeclado.TabStop = false;
            this.pictureBoxTeclado.Click += new System.EventHandler(this.pictureBoxTeclado_Click);
            // 
            // pictureBoxGuitarra
            // 
            this.pictureBoxGuitarra.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxGuitarra.Image = global::Comercializadora.Properties.Resources.gui;
            this.pictureBoxGuitarra.ImageLocation = "";
            this.pictureBoxGuitarra.InitialImage = null;
            this.pictureBoxGuitarra.Location = new System.Drawing.Point(77, 37);
            this.pictureBoxGuitarra.Name = "pictureBoxGuitarra";
            this.pictureBoxGuitarra.Size = new System.Drawing.Size(104, 103);
            this.pictureBoxGuitarra.TabIndex = 2;
            this.pictureBoxGuitarra.TabStop = false;
            this.pictureBoxGuitarra.Click += new System.EventHandler(this.ImagenBoxGuitarra_Click);
            // 
            // lblAudio
            // 
            this.lblAudio.AutoSize = true;
            this.lblAudio.Font = new System.Drawing.Font("Century Gothic", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAudio.Location = new System.Drawing.Point(330, 287);
            this.lblAudio.Name = "lblAudio";
            this.lblAudio.Size = new System.Drawing.Size(39, 15);
            this.lblAudio.TabIndex = 9;
            this.lblAudio.Text = "Audio";
            // 
            // btnRegresar
            // 
            this.btnRegresar.Font = new System.Drawing.Font("Microsoft New Tai Lue", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegresar.Location = new System.Drawing.Point(204, 322);
            this.btnRegresar.Name = "btnRegresar";
            this.btnRegresar.Size = new System.Drawing.Size(75, 23);
            this.btnRegresar.TabIndex = 10;
            this.btnRegresar.Text = "Regresar";
            this.btnRegresar.UseVisualStyleBackColor = true;
            this.btnRegresar.Click += new System.EventHandler(this.btnRegresar_Click);
            // 
            // groupBoxMenuMusica
            // 
            this.groupBoxMenuMusica.Controls.Add(this.pictureBoxGuitarra);
            this.groupBoxMenuMusica.Controls.Add(this.btnRegresar);
            this.groupBoxMenuMusica.Controls.Add(this.lblGuitarra);
            this.groupBoxMenuMusica.Controls.Add(this.lblAudio);
            this.groupBoxMenuMusica.Controls.Add(this.pictureBoxTeclado);
            this.groupBoxMenuMusica.Controls.Add(this.lblBateria);
            this.groupBoxMenuMusica.Controls.Add(this.pictureBox1);
            this.groupBoxMenuMusica.Controls.Add(this.pictureBoxBateria);
            this.groupBoxMenuMusica.Controls.Add(this.lblTeclados);
            this.groupBoxMenuMusica.Font = new System.Drawing.Font("Century Gothic", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxMenuMusica.Location = new System.Drawing.Point(12, 37);
            this.groupBoxMenuMusica.Name = "groupBoxMenuMusica";
            this.groupBoxMenuMusica.Size = new System.Drawing.Size(476, 351);
            this.groupBoxMenuMusica.TabIndex = 11;
            this.groupBoxMenuMusica.TabStop = false;
            this.groupBoxMenuMusica.Text = "Menu";
            // 
            // Musica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 400);
            this.Controls.Add(this.groupBoxMenuMusica);
            this.Controls.Add(this.label1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Musica";
            this.Text = "Musica";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBateria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTeclado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGuitarra)).EndInit();
            this.groupBoxMenuMusica.ResumeLayout(false);
            this.groupBoxMenuMusica.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBoxGuitarra;
        private System.Windows.Forms.Label lblGuitarra;
        private System.Windows.Forms.Label lblTeclados;
        private System.Windows.Forms.PictureBox pictureBoxTeclado;
        private System.Windows.Forms.PictureBox pictureBoxBateria;
        private System.Windows.Forms.Label lblBateria;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblAudio;
        private System.Windows.Forms.Button btnRegresar;
        private System.Windows.Forms.GroupBox groupBoxMenuMusica;
    }
}