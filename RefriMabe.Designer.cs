﻿namespace Comercializadora
{
    partial class RefriMabe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RefriMabe));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.ComprarRefriMabe3 = new System.Windows.Forms.Button();
            this.AgregarRefriMabe3 = new System.Windows.Forms.Button();
            this.ComprarRefriMabe2 = new System.Windows.Forms.Button();
            this.AgregarRefriMabe2 = new System.Windows.Forms.Button();
            this.ComprarRefriMabe1 = new System.Windows.Forms.Button();
            this.AgregarRefriMabe1 = new System.Windows.Forms.Button();
            this.BtRegresarMabe = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources.Mabe;
            this.pictureBox1.Location = new System.Drawing.Point(300, 22);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(231, 66);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Comercializadora.Properties.Resources.RefriMabe;
            this.pictureBox2.Location = new System.Drawing.Point(12, 127);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(112, 175);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Comercializadora.Properties.Resources.Refrimabe2;
            this.pictureBox3.Location = new System.Drawing.Point(12, 321);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(112, 172);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Comercializadora.Properties.Resources.refrimabe3;
            this.pictureBox4.Location = new System.Drawing.Point(12, 516);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(112, 190);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(186, 127);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(563, 22);
            this.label1.TabIndex = 4;
            this.label1.Text = "Mabe - Refrigerador Top Mount 14\" RME1436ZMFP0 - Negro";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(186, 516);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(416, 44);
            this.label2.TabIndex = 5;
            this.label2.Text = "Refrigerador Whirlpool 17 pies cúbicos gris \r\nacero WT1756A";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(187, 572);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 15);
            this.label6.TabIndex = 12;
            this.label6.Text = "Marca: MABE";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(187, 597);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 15);
            this.label3.TabIndex = 13;
            this.label3.Text = "$12,495.00";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(187, 621);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(470, 52);
            this.label4.TabIndex = 14;
            this.label4.Text = resources.GetString("label4.Text");
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(186, 321);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(300, 22);
            this.label5.TabIndex = 15;
            this.label5.Text = "REFRIGERADOR MABE 14 PIES";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(187, 355);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 15);
            this.label7.TabIndex = 16;
            this.label7.Text = "Marca: MABE";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(187, 379);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 15);
            this.label8.TabIndex = 17;
            this.label8.Text = "$ 8,999.00";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(190, 403);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(485, 52);
            this.label9.TabIndex = 18;
            this.label9.Text = resources.GetString("label9.Text");
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(190, 163);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(82, 15);
            this.label10.TabIndex = 19;
            this.label10.Text = "Marca: MABE";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(187, 192);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(69, 15);
            this.label11.TabIndex = 20;
            this.label11.Text = "$10,299.00";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(193, 222);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(568, 52);
            this.label12.TabIndex = 21;
            this.label12.Text = resources.GetString("label12.Text");
            // 
            // ComprarRefriMabe3
            // 
            this.ComprarRefriMabe3.Location = new System.Drawing.Point(190, 683);
            this.ComprarRefriMabe3.Name = "ComprarRefriMabe3";
            this.ComprarRefriMabe3.Size = new System.Drawing.Size(102, 23);
            this.ComprarRefriMabe3.TabIndex = 22;
            this.ComprarRefriMabe3.Text = "Comprar";
            this.ComprarRefriMabe3.UseVisualStyleBackColor = true;
            // 
            // AgregarRefriMabe3
            // 
            this.AgregarRefriMabe3.Location = new System.Drawing.Point(310, 683);
            this.AgregarRefriMabe3.Name = "AgregarRefriMabe3";
            this.AgregarRefriMabe3.Size = new System.Drawing.Size(102, 23);
            this.AgregarRefriMabe3.TabIndex = 23;
            this.AgregarRefriMabe3.Text = "Agregar al carrito";
            this.AgregarRefriMabe3.UseVisualStyleBackColor = true;
            // 
            // ComprarRefriMabe2
            // 
            this.ComprarRefriMabe2.Location = new System.Drawing.Point(196, 469);
            this.ComprarRefriMabe2.Name = "ComprarRefriMabe2";
            this.ComprarRefriMabe2.Size = new System.Drawing.Size(96, 23);
            this.ComprarRefriMabe2.TabIndex = 24;
            this.ComprarRefriMabe2.Text = "Comprar";
            this.ComprarRefriMabe2.UseVisualStyleBackColor = true;
            // 
            // AgregarRefriMabe2
            // 
            this.AgregarRefriMabe2.Location = new System.Drawing.Point(310, 467);
            this.AgregarRefriMabe2.Name = "AgregarRefriMabe2";
            this.AgregarRefriMabe2.Size = new System.Drawing.Size(102, 23);
            this.AgregarRefriMabe2.TabIndex = 25;
            this.AgregarRefriMabe2.Text = "Agregar al carrito";
            this.AgregarRefriMabe2.UseVisualStyleBackColor = true;
            // 
            // ComprarRefriMabe1
            // 
            this.ComprarRefriMabe1.Location = new System.Drawing.Point(196, 279);
            this.ComprarRefriMabe1.Name = "ComprarRefriMabe1";
            this.ComprarRefriMabe1.Size = new System.Drawing.Size(96, 23);
            this.ComprarRefriMabe1.TabIndex = 26;
            this.ComprarRefriMabe1.Text = "Comprar";
            this.ComprarRefriMabe1.UseVisualStyleBackColor = true;
            // 
            // AgregarRefriMabe1
            // 
            this.AgregarRefriMabe1.Location = new System.Drawing.Point(310, 279);
            this.AgregarRefriMabe1.Name = "AgregarRefriMabe1";
            this.AgregarRefriMabe1.Size = new System.Drawing.Size(102, 23);
            this.AgregarRefriMabe1.TabIndex = 27;
            this.AgregarRefriMabe1.Text = "Agregar al carrito";
            this.AgregarRefriMabe1.UseVisualStyleBackColor = true;
            // 
            // BtRegresarMabe
            // 
            this.BtRegresarMabe.Image = global::Comercializadora.Properties.Resources.regresar3;
            this.BtRegresarMabe.Location = new System.Drawing.Point(-2, -4);
            this.BtRegresarMabe.Name = "BtRegresarMabe";
            this.BtRegresarMabe.Size = new System.Drawing.Size(107, 82);
            this.BtRegresarMabe.TabIndex = 142;
            this.BtRegresarMabe.UseVisualStyleBackColor = true;
            this.BtRegresarMabe.Click += new System.EventHandler(this.BtRegresarMabe_Click);
            // 
            // RefriMabe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(798, 752);
            this.Controls.Add(this.BtRegresarMabe);
            this.Controls.Add(this.AgregarRefriMabe1);
            this.Controls.Add(this.ComprarRefriMabe1);
            this.Controls.Add(this.AgregarRefriMabe2);
            this.Controls.Add(this.ComprarRefriMabe2);
            this.Controls.Add(this.AgregarRefriMabe3);
            this.Controls.Add(this.ComprarRefriMabe3);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "RefriMabe";
            this.Text = "RefriMabe";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button ComprarRefriMabe3;
        private System.Windows.Forms.Button AgregarRefriMabe3;
        private System.Windows.Forms.Button ComprarRefriMabe2;
        private System.Windows.Forms.Button AgregarRefriMabe2;
        private System.Windows.Forms.Button ComprarRefriMabe1;
        private System.Windows.Forms.Button AgregarRefriMabe1;
        private System.Windows.Forms.Button BtRegresarMabe;
    }
}