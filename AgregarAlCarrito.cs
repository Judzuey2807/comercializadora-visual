﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comercializadora
{
    public partial class AgregarAlCarrito : Form
    {
        public AgregarAlCarrito()
        {
            InitializeComponent();
        }

        public void ListAgregar_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void BtRegresarMabe_Click(object sender, EventArgs e)
        {
            VerTodosLosProductos btRegresar = new VerTodosLosProductos();
            VerTodosLosProductos ver = btRegresar;
            ver.Show();
            this.Dispose();
        }

        private void CarritoBorrar_Click(object sender, EventArgs e)
        {
            ListAgregar.Items.RemoveAt(ListAgregar.SelectedIndex);
        }
    }
}
