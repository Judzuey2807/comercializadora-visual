﻿namespace Comercializadora
{
    partial class AireMabe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AgregarAireMabe1 = new System.Windows.Forms.Button();
            this.ComprarAireMabe1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.AgregarAireMabe2 = new System.Windows.Forms.Button();
            this.ComprarAireMabe2 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.AgregarAireMabe3 = new System.Windows.Forms.Button();
            this.ComprarAireMabe3 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.BtRegresarMabe = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // AgregarAireMabe1
            // 
            this.AgregarAireMabe1.Location = new System.Drawing.Point(432, 273);
            this.AgregarAireMabe1.Name = "AgregarAireMabe1";
            this.AgregarAireMabe1.Size = new System.Drawing.Size(105, 23);
            this.AgregarAireMabe1.TabIndex = 125;
            this.AgregarAireMabe1.Text = "Agregar al carrito";
            this.AgregarAireMabe1.UseVisualStyleBackColor = true;
            // 
            // ComprarAireMabe1
            // 
            this.ComprarAireMabe1.Location = new System.Drawing.Point(305, 273);
            this.ComprarAireMabe1.Name = "ComprarAireMabe1";
            this.ComprarAireMabe1.Size = new System.Drawing.Size(105, 23);
            this.ComprarAireMabe1.TabIndex = 124;
            this.ComprarAireMabe1.Text = "Comprar";
            this.ComprarAireMabe1.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(302, 213);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(306, 39);
            this.label4.TabIndex = 123;
            this.label4.Text = "Recubrimiento Blue Fin, protege las tuberías contra la corrosión\r\nFiltro Silver I" +
    "on que elimina bacterias\r\nCon Gas Ecológico R410A para ahorrar energía";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(302, 189);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 15);
            this.label3.TabIndex = 122;
            this.label3.Text = "$ 6,590.00";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(302, 165);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 15);
            this.label2.TabIndex = 121;
            this.label2.Text = "Marca: MABE";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(301, 143);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(369, 22);
            this.label1.TabIndex = 120;
            this.label1.Text = "Mini Split Mabe Frío y Calor 12000 BTU";
            // 
            // AgregarAireMabe2
            // 
            this.AgregarAireMabe2.Location = new System.Drawing.Point(432, 455);
            this.AgregarAireMabe2.Name = "AgregarAireMabe2";
            this.AgregarAireMabe2.Size = new System.Drawing.Size(105, 23);
            this.AgregarAireMabe2.TabIndex = 131;
            this.AgregarAireMabe2.Text = "Agregar al carrito";
            this.AgregarAireMabe2.UseVisualStyleBackColor = true;
            // 
            // ComprarAireMabe2
            // 
            this.ComprarAireMabe2.Location = new System.Drawing.Point(305, 455);
            this.ComprarAireMabe2.Name = "ComprarAireMabe2";
            this.ComprarAireMabe2.Size = new System.Drawing.Size(105, 23);
            this.ComprarAireMabe2.TabIndex = 130;
            this.ComprarAireMabe2.Text = "Comprar";
            this.ComprarAireMabe2.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(302, 395);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(291, 39);
            this.label5.TabIndex = 129;
            this.label5.Text = "Con tecnología Wi-Fi para encenderlo desde cualquier lugar\r\nTemperaturas de 16 a " +
    "30 ° C, 220 V / 60 Hz\r\nColor plata oscura";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(302, 371);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 15);
            this.label6.TabIndex = 128;
            this.label6.Text = "$ 10,990.00";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(302, 347);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(82, 15);
            this.label7.TabIndex = 127;
            this.label7.Text = "Marca: MABE";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(301, 325);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(443, 22);
            this.label8.TabIndex = 126;
            this.label8.Text = "Mini Split Mabe Inverter Solo Frío 18,000 BTU\'s";
            // 
            // AgregarAireMabe3
            // 
            this.AgregarAireMabe3.Location = new System.Drawing.Point(432, 710);
            this.AgregarAireMabe3.Name = "AgregarAireMabe3";
            this.AgregarAireMabe3.Size = new System.Drawing.Size(105, 23);
            this.AgregarAireMabe3.TabIndex = 137;
            this.AgregarAireMabe3.Text = "Agregar al carrito";
            this.AgregarAireMabe3.UseVisualStyleBackColor = true;
            // 
            // ComprarAireMabe3
            // 
            this.ComprarAireMabe3.Location = new System.Drawing.Point(305, 710);
            this.ComprarAireMabe3.Name = "ComprarAireMabe3";
            this.ComprarAireMabe3.Size = new System.Drawing.Size(105, 23);
            this.ComprarAireMabe3.TabIndex = 136;
            this.ComprarAireMabe3.Text = "Comprar";
            this.ComprarAireMabe3.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(302, 576);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(224, 117);
            this.label9.TabIndex = 135;
            this.label9.Text = "Tipo de clima, solo frío\r\nCapacidad de 12,000 BTU\r\n220 voltios\r\nTemperatura desde" +
    " 16-30 grados centígrados\r\nTubería de cobre\r\nFiltro de alta densidad\r\nIon de pla" +
    "ta\r\nBio filtro\r\nDisplay con luz LE";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(302, 552);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 15);
            this.label10.TabIndex = 134;
            this.label10.Text = "$ 12,290.00";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(302, 528);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 15);
            this.label11.TabIndex = 133;
            this.label11.Text = "Marca: MABE";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label12.Location = new System.Drawing.Point(301, 506);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(433, 22);
            this.label12.TabIndex = 132;
            this.label12.Text = "Inversor Mini Split Mabe Solo Frío 12,000 BTU";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // BtRegresarMabe
            // 
            this.BtRegresarMabe.Image = global::Comercializadora.Properties.Resources.regresar3;
            this.BtRegresarMabe.Location = new System.Drawing.Point(1, 1);
            this.BtRegresarMabe.Name = "BtRegresarMabe";
            this.BtRegresarMabe.Size = new System.Drawing.Size(107, 82);
            this.BtRegresarMabe.TabIndex = 138;
            this.BtRegresarMabe.UseVisualStyleBackColor = true;
            this.BtRegresarMabe.Click += new System.EventHandler(this.BtRegresarMabe_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Comercializadora.Properties.Resources.AireMabe3;
            this.pictureBox4.Location = new System.Drawing.Point(27, 552);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(225, 115);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Comercializadora.Properties.Resources.AireMabe2;
            this.pictureBox3.Location = new System.Drawing.Point(27, 348);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(225, 115);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Comercializadora.Properties.Resources.AireMabe1;
            this.pictureBox2.Location = new System.Drawing.Point(27, 165);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(225, 115);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources.Mabe;
            this.pictureBox1.Location = new System.Drawing.Point(305, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(216, 95);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // AireMabe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(841, 735);
            this.Controls.Add(this.BtRegresarMabe);
            this.Controls.Add(this.AgregarAireMabe3);
            this.Controls.Add(this.ComprarAireMabe3);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.AgregarAireMabe2);
            this.Controls.Add(this.ComprarAireMabe2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.AgregarAireMabe1);
            this.Controls.Add(this.ComprarAireMabe1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "AireMabe";
            this.Text = "AireMabe";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Button AgregarAireMabe1;
        private System.Windows.Forms.Button ComprarAireMabe1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button AgregarAireMabe2;
        private System.Windows.Forms.Button ComprarAireMabe2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button AgregarAireMabe3;
        private System.Windows.Forms.Button ComprarAireMabe3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button BtRegresarMabe;
    }
}