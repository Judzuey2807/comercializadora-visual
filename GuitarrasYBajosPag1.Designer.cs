﻿namespace Comercializadora
{
    partial class GuitarrasYBajosPag1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDescripcionFender = new System.Windows.Forms.Label();
            this.lblPrecioFender = new System.Windows.Forms.Label();
            this.btnAgregarFender = new System.Windows.Forms.Button();
            this.comboBoxFender = new System.Windows.Forms.ComboBox();
            this.lblGibson = new System.Windows.Forms.Label();
            this.lblPrecioGibson = new System.Windows.Forms.Label();
            this.comboBoxGibson = new System.Windows.Forms.ComboBox();
            this.btnAgregarGibson = new System.Windows.Forms.Button();
            this.lblGibAcu = new System.Windows.Forms.Label();
            this.lblPrecioGibAcu = new System.Windows.Forms.Label();
            this.comboBoxGibAcu = new System.Windows.Forms.ComboBox();
            this.btnGibAcu = new System.Windows.Forms.Button();
            this.btnPag2 = new System.Windows.Forms.Button();
            this.btnPag1 = new System.Windows.Forms.Label();
            this.btnPag3 = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxFender = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFender)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDescripcionFender
            // 
            this.lblDescripcionFender.AutoSize = true;
            this.lblDescripcionFender.Location = new System.Drawing.Point(168, 79);
            this.lblDescripcionFender.Name = "lblDescripcionFender";
            this.lblDescripcionFender.Size = new System.Drawing.Size(409, 13);
            this.lblDescripcionFender.TabIndex = 5;
            this.lblDescripcionFender.Text = "Guitarra Eléctrica Fender Rarities Flame Koa Top Stratocaster®, Maple Neck, Natur" +
    "al";
            // 
            // lblPrecioFender
            // 
            this.lblPrecioFender.AutoSize = true;
            this.lblPrecioFender.Location = new System.Drawing.Point(168, 104);
            this.lblPrecioFender.Name = "lblPrecioFender";
            this.lblPrecioFender.Size = new System.Drawing.Size(46, 13);
            this.lblPrecioFender.TabIndex = 6;
            this.lblPrecioFender.Text = "$50,840";
            // 
            // btnAgregarFender
            // 
            this.btnAgregarFender.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAgregarFender.Location = new System.Drawing.Point(673, 92);
            this.btnAgregarFender.Name = "btnAgregarFender";
            this.btnAgregarFender.Size = new System.Drawing.Size(75, 23);
            this.btnAgregarFender.TabIndex = 7;
            this.btnAgregarFender.Text = "Agregar";
            this.btnAgregarFender.UseVisualStyleBackColor = true;
            // 
            // comboBoxFender
            // 
            this.comboBoxFender.FormattingEnabled = true;
            this.comboBoxFender.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxFender.Location = new System.Drawing.Point(611, 94);
            this.comboBoxFender.Name = "comboBoxFender";
            this.comboBoxFender.Size = new System.Drawing.Size(44, 21);
            this.comboBoxFender.TabIndex = 8;
            // 
            // lblGibson
            // 
            this.lblGibson.AutoSize = true;
            this.lblGibson.Location = new System.Drawing.Point(168, 188);
            this.lblGibson.Name = "lblGibson";
            this.lblGibson.Size = new System.Drawing.Size(272, 13);
            this.lblGibson.TabIndex = 9;
            this.lblGibson.Text = "Guitarra Eléctrica Gibson Les Paul Modern Graphite Top";
            // 
            // lblPrecioGibson
            // 
            this.lblPrecioGibson.AutoSize = true;
            this.lblPrecioGibson.Location = new System.Drawing.Point(168, 214);
            this.lblPrecioGibson.Name = "lblPrecioGibson";
            this.lblPrecioGibson.Size = new System.Drawing.Size(46, 13);
            this.lblPrecioGibson.TabIndex = 10;
            this.lblPrecioGibson.Text = "$57,073";
            // 
            // comboBoxGibson
            // 
            this.comboBoxGibson.FormattingEnabled = true;
            this.comboBoxGibson.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxGibson.Location = new System.Drawing.Point(611, 200);
            this.comboBoxGibson.Name = "comboBoxGibson";
            this.comboBoxGibson.Size = new System.Drawing.Size(44, 21);
            this.comboBoxGibson.TabIndex = 11;
            // 
            // btnAgregarGibson
            // 
            this.btnAgregarGibson.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAgregarGibson.Location = new System.Drawing.Point(673, 198);
            this.btnAgregarGibson.Name = "btnAgregarGibson";
            this.btnAgregarGibson.Size = new System.Drawing.Size(75, 23);
            this.btnAgregarGibson.TabIndex = 12;
            this.btnAgregarGibson.Text = "Agregar";
            this.btnAgregarGibson.UseVisualStyleBackColor = true;
            // 
            // lblGibAcu
            // 
            this.lblGibAcu.AutoSize = true;
            this.lblGibAcu.Location = new System.Drawing.Point(168, 297);
            this.lblGibAcu.Name = "lblGibAcu";
            this.lblGibAcu.Size = new System.Drawing.Size(207, 13);
            this.lblGibAcu.TabIndex = 13;
            this.lblGibAcu.Text = "Guitarra Acústica Gibson J-45 Studio Burst";
            // 
            // lblPrecioGibAcu
            // 
            this.lblPrecioGibAcu.AutoSize = true;
            this.lblPrecioGibAcu.Location = new System.Drawing.Point(168, 329);
            this.lblPrecioGibAcu.Name = "lblPrecioGibAcu";
            this.lblPrecioGibAcu.Size = new System.Drawing.Size(46, 13);
            this.lblPrecioGibAcu.TabIndex = 14;
            this.lblPrecioGibAcu.Text = "$33,822";
            // 
            // comboBoxGibAcu
            // 
            this.comboBoxGibAcu.FormattingEnabled = true;
            this.comboBoxGibAcu.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxGibAcu.Location = new System.Drawing.Point(611, 310);
            this.comboBoxGibAcu.Name = "comboBoxGibAcu";
            this.comboBoxGibAcu.Size = new System.Drawing.Size(44, 21);
            this.comboBoxGibAcu.TabIndex = 15;
            // 
            // btnGibAcu
            // 
            this.btnGibAcu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGibAcu.Location = new System.Drawing.Point(673, 308);
            this.btnGibAcu.Name = "btnGibAcu";
            this.btnGibAcu.Size = new System.Drawing.Size(75, 23);
            this.btnGibAcu.TabIndex = 16;
            this.btnGibAcu.Text = "Agregar";
            this.btnGibAcu.UseVisualStyleBackColor = true;
            // 
            // btnPag2
            // 
            this.btnPag2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnPag2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPag2.Location = new System.Drawing.Point(392, 412);
            this.btnPag2.Name = "btnPag2";
            this.btnPag2.Size = new System.Drawing.Size(27, 23);
            this.btnPag2.TabIndex = 17;
            this.btnPag2.Text = "2";
            this.btnPag2.UseVisualStyleBackColor = true;
            this.btnPag2.Click += new System.EventHandler(this.btnPag2_Click);
            // 
            // btnPag1
            // 
            this.btnPag1.AutoSize = true;
            this.btnPag1.Location = new System.Drawing.Point(370, 417);
            this.btnPag1.Name = "btnPag1";
            this.btnPag1.Size = new System.Drawing.Size(13, 13);
            this.btnPag1.TabIndex = 18;
            this.btnPag1.Text = "1";
            this.btnPag1.Click += new System.EventHandler(this.btnPag1_Click);
            // 
            // btnPag3
            // 
            this.btnPag3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPag3.Location = new System.Drawing.Point(425, 412);
            this.btnPag3.Name = "btnPag3";
            this.btnPag3.Size = new System.Drawing.Size(27, 23);
            this.btnPag3.TabIndex = 19;
            this.btnPag3.Text = "3";
            this.btnPag3.UseVisualStyleBackColor = true;
            this.btnPag3.Click += new System.EventHandler(this.btnPag3_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Comercializadora.Properties.Resources.Gibson;
            this.pictureBox4.Location = new System.Drawing.Point(30, 152);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(100, 102);
            this.pictureBox4.TabIndex = 4;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources.GibAcustica1;
            this.pictureBox1.Location = new System.Drawing.Point(30, 260);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 102);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBoxFender
            // 
            this.pictureBoxFender.Image = global::Comercializadora.Properties.Resources.Fender1;
            this.pictureBoxFender.Location = new System.Drawing.Point(30, 44);
            this.pictureBoxFender.Name = "pictureBoxFender";
            this.pictureBoxFender.Size = new System.Drawing.Size(100, 102);
            this.pictureBoxFender.TabIndex = 3;
            this.pictureBoxFender.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Image = global::Comercializadora.Properties.Resources._2569f;
            this.pictureBox2.Location = new System.Drawing.Point(721, 403);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(27, 27);
            this.pictureBox2.TabIndex = 20;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // GuitarrasYBajosPag1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 465);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.btnPag3);
            this.Controls.Add(this.btnPag1);
            this.Controls.Add(this.btnPag2);
            this.Controls.Add(this.btnGibAcu);
            this.Controls.Add(this.comboBoxGibAcu);
            this.Controls.Add(this.lblPrecioGibAcu);
            this.Controls.Add(this.lblGibAcu);
            this.Controls.Add(this.btnAgregarGibson);
            this.Controls.Add(this.comboBoxGibson);
            this.Controls.Add(this.lblPrecioGibson);
            this.Controls.Add(this.lblGibson);
            this.Controls.Add(this.comboBoxFender);
            this.Controls.Add(this.btnAgregarFender);
            this.Controls.Add(this.lblPrecioFender);
            this.Controls.Add(this.lblDescripcionFender);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBoxFender);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "GuitarrasYBajosPag1";
            this.Text = "Guitarras y Bajos";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFender)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxFender;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblDescripcionFender;
        private System.Windows.Forms.Label lblPrecioFender;
        private System.Windows.Forms.Button btnAgregarFender;
        private System.Windows.Forms.ComboBox comboBoxFender;
        private System.Windows.Forms.Label lblGibson;
        private System.Windows.Forms.Label lblPrecioGibson;
        private System.Windows.Forms.ComboBox comboBoxGibson;
        private System.Windows.Forms.Button btnAgregarGibson;
        private System.Windows.Forms.Label lblGibAcu;
        private System.Windows.Forms.Label lblPrecioGibAcu;
        private System.Windows.Forms.ComboBox comboBoxGibAcu;
        private System.Windows.Forms.Button btnGibAcu;
        private System.Windows.Forms.Button btnPag2;
        private System.Windows.Forms.Label btnPag1;
        private System.Windows.Forms.Button btnPag3;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}