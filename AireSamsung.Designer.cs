﻿namespace Comercializadora
{
    partial class AireSamsung
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AireSamsung));
            this.ComprarAireSamsung3 = new System.Windows.Forms.Button();
            this.AgregarAireSamsung3 = new System.Windows.Forms.Button();
            this.AgregarAireSamsung2 = new System.Windows.Forms.Button();
            this.ComprarAireSamsung2 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.AgregarAireSamsung1 = new System.Windows.Forms.Button();
            this.ComprarAireSamsung1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BtRegresarMabe = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // ComprarAireSamsung3
            // 
            this.ComprarAireSamsung3.Location = new System.Drawing.Point(241, 707);
            this.ComprarAireSamsung3.Name = "ComprarAireSamsung3";
            this.ComprarAireSamsung3.Size = new System.Drawing.Size(105, 23);
            this.ComprarAireSamsung3.TabIndex = 95;
            this.ComprarAireSamsung3.Text = "Comprar";
            this.ComprarAireSamsung3.UseVisualStyleBackColor = true;
            // 
            // AgregarAireSamsung3
            // 
            this.AgregarAireSamsung3.Location = new System.Drawing.Point(370, 707);
            this.AgregarAireSamsung3.Name = "AgregarAireSamsung3";
            this.AgregarAireSamsung3.Size = new System.Drawing.Size(105, 23);
            this.AgregarAireSamsung3.TabIndex = 94;
            this.AgregarAireSamsung3.Text = "Agregar al carrito";
            this.AgregarAireSamsung3.UseVisualStyleBackColor = true;
            // 
            // AgregarAireSamsung2
            // 
            this.AgregarAireSamsung2.Location = new System.Drawing.Point(370, 489);
            this.AgregarAireSamsung2.Name = "AgregarAireSamsung2";
            this.AgregarAireSamsung2.Size = new System.Drawing.Size(105, 23);
            this.AgregarAireSamsung2.TabIndex = 93;
            this.AgregarAireSamsung2.Text = "Agregar al carrito";
            this.AgregarAireSamsung2.UseVisualStyleBackColor = true;
            // 
            // ComprarAireSamsung2
            // 
            this.ComprarAireSamsung2.Location = new System.Drawing.Point(241, 489);
            this.ComprarAireSamsung2.Name = "ComprarAireSamsung2";
            this.ComprarAireSamsung2.Size = new System.Drawing.Size(105, 23);
            this.ComprarAireSamsung2.TabIndex = 92;
            this.ComprarAireSamsung2.Text = "Comprar";
            this.ComprarAireSamsung2.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(239, 606);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(383, 78);
            this.label12.TabIndex = 91;
            this.label12.Text = resources.GetString("label12.Text");
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(239, 584);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 15);
            this.label11.TabIndex = 90;
            this.label11.Text = "$ 13,691.30";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(239, 554);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(107, 15);
            this.label10.TabIndex = 89;
            this.label10.Text = "Marca: SAMSUNG";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(238, 532);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(555, 22);
            this.label9.TabIndex = 88;
            this.label9.Text = "AIRE ACONDICIONADO MINISPLIT 18000 BTU\'S PLATEADO";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(239, 415);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(270, 39);
            this.label8.TabIndex = 87;
            this.label8.Text = "Enfría hasta un 38% más rápido que un mini split común\r\nTecnología de iones S-Pla" +
    "sma\r\n70% menos ruido";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(239, 396);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 15);
            this.label7.TabIndex = 86;
            this.label7.Text = "$ 15,990.00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(239, 367);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 15);
            this.label6.TabIndex = 85;
            this.label6.Text = "Marca: SAMSUNG";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label5.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(244, 345);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(488, 22);
            this.label5.TabIndex = 84;
            this.label5.Text = "Mini Split Samsung Inverter Frío y Calor 12,000 BTU";
            // 
            // AgregarAireSamsung1
            // 
            this.AgregarAireSamsung1.Location = new System.Drawing.Point(370, 292);
            this.AgregarAireSamsung1.Name = "AgregarAireSamsung1";
            this.AgregarAireSamsung1.Size = new System.Drawing.Size(105, 23);
            this.AgregarAireSamsung1.TabIndex = 83;
            this.AgregarAireSamsung1.Text = "Agregar al carrito";
            this.AgregarAireSamsung1.UseVisualStyleBackColor = true;
            // 
            // ComprarAireSamsung1
            // 
            this.ComprarAireSamsung1.Location = new System.Drawing.Point(241, 292);
            this.ComprarAireSamsung1.Name = "ComprarAireSamsung1";
            this.ComprarAireSamsung1.Size = new System.Drawing.Size(105, 23);
            this.ComprarAireSamsung1.TabIndex = 82;
            this.ComprarAireSamsung1.Text = "Comprar";
            this.ComprarAireSamsung1.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(238, 225);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 52);
            this.label4.TabIndex = 81;
            this.label4.Text = "220 V\r\n1 tonelada\r\n12,000 BTUS\r\n16 SEER\r\n";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(238, 200);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 15);
            this.label3.TabIndex = 80;
            this.label3.Text = "$ 8,999.00";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(239, 173);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 15);
            this.label2.TabIndex = 79;
            this.label2.Text = "Marca: SAMSUNG";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(237, 151);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(554, 22);
            this.label1.TabIndex = 78;
            this.label1.Text = "Aire Acondicionado Samsung AR12MVFHEWK Inverter Frio";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Comercializadora.Properties.Resources.AireSamsung3;
            this.pictureBox4.Location = new System.Drawing.Point(22, 572);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(208, 100);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox4.TabIndex = 97;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Comercializadora.Properties.Resources.AireSamsung2;
            this.pictureBox3.Location = new System.Drawing.Point(15, 367);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(218, 87);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 96;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Comercializadora.Properties.Resources.AireSamsung;
            this.pictureBox2.Location = new System.Drawing.Point(12, 173);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(220, 94);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources.samsung;
            this.pictureBox1.Location = new System.Drawing.Point(263, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(325, 116);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // BtRegresarMabe
            // 
            this.BtRegresarMabe.Image = global::Comercializadora.Properties.Resources.regresar3;
            this.BtRegresarMabe.Location = new System.Drawing.Point(2, 1);
            this.BtRegresarMabe.Name = "BtRegresarMabe";
            this.BtRegresarMabe.Size = new System.Drawing.Size(107, 82);
            this.BtRegresarMabe.TabIndex = 140;
            this.BtRegresarMabe.UseVisualStyleBackColor = true;
            this.BtRegresarMabe.Click += new System.EventHandler(this.BtRegresarMabe_Click);
            // 
            // AireSamsung
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(860, 741);
            this.Controls.Add(this.BtRegresarMabe);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.ComprarAireSamsung3);
            this.Controls.Add(this.AgregarAireSamsung3);
            this.Controls.Add(this.AgregarAireSamsung2);
            this.Controls.Add(this.ComprarAireSamsung2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.AgregarAireSamsung1);
            this.Controls.Add(this.ComprarAireSamsung1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "AireSamsung";
            this.Text = "AireSamsung";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button ComprarAireSamsung3;
        private System.Windows.Forms.Button AgregarAireSamsung3;
        private System.Windows.Forms.Button AgregarAireSamsung2;
        private System.Windows.Forms.Button ComprarAireSamsung2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button AgregarAireSamsung1;
        private System.Windows.Forms.Button ComprarAireSamsung1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Button BtRegresarMabe;
    }
}