﻿namespace Comercializadora
{
    partial class Categorias
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Categorias));
            this.label1 = new System.Windows.Forms.Label();
            this.Videojuegos = new System.Windows.Forms.Button();
            this.LineaB = new System.Windows.Forms.Button();
            this.Alcohol = new System.Windows.Forms.Button();
            this.Musica = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Categorias";
            // 
            // Videojuegos
            // 
            this.Videojuegos.BackColor = System.Drawing.Color.White;
            this.Videojuegos.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.Videojuegos.FlatAppearance.BorderSize = 0;
            this.Videojuegos.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.ControlDark;
            this.Videojuegos.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlDark;
            this.Videojuegos.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlDark;
            this.Videojuegos.Font = new System.Drawing.Font("Century Gothic", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Videojuegos.Image = ((System.Drawing.Image)(resources.GetObject("Videojuegos.Image")));
            this.Videojuegos.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Videojuegos.Location = new System.Drawing.Point(26, 51);
            this.Videojuegos.Name = "Videojuegos";
            this.Videojuegos.Size = new System.Drawing.Size(115, 99);
            this.Videojuegos.TabIndex = 8;
            this.Videojuegos.Text = "videojuegos";
            this.Videojuegos.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Videojuegos.UseVisualStyleBackColor = false;
            this.Videojuegos.Click += new System.EventHandler(this.Videojuegos_Click);
            // 
            // LineaB
            // 
            this.LineaB.BackColor = System.Drawing.Color.White;
            this.LineaB.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.LineaB.FlatAppearance.BorderSize = 0;
            this.LineaB.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.ControlDark;
            this.LineaB.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlDark;
            this.LineaB.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlDark;
            this.LineaB.Font = new System.Drawing.Font("Century Gothic", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LineaB.Image = ((System.Drawing.Image)(resources.GetObject("LineaB.Image")));
            this.LineaB.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.LineaB.Location = new System.Drawing.Point(346, 51);
            this.LineaB.Name = "LineaB";
            this.LineaB.Size = new System.Drawing.Size(115, 99);
            this.LineaB.TabIndex = 9;
            this.LineaB.Text = "Linea Blanca";
            this.LineaB.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.LineaB.UseVisualStyleBackColor = false;
            // 
            // Alcohol
            // 
            this.Alcohol.BackColor = System.Drawing.Color.White;
            this.Alcohol.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.Alcohol.FlatAppearance.BorderSize = 0;
            this.Alcohol.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.ControlDark;
            this.Alcohol.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlDark;
            this.Alcohol.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlDark;
            this.Alcohol.Font = new System.Drawing.Font("Century Gothic", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Alcohol.Image = ((System.Drawing.Image)(resources.GetObject("Alcohol.Image")));
            this.Alcohol.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Alcohol.Location = new System.Drawing.Point(26, 213);
            this.Alcohol.Name = "Alcohol";
            this.Alcohol.Size = new System.Drawing.Size(115, 99);
            this.Alcohol.TabIndex = 10;
            this.Alcohol.Text = "Vinos y licores";
            this.Alcohol.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Alcohol.UseVisualStyleBackColor = false;
            this.Alcohol.Click += new System.EventHandler(this.Alcohol_Click);
            // 
            // Musica
            // 
            this.Musica.BackColor = System.Drawing.Color.White;
            this.Musica.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.Musica.FlatAppearance.BorderSize = 0;
            this.Musica.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.ControlDark;
            this.Musica.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ControlDark;
            this.Musica.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ControlDark;
            this.Musica.Font = new System.Drawing.Font("Century Gothic", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Musica.Image = ((System.Drawing.Image)(resources.GetObject("Musica.Image")));
            this.Musica.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Musica.Location = new System.Drawing.Point(346, 213);
            this.Musica.Name = "Musica";
            this.Musica.Size = new System.Drawing.Size(115, 99);
            this.Musica.TabIndex = 11;
            this.Musica.Text = "Musica";
            this.Musica.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Musica.UseVisualStyleBackColor = false;
            this.Musica.Click += new System.EventHandler(this.Musica_Click);
            // 
            // Categorias
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(500, 400);
            this.Controls.Add(this.Musica);
            this.Controls.Add(this.Alcohol);
            this.Controls.Add(this.LineaB);
            this.Controls.Add(this.Videojuegos);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Categorias";
            this.Text = "Categorias";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Videojuegos;
        private System.Windows.Forms.Button LineaB;
        private System.Windows.Forms.Button Alcohol;
        private System.Windows.Forms.Button Musica;
    }
}