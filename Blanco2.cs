﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comercializadora
{
    public partial class Blanco2 : Form
    {
        public Blanco2()
        {
            InitializeComponent();
        }

        private void btnPag1_Click(object sender, EventArgs e)
        {
            this.Close();
            Blanco b1 = new Blanco();
            b1.ShowDialog();
        }

        private void label7_Click(object sender, EventArgs e)
        {
            Blanco2 b2 = new Blanco2();
            b2.ShowDialog();
            this.Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
