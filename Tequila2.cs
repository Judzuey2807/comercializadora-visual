﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comercializadora
{
    public partial class Tequila2 : Form
    {
        public Tequila2()
        {
            InitializeComponent();
        }

        private void btnPag1_Click(object sender, EventArgs e)
        {
            Tequila t1 = new Tequila();
            t1.ShowDialog();
            this.Close();
        }

        private void label7_Click(object sender, EventArgs e)
        {
            Tequila2 t2 = new Tequila2();
            t2.ShowDialog();
            this.Close();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
