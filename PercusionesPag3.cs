﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comercializadora
{
    public partial class PercusionesPag3 : Form
    {
        public PercusionesPag3()
        {
            InitializeComponent();
        }

        private void btnBPag1_Click(object sender, EventArgs e)
        {
            this.Close();
            PercusionesPag1 pr1 = new PercusionesPag1();
            pr1.Show();
        }

        private void btnBPag2_Click(object sender, EventArgs e)
        {
            this.Close();
            PercusionesPag2 pr2 = new PercusionesPag2();
            pr2.Show();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
