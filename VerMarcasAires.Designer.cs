﻿namespace Comercializadora
{
    partial class VerMarcasAires
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VerMarcasAires));
            this.BtRegresarMabe = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.ComprarVerTodoAireSamsung3 = new System.Windows.Forms.Button();
            this.AgregarVerTodoAireSamsung3 = new System.Windows.Forms.Button();
            this.AgregarVerTodoAireSamsung2 = new System.Windows.Forms.Button();
            this.ComprarVerTodoAireSamsung2 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.AgregarVerTodoAireSamsung1 = new System.Windows.Forms.Button();
            this.ComprarVerTodoAireSamsung1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.AgregarVerTodoAireMabe3 = new System.Windows.Forms.Button();
            this.ComprarVerTodoAireMabe3 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.AgregarVerTodoAireMabe2 = new System.Windows.Forms.Button();
            this.ComprarVerTodoAireMabe2 = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.AgregarVerTodoAireMabe1 = new System.Windows.Forms.Button();
            this.ComprarVerTodoAireMabe1 = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.ComprarVerTodoAireLg3 = new System.Windows.Forms.Button();
            this.AgregarVerTodoAireLg3 = new System.Windows.Forms.Button();
            this.AgregarVerTodoAireLg2 = new System.Windows.Forms.Button();
            this.ComprarVerTodoAireLg2 = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.AgregarVerTodoAireLg1 = new System.Windows.Forms.Button();
            this.ComprarVerTodoAireLg1 = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.ComprarVerTodoAireCarrier3 = new System.Windows.Forms.Button();
            this.AgregarVerTodoAireCarrier3 = new System.Windows.Forms.Button();
            this.AgregarVerTodoAireCarrier2 = new System.Windows.Forms.Button();
            this.ComprarVerTodoAireCarrier2 = new System.Windows.Forms.Button();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.AgregarVerTodoAireCarrier1 = new System.Windows.Forms.Button();
            this.ComprarVerTodoAireCarrier1 = new System.Windows.Forms.Button();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            this.SuspendLayout();
            // 
            // BtRegresarMabe
            // 
            this.BtRegresarMabe.Image = global::Comercializadora.Properties.Resources.regresar3;
            this.BtRegresarMabe.Location = new System.Drawing.Point(2, 1);
            this.BtRegresarMabe.Name = "BtRegresarMabe";
            this.BtRegresarMabe.Size = new System.Drawing.Size(107, 82);
            this.BtRegresarMabe.TabIndex = 139;
            this.BtRegresarMabe.UseVisualStyleBackColor = true;
            this.BtRegresarMabe.Click += new System.EventHandler(this.BtRegresarMabe_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Comercializadora.Properties.Resources.AireSamsung3;
            this.pictureBox4.Location = new System.Drawing.Point(28, 588);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(208, 100);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox4.TabIndex = 161;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Comercializadora.Properties.Resources.AireSamsung2;
            this.pictureBox3.Location = new System.Drawing.Point(21, 383);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(218, 87);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 160;
            this.pictureBox3.TabStop = false;
            // 
            // ComprarVerTodoAireSamsung3
            // 
            this.ComprarVerTodoAireSamsung3.Location = new System.Drawing.Point(247, 723);
            this.ComprarVerTodoAireSamsung3.Name = "ComprarVerTodoAireSamsung3";
            this.ComprarVerTodoAireSamsung3.Size = new System.Drawing.Size(105, 23);
            this.ComprarVerTodoAireSamsung3.TabIndex = 159;
            this.ComprarVerTodoAireSamsung3.Text = "Comprar";
            this.ComprarVerTodoAireSamsung3.UseVisualStyleBackColor = true;
            // 
            // AgregarVerTodoAireSamsung3
            // 
            this.AgregarVerTodoAireSamsung3.Location = new System.Drawing.Point(376, 723);
            this.AgregarVerTodoAireSamsung3.Name = "AgregarVerTodoAireSamsung3";
            this.AgregarVerTodoAireSamsung3.Size = new System.Drawing.Size(105, 23);
            this.AgregarVerTodoAireSamsung3.TabIndex = 158;
            this.AgregarVerTodoAireSamsung3.Text = "Agregar al carrito";
            this.AgregarVerTodoAireSamsung3.UseVisualStyleBackColor = true;
            // 
            // AgregarVerTodoAireSamsung2
            // 
            this.AgregarVerTodoAireSamsung2.Location = new System.Drawing.Point(376, 505);
            this.AgregarVerTodoAireSamsung2.Name = "AgregarVerTodoAireSamsung2";
            this.AgregarVerTodoAireSamsung2.Size = new System.Drawing.Size(105, 23);
            this.AgregarVerTodoAireSamsung2.TabIndex = 157;
            this.AgregarVerTodoAireSamsung2.Text = "Agregar al carrito";
            this.AgregarVerTodoAireSamsung2.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoAireSamsung2
            // 
            this.ComprarVerTodoAireSamsung2.Location = new System.Drawing.Point(247, 505);
            this.ComprarVerTodoAireSamsung2.Name = "ComprarVerTodoAireSamsung2";
            this.ComprarVerTodoAireSamsung2.Size = new System.Drawing.Size(105, 23);
            this.ComprarVerTodoAireSamsung2.TabIndex = 156;
            this.ComprarVerTodoAireSamsung2.Text = "Comprar";
            this.ComprarVerTodoAireSamsung2.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(245, 622);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(383, 78);
            this.label12.TabIndex = 155;
            this.label12.Text = resources.GetString("label12.Text");
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(245, 600);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 15);
            this.label11.TabIndex = 154;
            this.label11.Text = "$ 13,691.30";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(245, 570);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(107, 15);
            this.label10.TabIndex = 153;
            this.label10.Text = "Marca: SAMSUNG";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(244, 548);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(555, 22);
            this.label9.TabIndex = 152;
            this.label9.Text = "AIRE ACONDICIONADO MINISPLIT 18000 BTU\'S PLATEADO";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(245, 431);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(270, 39);
            this.label8.TabIndex = 151;
            this.label8.Text = "Enfría hasta un 38% más rápido que un mini split común\r\nTecnología de iones S-Pla" +
    "sma\r\n70% menos ruido";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(245, 412);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 15);
            this.label7.TabIndex = 150;
            this.label7.Text = "$ 15,990.00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(245, 383);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 15);
            this.label6.TabIndex = 149;
            this.label6.Text = "Marca: SAMSUNG";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label5.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(250, 361);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(488, 22);
            this.label5.TabIndex = 148;
            this.label5.Text = "Mini Split Samsung Inverter Frío y Calor 12,000 BTU";
            // 
            // AgregarVerTodoAireSamsung1
            // 
            this.AgregarVerTodoAireSamsung1.Location = new System.Drawing.Point(376, 308);
            this.AgregarVerTodoAireSamsung1.Name = "AgregarVerTodoAireSamsung1";
            this.AgregarVerTodoAireSamsung1.Size = new System.Drawing.Size(105, 23);
            this.AgregarVerTodoAireSamsung1.TabIndex = 147;
            this.AgregarVerTodoAireSamsung1.Text = "Agregar al carrito";
            this.AgregarVerTodoAireSamsung1.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoAireSamsung1
            // 
            this.ComprarVerTodoAireSamsung1.Location = new System.Drawing.Point(247, 308);
            this.ComprarVerTodoAireSamsung1.Name = "ComprarVerTodoAireSamsung1";
            this.ComprarVerTodoAireSamsung1.Size = new System.Drawing.Size(105, 23);
            this.ComprarVerTodoAireSamsung1.TabIndex = 146;
            this.ComprarVerTodoAireSamsung1.Text = "Comprar";
            this.ComprarVerTodoAireSamsung1.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(244, 241);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 52);
            this.label4.TabIndex = 145;
            this.label4.Text = "220 V\r\n1 tonelada\r\n12,000 BTUS\r\n16 SEER\r\n";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(244, 216);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 15);
            this.label3.TabIndex = 144;
            this.label3.Text = "$ 8,999.00";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(245, 189);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 15);
            this.label2.TabIndex = 143;
            this.label2.Text = "Marca: SAMSUNG";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(243, 167);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(554, 22);
            this.label1.TabIndex = 142;
            this.label1.Text = "Aire Acondicionado Samsung AR12MVFHEWK Inverter Frio";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Comercializadora.Properties.Resources.AireSamsung;
            this.pictureBox2.Location = new System.Drawing.Point(18, 189);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(220, 94);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 141;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources.samsung;
            this.pictureBox1.Location = new System.Drawing.Point(269, 28);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(325, 116);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 140;
            this.pictureBox1.TabStop = false;
            // 
            // AgregarVerTodoAireMabe3
            // 
            this.AgregarVerTodoAireMabe3.Location = new System.Drawing.Point(375, 1473);
            this.AgregarVerTodoAireMabe3.Name = "AgregarVerTodoAireMabe3";
            this.AgregarVerTodoAireMabe3.Size = new System.Drawing.Size(105, 23);
            this.AgregarVerTodoAireMabe3.TabIndex = 183;
            this.AgregarVerTodoAireMabe3.Text = "Agregar al carrito";
            this.AgregarVerTodoAireMabe3.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoAireMabe3
            // 
            this.ComprarVerTodoAireMabe3.Location = new System.Drawing.Point(248, 1473);
            this.ComprarVerTodoAireMabe3.Name = "ComprarVerTodoAireMabe3";
            this.ComprarVerTodoAireMabe3.Size = new System.Drawing.Size(105, 23);
            this.ComprarVerTodoAireMabe3.TabIndex = 182;
            this.ComprarVerTodoAireMabe3.Text = "Comprar";
            this.ComprarVerTodoAireMabe3.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(245, 1339);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(224, 117);
            this.label13.TabIndex = 181;
            this.label13.Text = "Tipo de clima, solo frío\r\nCapacidad de 12,000 BTU\r\n220 voltios\r\nTemperatura desde" +
    " 16-30 grados centígrados\r\nTubería de cobre\r\nFiltro de alta densidad\r\nIon de pla" +
    "ta\r\nBio filtro\r\nDisplay con luz LE";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(245, 1315);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(72, 15);
            this.label14.TabIndex = 180;
            this.label14.Text = "$ 12,290.00";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(245, 1291);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(82, 15);
            this.label15.TabIndex = 179;
            this.label15.Text = "Marca: MABE";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(244, 1269);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(433, 22);
            this.label16.TabIndex = 178;
            this.label16.Text = "Inversor Mini Split Mabe Solo Frío 12,000 BTU";
            // 
            // AgregarVerTodoAireMabe2
            // 
            this.AgregarVerTodoAireMabe2.Location = new System.Drawing.Point(375, 1218);
            this.AgregarVerTodoAireMabe2.Name = "AgregarVerTodoAireMabe2";
            this.AgregarVerTodoAireMabe2.Size = new System.Drawing.Size(105, 23);
            this.AgregarVerTodoAireMabe2.TabIndex = 177;
            this.AgregarVerTodoAireMabe2.Text = "Agregar al carrito";
            this.AgregarVerTodoAireMabe2.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoAireMabe2
            // 
            this.ComprarVerTodoAireMabe2.Location = new System.Drawing.Point(248, 1218);
            this.ComprarVerTodoAireMabe2.Name = "ComprarVerTodoAireMabe2";
            this.ComprarVerTodoAireMabe2.Size = new System.Drawing.Size(105, 23);
            this.ComprarVerTodoAireMabe2.TabIndex = 176;
            this.ComprarVerTodoAireMabe2.Text = "Comprar";
            this.ComprarVerTodoAireMabe2.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(245, 1158);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(291, 39);
            this.label17.TabIndex = 175;
            this.label17.Text = "Con tecnología Wi-Fi para encenderlo desde cualquier lugar\r\nTemperaturas de 16 a " +
    "30 ° C, 220 V / 60 Hz\r\nColor plata oscura";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(245, 1134);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(72, 15);
            this.label18.TabIndex = 174;
            this.label18.Text = "$ 10,990.00";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(245, 1110);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(82, 15);
            this.label19.TabIndex = 173;
            this.label19.Text = "Marca: MABE";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(244, 1088);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(443, 22);
            this.label20.TabIndex = 172;
            this.label20.Text = "Mini Split Mabe Inverter Solo Frío 18,000 BTU\'s";
            // 
            // AgregarVerTodoAireMabe1
            // 
            this.AgregarVerTodoAireMabe1.Location = new System.Drawing.Point(375, 1036);
            this.AgregarVerTodoAireMabe1.Name = "AgregarVerTodoAireMabe1";
            this.AgregarVerTodoAireMabe1.Size = new System.Drawing.Size(105, 23);
            this.AgregarVerTodoAireMabe1.TabIndex = 171;
            this.AgregarVerTodoAireMabe1.Text = "Agregar al carrito";
            this.AgregarVerTodoAireMabe1.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoAireMabe1
            // 
            this.ComprarVerTodoAireMabe1.Location = new System.Drawing.Point(248, 1036);
            this.ComprarVerTodoAireMabe1.Name = "ComprarVerTodoAireMabe1";
            this.ComprarVerTodoAireMabe1.Size = new System.Drawing.Size(105, 23);
            this.ComprarVerTodoAireMabe1.TabIndex = 170;
            this.ComprarVerTodoAireMabe1.Text = "Comprar";
            this.ComprarVerTodoAireMabe1.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(245, 976);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(306, 39);
            this.label21.TabIndex = 169;
            this.label21.Text = "Recubrimiento Blue Fin, protege las tuberías contra la corrosión\r\nFiltro Silver I" +
    "on que elimina bacterias\r\nCon Gas Ecológico R410A para ahorrar energía";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(245, 952);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(65, 15);
            this.label22.TabIndex = 168;
            this.label22.Text = "$ 6,590.00";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(245, 928);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(82, 15);
            this.label23.TabIndex = 167;
            this.label23.Text = "Marca: MABE";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(244, 906);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(369, 22);
            this.label24.TabIndex = 166;
            this.label24.Text = "Mini Split Mabe Frío y Calor 12000 BTU";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::Comercializadora.Properties.Resources.AireMabe3;
            this.pictureBox5.Location = new System.Drawing.Point(12, 1315);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(225, 115);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox5.TabIndex = 165;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::Comercializadora.Properties.Resources.AireMabe2;
            this.pictureBox6.Location = new System.Drawing.Point(12, 1111);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(225, 115);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox6.TabIndex = 164;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::Comercializadora.Properties.Resources.AireMabe1;
            this.pictureBox7.Location = new System.Drawing.Point(12, 928);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(225, 115);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox7.TabIndex = 163;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox8.Image = global::Comercializadora.Properties.Resources.Mabe;
            this.pictureBox8.Location = new System.Drawing.Point(299, 778);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(216, 95);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox8.TabIndex = 162;
            this.pictureBox8.TabStop = false;
            // 
            // ComprarVerTodoAireLg3
            // 
            this.ComprarVerTodoAireLg3.Location = new System.Drawing.Point(248, 2223);
            this.ComprarVerTodoAireLg3.Name = "ComprarVerTodoAireLg3";
            this.ComprarVerTodoAireLg3.Size = new System.Drawing.Size(105, 23);
            this.ComprarVerTodoAireLg3.TabIndex = 205;
            this.ComprarVerTodoAireLg3.Text = "Comprar";
            this.ComprarVerTodoAireLg3.UseVisualStyleBackColor = true;
            // 
            // AgregarVerTodoAireLg3
            // 
            this.AgregarVerTodoAireLg3.Location = new System.Drawing.Point(377, 2223);
            this.AgregarVerTodoAireLg3.Name = "AgregarVerTodoAireLg3";
            this.AgregarVerTodoAireLg3.Size = new System.Drawing.Size(105, 23);
            this.AgregarVerTodoAireLg3.TabIndex = 204;
            this.AgregarVerTodoAireLg3.Text = "Agregar al carrito";
            this.AgregarVerTodoAireLg3.UseVisualStyleBackColor = true;
            // 
            // AgregarVerTodoAireLg2
            // 
            this.AgregarVerTodoAireLg2.Location = new System.Drawing.Point(377, 2024);
            this.AgregarVerTodoAireLg2.Name = "AgregarVerTodoAireLg2";
            this.AgregarVerTodoAireLg2.Size = new System.Drawing.Size(105, 23);
            this.AgregarVerTodoAireLg2.TabIndex = 203;
            this.AgregarVerTodoAireLg2.Text = "Agregar al carrito";
            this.AgregarVerTodoAireLg2.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoAireLg2
            // 
            this.ComprarVerTodoAireLg2.Location = new System.Drawing.Point(248, 2024);
            this.ComprarVerTodoAireLg2.Name = "ComprarVerTodoAireLg2";
            this.ComprarVerTodoAireLg2.Size = new System.Drawing.Size(105, 23);
            this.ComprarVerTodoAireLg2.TabIndex = 202;
            this.ComprarVerTodoAireLg2.Text = "Comprar";
            this.ComprarVerTodoAireLg2.UseVisualStyleBackColor = true;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(246, 2137);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(321, 65);
            this.label25.TabIndex = 201;
            this.label25.Text = resources.GetString("label25.Text");
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label26.Location = new System.Drawing.Point(246, 2115);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(62, 15);
            this.label26.TabIndex = 200;
            this.label26.Text = "14,213.74";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(246, 2085);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(67, 15);
            this.label27.TabIndex = 199;
            this.label27.Text = "Marca:  LG";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label28.Location = new System.Drawing.Point(245, 2063);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(527, 22);
            this.label28.TabIndex = 198;
            this.label28.Text = "LG AIRE ACONDICIONADO MINISPLIT 12000 BTU\'S GRIS";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(246, 1927);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(253, 78);
            this.label29.TabIndex = 197;
            this.label29.Text = resources.GetString("label29.Text");
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label30.Location = new System.Drawing.Point(246, 1908);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(72, 15);
            this.label30.TabIndex = 196;
            this.label30.Text = "$ 10,290.00";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label31.Location = new System.Drawing.Point(246, 1879);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(67, 15);
            this.label31.TabIndex = 195;
            this.label31.Text = "Marca:  LG";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label32.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label32.Location = new System.Drawing.Point(245, 1857);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(433, 22);
            this.label32.TabIndex = 194;
            this.label32.Text = "Inversor Mini Split LG Frío y Calor 12,000 BTU";
            // 
            // AgregarVerTodoAireLg1
            // 
            this.AgregarVerTodoAireLg1.Location = new System.Drawing.Point(377, 1811);
            this.AgregarVerTodoAireLg1.Name = "AgregarVerTodoAireLg1";
            this.AgregarVerTodoAireLg1.Size = new System.Drawing.Size(105, 23);
            this.AgregarVerTodoAireLg1.TabIndex = 193;
            this.AgregarVerTodoAireLg1.Text = "Agregar al carrito";
            this.AgregarVerTodoAireLg1.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoAireLg1
            // 
            this.ComprarVerTodoAireLg1.Location = new System.Drawing.Point(248, 1811);
            this.ComprarVerTodoAireLg1.Name = "ComprarVerTodoAireLg1";
            this.ComprarVerTodoAireLg1.Size = new System.Drawing.Size(105, 23);
            this.ComprarVerTodoAireLg1.TabIndex = 192;
            this.ComprarVerTodoAireLg1.Text = "Comprar";
            this.ComprarVerTodoAireLg1.UseVisualStyleBackColor = true;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(246, 1757);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(57, 39);
            this.label33.TabIndex = 191;
            this.label33.Text = "1 tonelada\r\n18 SEER\r\nR410A";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label34.Location = new System.Drawing.Point(245, 1731);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(72, 15);
            this.label34.TabIndex = 190;
            this.label34.Text = "$ 12,999.00";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label35.Location = new System.Drawing.Point(246, 1704);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(64, 15);
            this.label35.TabIndex = 189;
            this.label35.Text = "Marca: LG";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label36.Location = new System.Drawing.Point(244, 1682);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(463, 22);
            this.label36.TabIndex = 188;
            this.label36.Text = "Aire Acondicionado LG VM121C8 Inverter S / Frio";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::Comercializadora.Properties.Resources.AireLg2;
            this.pictureBox9.Location = new System.Drawing.Point(12, 1882);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(219, 114);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox9.TabIndex = 187;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = global::Comercializadora.Properties.Resources.AireLg3;
            this.pictureBox10.Location = new System.Drawing.Point(12, 2091);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(219, 114);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox10.TabIndex = 186;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = global::Comercializadora.Properties.Resources.AireLg1;
            this.pictureBox11.Location = new System.Drawing.Point(12, 1695);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(219, 114);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox11.TabIndex = 185;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox12.Image = global::Comercializadora.Properties.Resources.Lg;
            this.pictureBox12.Location = new System.Drawing.Point(248, 1517);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(284, 132);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox12.TabIndex = 184;
            this.pictureBox12.TabStop = false;
            // 
            // ComprarVerTodoAireCarrier3
            // 
            this.ComprarVerTodoAireCarrier3.Location = new System.Drawing.Point(249, 2974);
            this.ComprarVerTodoAireCarrier3.Name = "ComprarVerTodoAireCarrier3";
            this.ComprarVerTodoAireCarrier3.Size = new System.Drawing.Size(105, 23);
            this.ComprarVerTodoAireCarrier3.TabIndex = 227;
            this.ComprarVerTodoAireCarrier3.Text = "Comprar";
            this.ComprarVerTodoAireCarrier3.UseVisualStyleBackColor = true;
            // 
            // AgregarVerTodoAireCarrier3
            // 
            this.AgregarVerTodoAireCarrier3.Location = new System.Drawing.Point(377, 2974);
            this.AgregarVerTodoAireCarrier3.Name = "AgregarVerTodoAireCarrier3";
            this.AgregarVerTodoAireCarrier3.Size = new System.Drawing.Size(105, 23);
            this.AgregarVerTodoAireCarrier3.TabIndex = 226;
            this.AgregarVerTodoAireCarrier3.Text = "Agregar al carrito";
            this.AgregarVerTodoAireCarrier3.UseVisualStyleBackColor = true;
            // 
            // AgregarVerTodoAireCarrier2
            // 
            this.AgregarVerTodoAireCarrier2.Location = new System.Drawing.Point(377, 2791);
            this.AgregarVerTodoAireCarrier2.Name = "AgregarVerTodoAireCarrier2";
            this.AgregarVerTodoAireCarrier2.Size = new System.Drawing.Size(105, 23);
            this.AgregarVerTodoAireCarrier2.TabIndex = 225;
            this.AgregarVerTodoAireCarrier2.Text = "Agregar al carrito";
            this.AgregarVerTodoAireCarrier2.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoAireCarrier2
            // 
            this.ComprarVerTodoAireCarrier2.Location = new System.Drawing.Point(249, 2791);
            this.ComprarVerTodoAireCarrier2.Name = "ComprarVerTodoAireCarrier2";
            this.ComprarVerTodoAireCarrier2.Size = new System.Drawing.Size(105, 23);
            this.ComprarVerTodoAireCarrier2.TabIndex = 224;
            this.ComprarVerTodoAireCarrier2.Text = "Comprar";
            this.ComprarVerTodoAireCarrier2.UseVisualStyleBackColor = true;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(246, 2919);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(306, 39);
            this.label37.TabIndex = 223;
            this.label37.Text = "Recubrimiento Blue Fin, protege las tuberías contra la corrosión\r\nFiltro Silver I" +
    "on que elimina bacterias\r\nCon Gas Ecológico R410A para ahorrar energía";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label38.Location = new System.Drawing.Point(246, 2892);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(65, 15);
            this.label38.TabIndex = 222;
            this.label38.Text = "$ 6,590.00";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label39.Location = new System.Drawing.Point(246, 2867);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(99, 15);
            this.label39.TabIndex = 221;
            this.label39.Text = "Marca: CARRIER";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label40.Location = new System.Drawing.Point(245, 2845);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(318, 22);
            this.label40.TabIndex = 220;
            this.label40.Text = "Mini Split  Frío y Calor 12000 BTU";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(246, 2711);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(384, 65);
            this.label41.TabIndex = 219;
            this.label41.Text = resources.GetString("label41.Text");
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label42.Location = new System.Drawing.Point(241, 2685);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(65, 15);
            this.label42.TabIndex = 218;
            this.label42.Text = "$ 4,990.00";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label43.Location = new System.Drawing.Point(240, 2660);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(102, 15);
            this.label43.TabIndex = 217;
            this.label43.Text = "Marca:  CARRIER";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label44.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label44.Location = new System.Drawing.Point(239, 2638);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(256, 22);
            this.label44.TabIndex = 216;
            this.label44.Text = "Aire Acondicionado 12000 ";
            // 
            // AgregarVerTodoAireCarrier1
            // 
            this.AgregarVerTodoAireCarrier1.Location = new System.Drawing.Point(378, 2591);
            this.AgregarVerTodoAireCarrier1.Name = "AgregarVerTodoAireCarrier1";
            this.AgregarVerTodoAireCarrier1.Size = new System.Drawing.Size(105, 23);
            this.AgregarVerTodoAireCarrier1.TabIndex = 215;
            this.AgregarVerTodoAireCarrier1.Text = "Agregar al carrito";
            this.AgregarVerTodoAireCarrier1.UseVisualStyleBackColor = true;
            // 
            // ComprarVerTodoAireCarrier1
            // 
            this.ComprarVerTodoAireCarrier1.Location = new System.Drawing.Point(250, 2591);
            this.ComprarVerTodoAireCarrier1.Name = "ComprarVerTodoAireCarrier1";
            this.ComprarVerTodoAireCarrier1.Size = new System.Drawing.Size(105, 23);
            this.ComprarVerTodoAireCarrier1.TabIndex = 214;
            this.ComprarVerTodoAireCarrier1.Text = "Comprar";
            this.ComprarVerTodoAireCarrier1.UseVisualStyleBackColor = true;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(246, 2540);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(57, 39);
            this.label45.TabIndex = 213;
            this.label45.Text = "1 tonelada\r\n18 SEER\r\nR410A";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label46.Location = new System.Drawing.Point(247, 2511);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(65, 15);
            this.label46.TabIndex = 212;
            this.label46.Text = "$ 6,600.71";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label47.Location = new System.Drawing.Point(245, 2486);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(99, 15);
            this.label47.TabIndex = 211;
            this.label47.Text = "Marca: CARRIER";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label48.Location = new System.Drawing.Point(245, 2442);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(445, 44);
            this.label48.TabIndex = 210;
            this.label48.Text = "CARRIER AIRE ACONDICIONADO MINISPLIT LIV\r\n12000 BTU\'S BLANCO";
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = global::Comercializadora.Properties.Resources.AireCarrier2;
            this.pictureBox13.Location = new System.Drawing.Point(14, 2678);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(214, 94);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox13.TabIndex = 209;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.Image = global::Comercializadora.Properties.Resources.AireCarrier3;
            this.pictureBox14.Location = new System.Drawing.Point(14, 2872);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(214, 94);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox14.TabIndex = 208;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.Image = global::Comercializadora.Properties.Resources.AireCarrier;
            this.pictureBox15.Location = new System.Drawing.Point(14, 2472);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(214, 94);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox15.TabIndex = 207;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.Image = global::Comercializadora.Properties.Resources.Carrier;
            this.pictureBox16.Location = new System.Drawing.Point(188, 2266);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(448, 142);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox16.TabIndex = 206;
            this.pictureBox16.TabStop = false;
            // 
            // VerMarcasAires
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(817, 774);
            this.Controls.Add(this.ComprarVerTodoAireCarrier3);
            this.Controls.Add(this.AgregarVerTodoAireCarrier3);
            this.Controls.Add(this.AgregarVerTodoAireCarrier2);
            this.Controls.Add(this.ComprarVerTodoAireCarrier2);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.AgregarVerTodoAireCarrier1);
            this.Controls.Add(this.ComprarVerTodoAireCarrier1);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.label48);
            this.Controls.Add(this.pictureBox13);
            this.Controls.Add(this.pictureBox14);
            this.Controls.Add(this.pictureBox15);
            this.Controls.Add(this.pictureBox16);
            this.Controls.Add(this.ComprarVerTodoAireLg3);
            this.Controls.Add(this.AgregarVerTodoAireLg3);
            this.Controls.Add(this.AgregarVerTodoAireLg2);
            this.Controls.Add(this.ComprarVerTodoAireLg2);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.AgregarVerTodoAireLg1);
            this.Controls.Add(this.ComprarVerTodoAireLg1);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.pictureBox12);
            this.Controls.Add(this.AgregarVerTodoAireMabe3);
            this.Controls.Add(this.ComprarVerTodoAireMabe3);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.AgregarVerTodoAireMabe2);
            this.Controls.Add(this.ComprarVerTodoAireMabe2);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.AgregarVerTodoAireMabe1);
            this.Controls.Add(this.ComprarVerTodoAireMabe1);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.ComprarVerTodoAireSamsung3);
            this.Controls.Add(this.AgregarVerTodoAireSamsung3);
            this.Controls.Add(this.AgregarVerTodoAireSamsung2);
            this.Controls.Add(this.ComprarVerTodoAireSamsung2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.AgregarVerTodoAireSamsung1);
            this.Controls.Add(this.ComprarVerTodoAireSamsung1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.BtRegresarMabe);
            this.Name = "VerMarcasAires";
            this.Text = "VerMarcasAires";
            this.Load += new System.EventHandler(this.VerMarcasAires_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtRegresarMabe;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button ComprarVerTodoAireSamsung3;
        private System.Windows.Forms.Button AgregarVerTodoAireSamsung3;
        private System.Windows.Forms.Button AgregarVerTodoAireSamsung2;
        private System.Windows.Forms.Button ComprarVerTodoAireSamsung2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button AgregarVerTodoAireSamsung1;
        private System.Windows.Forms.Button ComprarVerTodoAireSamsung1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button AgregarVerTodoAireMabe3;
        private System.Windows.Forms.Button ComprarVerTodoAireMabe3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button AgregarVerTodoAireMabe2;
        private System.Windows.Forms.Button ComprarVerTodoAireMabe2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button AgregarVerTodoAireMabe1;
        private System.Windows.Forms.Button ComprarVerTodoAireMabe1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Button ComprarVerTodoAireLg3;
        private System.Windows.Forms.Button AgregarVerTodoAireLg3;
        private System.Windows.Forms.Button AgregarVerTodoAireLg2;
        private System.Windows.Forms.Button ComprarVerTodoAireLg2;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Button AgregarVerTodoAireLg1;
        private System.Windows.Forms.Button ComprarVerTodoAireLg1;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.Button ComprarVerTodoAireCarrier3;
        private System.Windows.Forms.Button AgregarVerTodoAireCarrier3;
        private System.Windows.Forms.Button AgregarVerTodoAireCarrier2;
        private System.Windows.Forms.Button ComprarVerTodoAireCarrier2;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Button AgregarVerTodoAireCarrier1;
        private System.Windows.Forms.Button ComprarVerTodoAireCarrier1;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox16;
    }
}