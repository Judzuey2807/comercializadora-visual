﻿namespace Comercializadora
{
    partial class AudioPag1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDescripcionConsolaA = new System.Windows.Forms.Label();
            this.lblPrecioConsolaA = new System.Windows.Forms.Label();
            this.comboBoxConsolaA = new System.Windows.Forms.ComboBox();
            this.btnConsolaA = new System.Windows.Forms.Button();
            this.lblDescripcionMicro = new System.Windows.Forms.Label();
            this.lblPrecioMicro = new System.Windows.Forms.Label();
            this.btnMicro = new System.Windows.Forms.Button();
            this.comboBoxMicro = new System.Windows.Forms.ComboBox();
            this.lblDescripcionAudiBey = new System.Windows.Forms.Label();
            this.lblPrecioAudiBey = new System.Windows.Forms.Label();
            this.btnAudiBey = new System.Windows.Forms.Button();
            this.comboBoxAudiBey = new System.Windows.Forms.ComboBox();
            this.btnAPag2 = new System.Windows.Forms.Button();
            this.lblAPag1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxAudiBey = new System.Windows.Forms.PictureBox();
            this.pictureBoxMicro = new System.Windows.Forms.PictureBox();
            this.pictureBoxConsolaA = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAudiBey)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMicro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxConsolaA)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDescripcionConsolaA
            // 
            this.lblDescripcionConsolaA.AutoSize = true;
            this.lblDescripcionConsolaA.Location = new System.Drawing.Point(168, 79);
            this.lblDescripcionConsolaA.Name = "lblDescripcionConsolaA";
            this.lblDescripcionConsolaA.Size = new System.Drawing.Size(340, 13);
            this.lblDescripcionConsolaA.TabIndex = 1;
            this.lblDescripcionConsolaA.Text = "Mezcladora 8 canales con efectos y conexión usb Alesis MM 8 usb FX";
            // 
            // lblPrecioConsolaA
            // 
            this.lblPrecioConsolaA.AutoSize = true;
            this.lblPrecioConsolaA.Location = new System.Drawing.Point(168, 104);
            this.lblPrecioConsolaA.Name = "lblPrecioConsolaA";
            this.lblPrecioConsolaA.Size = new System.Drawing.Size(40, 13);
            this.lblPrecioConsolaA.TabIndex = 2;
            this.lblPrecioConsolaA.Text = "$4,155";
            // 
            // comboBoxConsolaA
            // 
            this.comboBoxConsolaA.FormattingEnabled = true;
            this.comboBoxConsolaA.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxConsolaA.Location = new System.Drawing.Point(611, 94);
            this.comboBoxConsolaA.Name = "comboBoxConsolaA";
            this.comboBoxConsolaA.Size = new System.Drawing.Size(44, 21);
            this.comboBoxConsolaA.TabIndex = 3;
            // 
            // btnConsolaA
            // 
            this.btnConsolaA.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnConsolaA.Location = new System.Drawing.Point(673, 92);
            this.btnConsolaA.Name = "btnConsolaA";
            this.btnConsolaA.Size = new System.Drawing.Size(75, 23);
            this.btnConsolaA.TabIndex = 4;
            this.btnConsolaA.Text = "Agregar";
            this.btnConsolaA.UseVisualStyleBackColor = true;
            // 
            // lblDescripcionMicro
            // 
            this.lblDescripcionMicro.AutoSize = true;
            this.lblDescripcionMicro.Location = new System.Drawing.Point(168, 188);
            this.lblDescripcionMicro.Name = "lblDescripcionMicro";
            this.lblDescripcionMicro.Size = new System.Drawing.Size(308, 13);
            this.lblDescripcionMicro.TabIndex = 6;
            this.lblDescripcionMicro.Text = "Micrófono condensador multipatrón AUDIO-TECHNICA AT2050";
            // 
            // lblPrecioMicro
            // 
            this.lblPrecioMicro.AutoSize = true;
            this.lblPrecioMicro.Location = new System.Drawing.Point(168, 214);
            this.lblPrecioMicro.Name = "lblPrecioMicro";
            this.lblPrecioMicro.Size = new System.Drawing.Size(40, 13);
            this.lblPrecioMicro.TabIndex = 7;
            this.lblPrecioMicro.Text = "$5,410";
            // 
            // btnMicro
            // 
            this.btnMicro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMicro.Location = new System.Drawing.Point(673, 198);
            this.btnMicro.Name = "btnMicro";
            this.btnMicro.Size = new System.Drawing.Size(75, 23);
            this.btnMicro.TabIndex = 8;
            this.btnMicro.Text = "Agregar";
            this.btnMicro.UseVisualStyleBackColor = true;
            // 
            // comboBoxMicro
            // 
            this.comboBoxMicro.FormattingEnabled = true;
            this.comboBoxMicro.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxMicro.Location = new System.Drawing.Point(611, 200);
            this.comboBoxMicro.Name = "comboBoxMicro";
            this.comboBoxMicro.Size = new System.Drawing.Size(44, 21);
            this.comboBoxMicro.TabIndex = 9;
            // 
            // lblDescripcionAudiBey
            // 
            this.lblDescripcionAudiBey.AutoSize = true;
            this.lblDescripcionAudiBey.Location = new System.Drawing.Point(168, 297);
            this.lblDescripcionAudiBey.Name = "lblDescripcionAudiBey";
            this.lblDescripcionAudiBey.Size = new System.Drawing.Size(256, 13);
            this.lblDescripcionAudiBey.TabIndex = 11;
            this.lblDescripcionAudiBey.Text = "Audífonos cerrados de estudio BEYER DT 770 PRO";
            // 
            // lblPrecioAudiBey
            // 
            this.lblPrecioAudiBey.AutoSize = true;
            this.lblPrecioAudiBey.Location = new System.Drawing.Point(168, 329);
            this.lblPrecioAudiBey.Name = "lblPrecioAudiBey";
            this.lblPrecioAudiBey.Size = new System.Drawing.Size(40, 13);
            this.lblPrecioAudiBey.TabIndex = 12;
            this.lblPrecioAudiBey.Text = "$2,655";
            // 
            // btnAudiBey
            // 
            this.btnAudiBey.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAudiBey.Location = new System.Drawing.Point(673, 308);
            this.btnAudiBey.Name = "btnAudiBey";
            this.btnAudiBey.Size = new System.Drawing.Size(75, 23);
            this.btnAudiBey.TabIndex = 13;
            this.btnAudiBey.Text = "Agregar";
            this.btnAudiBey.UseVisualStyleBackColor = true;
            // 
            // comboBoxAudiBey
            // 
            this.comboBoxAudiBey.FormattingEnabled = true;
            this.comboBoxAudiBey.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxAudiBey.Location = new System.Drawing.Point(611, 310);
            this.comboBoxAudiBey.Name = "comboBoxAudiBey";
            this.comboBoxAudiBey.Size = new System.Drawing.Size(44, 21);
            this.comboBoxAudiBey.TabIndex = 14;
            // 
            // btnAPag2
            // 
            this.btnAPag2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAPag2.Location = new System.Drawing.Point(392, 412);
            this.btnAPag2.Name = "btnAPag2";
            this.btnAPag2.Size = new System.Drawing.Size(27, 23);
            this.btnAPag2.TabIndex = 15;
            this.btnAPag2.Text = "2";
            this.btnAPag2.UseVisualStyleBackColor = true;
            this.btnAPag2.Click += new System.EventHandler(this.btnAPag2_Click);
            // 
            // lblAPag1
            // 
            this.lblAPag1.AutoSize = true;
            this.lblAPag1.Location = new System.Drawing.Point(370, 417);
            this.lblAPag1.Name = "lblAPag1";
            this.lblAPag1.Size = new System.Drawing.Size(13, 13);
            this.lblAPag1.TabIndex = 17;
            this.lblAPag1.Text = "1";
            this.lblAPag1.Click += new System.EventHandler(this.lblAPag1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources._2569f;
            this.pictureBox1.Location = new System.Drawing.Point(720, 407);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(28, 28);
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click_1);
            // 
            // pictureBoxAudiBey
            // 
            this.pictureBoxAudiBey.Image = global::Comercializadora.Properties.Resources.beyerdinamic_dt770pro_01;
            this.pictureBoxAudiBey.Location = new System.Drawing.Point(31, 264);
            this.pictureBoxAudiBey.Name = "pictureBoxAudiBey";
            this.pictureBoxAudiBey.Size = new System.Drawing.Size(103, 103);
            this.pictureBoxAudiBey.TabIndex = 10;
            this.pictureBoxAudiBey.TabStop = false;
            // 
            // pictureBoxMicro
            // 
            this.pictureBoxMicro.Image = global::Comercializadora.Properties.Resources.audiotechnica_at2050_01;
            this.pictureBoxMicro.Location = new System.Drawing.Point(31, 155);
            this.pictureBoxMicro.Name = "pictureBoxMicro";
            this.pictureBoxMicro.Size = new System.Drawing.Size(102, 103);
            this.pictureBoxMicro.TabIndex = 5;
            this.pictureBoxMicro.TabStop = false;
            // 
            // pictureBoxConsolaA
            // 
            this.pictureBoxConsolaA.Image = global::Comercializadora.Properties.Resources._52cals0002a_1;
            this.pictureBoxConsolaA.Location = new System.Drawing.Point(30, 44);
            this.pictureBoxConsolaA.Name = "pictureBoxConsolaA";
            this.pictureBoxConsolaA.Size = new System.Drawing.Size(103, 105);
            this.pictureBoxConsolaA.TabIndex = 0;
            this.pictureBoxConsolaA.TabStop = false;
            this.pictureBoxConsolaA.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // AudioPag1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 465);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblAPag1);
            this.Controls.Add(this.btnAPag2);
            this.Controls.Add(this.comboBoxAudiBey);
            this.Controls.Add(this.btnAudiBey);
            this.Controls.Add(this.lblPrecioAudiBey);
            this.Controls.Add(this.lblDescripcionAudiBey);
            this.Controls.Add(this.pictureBoxAudiBey);
            this.Controls.Add(this.comboBoxMicro);
            this.Controls.Add(this.btnMicro);
            this.Controls.Add(this.lblPrecioMicro);
            this.Controls.Add(this.lblDescripcionMicro);
            this.Controls.Add(this.pictureBoxMicro);
            this.Controls.Add(this.btnConsolaA);
            this.Controls.Add(this.comboBoxConsolaA);
            this.Controls.Add(this.lblPrecioConsolaA);
            this.Controls.Add(this.lblDescripcionConsolaA);
            this.Controls.Add(this.pictureBoxConsolaA);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AudioPag1";
            this.Text = "AudioPag1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAudiBey)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMicro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxConsolaA)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxConsolaA;
        private System.Windows.Forms.Label lblDescripcionConsolaA;
        private System.Windows.Forms.Label lblPrecioConsolaA;
        private System.Windows.Forms.ComboBox comboBoxConsolaA;
        private System.Windows.Forms.Button btnConsolaA;
        private System.Windows.Forms.PictureBox pictureBoxMicro;
        private System.Windows.Forms.Label lblDescripcionMicro;
        private System.Windows.Forms.Label lblPrecioMicro;
        private System.Windows.Forms.Button btnMicro;
        private System.Windows.Forms.ComboBox comboBoxMicro;
        private System.Windows.Forms.PictureBox pictureBoxAudiBey;
        private System.Windows.Forms.Label lblDescripcionAudiBey;
        private System.Windows.Forms.Label lblPrecioAudiBey;
        private System.Windows.Forms.Button btnAudiBey;
        private System.Windows.Forms.ComboBox comboBoxAudiBey;
        private System.Windows.Forms.Button btnAPag2;
        private System.Windows.Forms.Label lblAPag1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}