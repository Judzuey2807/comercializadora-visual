﻿namespace Comercializadora
{
    partial class MicroWhirlpool
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ComprarMicroWhirlpool3 = new System.Windows.Forms.Button();
            this.AgregarMicroWhirlpool3 = new System.Windows.Forms.Button();
            this.AgregarMicroWhirlpool2 = new System.Windows.Forms.Button();
            this.ComprarMicroWhirlpool2 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.AgregarMicroWhirlpool1 = new System.Windows.Forms.Button();
            this.CompraMicroWhirlpool1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BtRegresarMabe = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // ComprarMicroWhirlpool3
            // 
            this.ComprarMicroWhirlpool3.Location = new System.Drawing.Point(229, 662);
            this.ComprarMicroWhirlpool3.Name = "ComprarMicroWhirlpool3";
            this.ComprarMicroWhirlpool3.Size = new System.Drawing.Size(105, 23);
            this.ComprarMicroWhirlpool3.TabIndex = 77;
            this.ComprarMicroWhirlpool3.Text = "Comprar";
            this.ComprarMicroWhirlpool3.UseVisualStyleBackColor = true;
            // 
            // AgregarMicroWhirlpool3
            // 
            this.AgregarMicroWhirlpool3.Location = new System.Drawing.Point(368, 662);
            this.AgregarMicroWhirlpool3.Name = "AgregarMicroWhirlpool3";
            this.AgregarMicroWhirlpool3.Size = new System.Drawing.Size(105, 23);
            this.AgregarMicroWhirlpool3.TabIndex = 76;
            this.AgregarMicroWhirlpool3.Text = "Agregar al carrito";
            this.AgregarMicroWhirlpool3.UseVisualStyleBackColor = true;
            // 
            // AgregarMicroWhirlpool2
            // 
            this.AgregarMicroWhirlpool2.Location = new System.Drawing.Point(368, 471);
            this.AgregarMicroWhirlpool2.Name = "AgregarMicroWhirlpool2";
            this.AgregarMicroWhirlpool2.Size = new System.Drawing.Size(105, 23);
            this.AgregarMicroWhirlpool2.TabIndex = 75;
            this.AgregarMicroWhirlpool2.Text = "Agregar al carrito";
            this.AgregarMicroWhirlpool2.UseVisualStyleBackColor = true;
            // 
            // ComprarMicroWhirlpool2
            // 
            this.ComprarMicroWhirlpool2.Location = new System.Drawing.Point(229, 471);
            this.ComprarMicroWhirlpool2.Name = "ComprarMicroWhirlpool2";
            this.ComprarMicroWhirlpool2.Size = new System.Drawing.Size(105, 23);
            this.ComprarMicroWhirlpool2.TabIndex = 74;
            this.ComprarMicroWhirlpool2.Text = "Comprar";
            this.ComprarMicroWhirlpool2.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(237, 599);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(181, 39);
            this.label12.TabIndex = 73;
            this.label12.Text = "10 Niveles de potencia\r\nFunciones predeterminadas\r\nDescongelamiento por peso y ti" +
    "empo";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(227, 574);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 15);
            this.label11.TabIndex = 72;
            this.label11.Text = "$ 2,290.00";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(226, 548);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(120, 15);
            this.label10.TabIndex = 71;
            this.label10.Text = "Marca: WHIRLPOOL";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(226, 526);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(463, 22);
            this.label9.TabIndex = 70;
            this.label9.Text = "Horno de Microondas Whirlpool 1.1 Pies Cúbicos";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(237, 411);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(205, 39);
            this.label8.TabIndex = 69;
            this.label8.Text = "- Funciones predeterminadas de alimentos\r\n- 3 opciones de descongelar por peso\r\n-" +
    " Diseño moderno";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(237, 384);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 15);
            this.label7.TabIndex = 68;
            this.label7.Text = "$ 2190.00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(237, 349);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 15);
            this.label6.TabIndex = 67;
            this.label6.Text = "Marca: WHIRLPOOL";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label5.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(242, 327);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(470, 22);
            this.label5.TabIndex = 66;
            this.label5.Text = "Horno de Microondas Whirlpool WM1511D Blanco";
            // 
            // AgregarMicroWhirlpool1
            // 
            this.AgregarMicroWhirlpool1.Location = new System.Drawing.Point(368, 262);
            this.AgregarMicroWhirlpool1.Name = "AgregarMicroWhirlpool1";
            this.AgregarMicroWhirlpool1.Size = new System.Drawing.Size(105, 23);
            this.AgregarMicroWhirlpool1.TabIndex = 65;
            this.AgregarMicroWhirlpool1.Text = "Agregar al carrito";
            this.AgregarMicroWhirlpool1.UseVisualStyleBackColor = true;
            // 
            // CompraMicroWhirlpool1
            // 
            this.CompraMicroWhirlpool1.Location = new System.Drawing.Point(229, 262);
            this.CompraMicroWhirlpool1.Name = "CompraMicroWhirlpool1";
            this.CompraMicroWhirlpool1.Size = new System.Drawing.Size(105, 23);
            this.CompraMicroWhirlpool1.TabIndex = 64;
            this.CompraMicroWhirlpool1.Text = "Comprar";
            this.CompraMicroWhirlpool1.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(237, 204);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(155, 39);
            this.label4.TabIndex = 63;
            this.label4.Text = "9 opciones para cocinar\r\nDescongela alimentos por peso\r\nBloqueo de controles";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(237, 179);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 15);
            this.label3.TabIndex = 62;
            this.label3.Text = "$ 2,035.00";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(237, 155);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 15);
            this.label2.TabIndex = 61;
            this.label2.Text = "Marca: WHIRLPOOL";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(236, 133);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(521, 22);
            this.label1.TabIndex = 60;
            this.label1.Text = "Horno de Microondas Whirlpool 1.1 Pies Cúbicos Silver";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Comercializadora.Properties.Resources.MicroWhirlpool3;
            this.pictureBox4.Location = new System.Drawing.Point(1, 526);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(216, 131);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Comercializadora.Properties.Resources.MicroWhirlpool2;
            this.pictureBox3.Location = new System.Drawing.Point(1, 327);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(216, 123);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Comercializadora.Properties.Resources.MicroWhirlpool1;
            this.pictureBox2.Location = new System.Drawing.Point(1, 133);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(216, 132);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources.whirlpool;
            this.pictureBox1.Location = new System.Drawing.Point(246, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(237, 104);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // BtRegresarMabe
            // 
            this.BtRegresarMabe.Image = global::Comercializadora.Properties.Resources.regresar3;
            this.BtRegresarMabe.Location = new System.Drawing.Point(1, 1);
            this.BtRegresarMabe.Name = "BtRegresarMabe";
            this.BtRegresarMabe.Size = new System.Drawing.Size(107, 82);
            this.BtRegresarMabe.TabIndex = 141;
            this.BtRegresarMabe.UseVisualStyleBackColor = true;
            this.BtRegresarMabe.Click += new System.EventHandler(this.BtRegresarMabe_Click);
            // 
            // MicroWhirlpool
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(820, 719);
            this.Controls.Add(this.BtRegresarMabe);
            this.Controls.Add(this.ComprarMicroWhirlpool3);
            this.Controls.Add(this.AgregarMicroWhirlpool3);
            this.Controls.Add(this.AgregarMicroWhirlpool2);
            this.Controls.Add(this.ComprarMicroWhirlpool2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.AgregarMicroWhirlpool1);
            this.Controls.Add(this.CompraMicroWhirlpool1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "MicroWhirlpool";
            this.Text = "v";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Button ComprarMicroWhirlpool3;
        private System.Windows.Forms.Button AgregarMicroWhirlpool3;
        private System.Windows.Forms.Button AgregarMicroWhirlpool2;
        private System.Windows.Forms.Button ComprarMicroWhirlpool2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button AgregarMicroWhirlpool1;
        private System.Windows.Forms.Button CompraMicroWhirlpool1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtRegresarMabe;
    }
}