﻿namespace Comercializadora
{
    partial class GuitarrasYBajosPag3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDescripcionBIbanezAcu = new System.Windows.Forms.Label();
            this.lblPrecioBIbanezAcu = new System.Windows.Forms.Label();
            this.lblDescripcionBFenderAcu = new System.Windows.Forms.Label();
            this.lblPrecioBFenderAcu = new System.Windows.Forms.Label();
            this.lblDescripcionUku = new System.Windows.Forms.Label();
            this.btnBIbanezAcu = new System.Windows.Forms.Button();
            this.btnBFenderAcu = new System.Windows.Forms.Button();
            this.comboBoxBFenderAcu = new System.Windows.Forms.ComboBox();
            this.comboBoxBIbanezAcu = new System.Windows.Forms.ComboBox();
            this.lblDescripcionFenderbPart2 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblPrecioUku = new System.Windows.Forms.Label();
            this.comboBoxUku = new System.Windows.Forms.ComboBox();
            this.btnUku = new System.Windows.Forms.Button();
            this.btnPag1 = new System.Windows.Forms.Button();
            this.btnPag2 = new System.Windows.Forms.Button();
            this.lblPag3 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDescripcionBIbanezAcu
            // 
            this.lblDescripcionBIbanezAcu.AutoSize = true;
            this.lblDescripcionBIbanezAcu.Location = new System.Drawing.Point(168, 79);
            this.lblDescripcionBIbanezAcu.Name = "lblDescripcionBIbanezAcu";
            this.lblDescripcionBIbanezAcu.Size = new System.Drawing.Size(296, 13);
            this.lblDescripcionBIbanezAcu.TabIndex = 2;
            this.lblDescripcionBIbanezAcu.Text = "Bajo Acústico / Electroacústico Ibanez Natural De 5 Cuerdas";
            // 
            // lblPrecioBIbanezAcu
            // 
            this.lblPrecioBIbanezAcu.AutoSize = true;
            this.lblPrecioBIbanezAcu.Location = new System.Drawing.Point(168, 104);
            this.lblPrecioBIbanezAcu.Name = "lblPrecioBIbanezAcu";
            this.lblPrecioBIbanezAcu.Size = new System.Drawing.Size(40, 13);
            this.lblPrecioBIbanezAcu.TabIndex = 3;
            this.lblPrecioBIbanezAcu.Text = "$7,945";
            // 
            // lblDescripcionBFenderAcu
            // 
            this.lblDescripcionBFenderAcu.AutoSize = true;
            this.lblDescripcionBFenderAcu.Location = new System.Drawing.Point(168, 177);
            this.lblDescripcionBFenderAcu.Name = "lblDescripcionBFenderAcu";
            this.lblDescripcionBFenderAcu.Size = new System.Drawing.Size(387, 13);
            this.lblDescripcionBFenderAcu.TabIndex = 4;
            this.lblDescripcionBFenderAcu.Text = "Bajo Acústico / Electroacústico Fender Tbucket Bass E Rosewood Fingerboard ";
            // 
            // lblPrecioBFenderAcu
            // 
            this.lblPrecioBFenderAcu.AutoSize = true;
            this.lblPrecioBFenderAcu.Location = new System.Drawing.Point(168, 214);
            this.lblPrecioBFenderAcu.Name = "lblPrecioBFenderAcu";
            this.lblPrecioBFenderAcu.Size = new System.Drawing.Size(40, 13);
            this.lblPrecioBFenderAcu.TabIndex = 5;
            this.lblPrecioBFenderAcu.Text = "$6,541";
            // 
            // lblDescripcionUku
            // 
            this.lblDescripcionUku.AutoSize = true;
            this.lblDescripcionUku.Location = new System.Drawing.Point(168, 297);
            this.lblDescripcionUku.Name = "lblDescripcionUku";
            this.lblDescripcionUku.Size = new System.Drawing.Size(360, 13);
            this.lblDescripcionUku.TabIndex = 6;
            this.lblDescripcionUku.Text = "Ukulel Fender Rincon Tenor Ukulele Ovangkol Fingerboard Natural w/Bag";
            // 
            // btnBIbanezAcu
            // 
            this.btnBIbanezAcu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBIbanezAcu.Location = new System.Drawing.Point(673, 92);
            this.btnBIbanezAcu.Name = "btnBIbanezAcu";
            this.btnBIbanezAcu.Size = new System.Drawing.Size(75, 23);
            this.btnBIbanezAcu.TabIndex = 7;
            this.btnBIbanezAcu.Text = "Agregar";
            this.btnBIbanezAcu.UseVisualStyleBackColor = true;
            // 
            // btnBFenderAcu
            // 
            this.btnBFenderAcu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBFenderAcu.Location = new System.Drawing.Point(673, 198);
            this.btnBFenderAcu.Name = "btnBFenderAcu";
            this.btnBFenderAcu.Size = new System.Drawing.Size(75, 23);
            this.btnBFenderAcu.TabIndex = 8;
            this.btnBFenderAcu.Text = "Agregar";
            this.btnBFenderAcu.UseVisualStyleBackColor = true;
            // 
            // comboBoxBFenderAcu
            // 
            this.comboBoxBFenderAcu.FormattingEnabled = true;
            this.comboBoxBFenderAcu.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxBFenderAcu.Location = new System.Drawing.Point(611, 200);
            this.comboBoxBFenderAcu.Name = "comboBoxBFenderAcu";
            this.comboBoxBFenderAcu.Size = new System.Drawing.Size(44, 21);
            this.comboBoxBFenderAcu.TabIndex = 9;
            // 
            // comboBoxBIbanezAcu
            // 
            this.comboBoxBIbanezAcu.FormattingEnabled = true;
            this.comboBoxBIbanezAcu.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxBIbanezAcu.Location = new System.Drawing.Point(611, 94);
            this.comboBoxBIbanezAcu.Name = "comboBoxBIbanezAcu";
            this.comboBoxBIbanezAcu.Size = new System.Drawing.Size(44, 21);
            this.comboBoxBIbanezAcu.TabIndex = 10;
            // 
            // lblDescripcionFenderbPart2
            // 
            this.lblDescripcionFenderbPart2.AutoSize = true;
            this.lblDescripcionFenderbPart2.Location = new System.Drawing.Point(168, 193);
            this.lblDescripcionFenderbPart2.Name = "lblDescripcionFenderbPart2";
            this.lblDescripcionFenderbPart2.Size = new System.Drawing.Size(145, 13);
            this.lblDescripcionFenderbPart2.TabIndex = 11;
            this.lblDescripcionFenderbPart2.Text = "3Color Sunburst Flame Maple";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Comercializadora.Properties.Resources._0971662121;
            this.pictureBox3.Location = new System.Drawing.Point(30, 261);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(103, 103);
            this.pictureBox3.TabIndex = 12;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Comercializadora.Properties.Resources.FenderAcusticoaj;
            this.pictureBox2.Location = new System.Drawing.Point(30, 152);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(103, 103);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources.IBanezAcustiBa;
            this.pictureBox1.Location = new System.Drawing.Point(30, 44);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(103, 102);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // lblPrecioUku
            // 
            this.lblPrecioUku.AutoSize = true;
            this.lblPrecioUku.Location = new System.Drawing.Point(168, 329);
            this.lblPrecioUku.Name = "lblPrecioUku";
            this.lblPrecioUku.Size = new System.Drawing.Size(40, 13);
            this.lblPrecioUku.TabIndex = 13;
            this.lblPrecioUku.Text = "$3,442";
            // 
            // comboBoxUku
            // 
            this.comboBoxUku.FormattingEnabled = true;
            this.comboBoxUku.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxUku.Location = new System.Drawing.Point(611, 310);
            this.comboBoxUku.Name = "comboBoxUku";
            this.comboBoxUku.Size = new System.Drawing.Size(44, 21);
            this.comboBoxUku.TabIndex = 14;
            // 
            // btnUku
            // 
            this.btnUku.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUku.Location = new System.Drawing.Point(673, 308);
            this.btnUku.Name = "btnUku";
            this.btnUku.Size = new System.Drawing.Size(75, 23);
            this.btnUku.TabIndex = 15;
            this.btnUku.Text = "Agregar";
            this.btnUku.UseVisualStyleBackColor = true;
            // 
            // btnPag1
            // 
            this.btnPag1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPag1.Location = new System.Drawing.Point(354, 412);
            this.btnPag1.Name = "btnPag1";
            this.btnPag1.Size = new System.Drawing.Size(27, 23);
            this.btnPag1.TabIndex = 16;
            this.btnPag1.Text = "1";
            this.btnPag1.UseVisualStyleBackColor = true;
            this.btnPag1.Click += new System.EventHandler(this.btnPag1_Click);
            // 
            // btnPag2
            // 
            this.btnPag2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPag2.Location = new System.Drawing.Point(387, 412);
            this.btnPag2.Name = "btnPag2";
            this.btnPag2.Size = new System.Drawing.Size(27, 23);
            this.btnPag2.TabIndex = 17;
            this.btnPag2.Text = "2";
            this.btnPag2.UseVisualStyleBackColor = true;
            this.btnPag2.Click += new System.EventHandler(this.btnPag2_Click);
            // 
            // lblPag3
            // 
            this.lblPag3.AutoSize = true;
            this.lblPag3.Location = new System.Drawing.Point(420, 417);
            this.lblPag3.Name = "lblPag3";
            this.lblPag3.Size = new System.Drawing.Size(13, 13);
            this.lblPag3.TabIndex = 18;
            this.lblPag3.Text = "3";
            this.lblPag3.Click += new System.EventHandler(this.lblPag3_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox4.Image = global::Comercializadora.Properties.Resources._2569f;
            this.pictureBox4.Location = new System.Drawing.Point(721, 403);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(27, 27);
            this.pictureBox4.TabIndex = 19;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // GuitarrasYBajosPag3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.lblPag3);
            this.Controls.Add(this.btnPag2);
            this.Controls.Add(this.btnPag1);
            this.Controls.Add(this.btnUku);
            this.Controls.Add(this.comboBoxUku);
            this.Controls.Add(this.lblPrecioUku);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.lblDescripcionFenderbPart2);
            this.Controls.Add(this.comboBoxBIbanezAcu);
            this.Controls.Add(this.comboBoxBFenderAcu);
            this.Controls.Add(this.btnBFenderAcu);
            this.Controls.Add(this.btnBIbanezAcu);
            this.Controls.Add(this.lblDescripcionUku);
            this.Controls.Add(this.lblPrecioBFenderAcu);
            this.Controls.Add(this.lblDescripcionBFenderAcu);
            this.Controls.Add(this.lblPrecioBIbanezAcu);
            this.Controls.Add(this.lblDescripcionBIbanezAcu);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "GuitarrasYBajosPag3";
            this.Text = "GuitarrasYBajosPag3";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblDescripcionBIbanezAcu;
        private System.Windows.Forms.Label lblPrecioBIbanezAcu;
        private System.Windows.Forms.Label lblDescripcionBFenderAcu;
        private System.Windows.Forms.Label lblPrecioBFenderAcu;
        private System.Windows.Forms.Label lblDescripcionUku;
        private System.Windows.Forms.Button btnBIbanezAcu;
        private System.Windows.Forms.Button btnBFenderAcu;
        private System.Windows.Forms.ComboBox comboBoxBFenderAcu;
        private System.Windows.Forms.ComboBox comboBoxBIbanezAcu;
        private System.Windows.Forms.Label lblDescripcionFenderbPart2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label lblPrecioUku;
        private System.Windows.Forms.ComboBox comboBoxUku;
        private System.Windows.Forms.Button btnUku;
        private System.Windows.Forms.Button btnPag1;
        private System.Windows.Forms.Button btnPag2;
        private System.Windows.Forms.Label lblPag3;
        private System.Windows.Forms.PictureBox pictureBox4;
    }
}