﻿namespace Comercializadora
{
    partial class PianosPag1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDescripcionPYamahaDigi = new System.Windows.Forms.Label();
            this.lblPrecioPYamahaDigi = new System.Windows.Forms.Label();
            this.comboBoxPYamahaDigi = new System.Windows.Forms.ComboBox();
            this.btnPYamahaDigi = new System.Windows.Forms.Button();
            this.lblDescripcionPCasioDigi = new System.Windows.Forms.Label();
            this.lblPrecioPCasioDigi = new System.Windows.Forms.Label();
            this.btnPCasioDigi = new System.Windows.Forms.Button();
            this.comboBoxPCasioDigi = new System.Windows.Forms.ComboBox();
            this.lblDescripcionPYamahaDigi2 = new System.Windows.Forms.Label();
            this.lblPrecioPYamahaDigi2 = new System.Windows.Forms.Label();
            this.btnPYamahaDigi2 = new System.Windows.Forms.Button();
            this.comboBoxPYamahaDigi2 = new System.Windows.Forms.ComboBox();
            this.lblPianoPag1 = new System.Windows.Forms.Label();
            this.btnPianoPag2 = new System.Windows.Forms.Button();
            this.btnPianoPag3 = new System.Windows.Forms.Button();
            this.pictureBoxPYamahaDigi2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxPCasioDigi = new System.Windows.Forms.PictureBox();
            this.pictureBoxPYamahaDigi = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPYamahaDigi2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPCasioDigi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPYamahaDigi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDescripcionPYamahaDigi
            // 
            this.lblDescripcionPYamahaDigi.AutoSize = true;
            this.lblDescripcionPYamahaDigi.Location = new System.Drawing.Point(168, 79);
            this.lblDescripcionPYamahaDigi.Name = "lblDescripcionPYamahaDigi";
            this.lblDescripcionPYamahaDigi.Size = new System.Drawing.Size(265, 13);
            this.lblDescripcionPYamahaDigi.TabIndex = 1;
            this.lblDescripcionPYamahaDigi.Text = "Pianos Digital Yamaha Clavinova Clp Mod. NCLP535R";
            // 
            // lblPrecioPYamahaDigi
            // 
            this.lblPrecioPYamahaDigi.AutoSize = true;
            this.lblPrecioPYamahaDigi.Location = new System.Drawing.Point(168, 104);
            this.lblPrecioPYamahaDigi.Name = "lblPrecioPYamahaDigi";
            this.lblPrecioPYamahaDigi.Size = new System.Drawing.Size(46, 13);
            this.lblPrecioPYamahaDigi.TabIndex = 2;
            this.lblPrecioPYamahaDigi.Text = "$33,299";
            // 
            // comboBoxPYamahaDigi
            // 
            this.comboBoxPYamahaDigi.FormattingEnabled = true;
            this.comboBoxPYamahaDigi.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxPYamahaDigi.Location = new System.Drawing.Point(611, 94);
            this.comboBoxPYamahaDigi.Name = "comboBoxPYamahaDigi";
            this.comboBoxPYamahaDigi.Size = new System.Drawing.Size(44, 21);
            this.comboBoxPYamahaDigi.TabIndex = 3;
            // 
            // btnPYamahaDigi
            // 
            this.btnPYamahaDigi.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPYamahaDigi.Location = new System.Drawing.Point(673, 92);
            this.btnPYamahaDigi.Name = "btnPYamahaDigi";
            this.btnPYamahaDigi.Size = new System.Drawing.Size(75, 23);
            this.btnPYamahaDigi.TabIndex = 4;
            this.btnPYamahaDigi.Text = "Agregar";
            this.btnPYamahaDigi.UseVisualStyleBackColor = true;
            // 
            // lblDescripcionPCasioDigi
            // 
            this.lblDescripcionPCasioDigi.AutoSize = true;
            this.lblDescripcionPCasioDigi.Location = new System.Drawing.Point(168, 188);
            this.lblDescripcionPCasioDigi.Name = "lblDescripcionPCasioDigi";
            this.lblDescripcionPCasioDigi.Size = new System.Drawing.Size(233, 13);
            this.lblDescripcionPCasioDigi.TabIndex = 6;
            this.lblDescripcionPCasioDigi.Text = "Pianos Digital Casio Piano Casio Digital Px5Swe";
            // 
            // lblPrecioPCasioDigi
            // 
            this.lblPrecioPCasioDigi.AutoSize = true;
            this.lblPrecioPCasioDigi.Location = new System.Drawing.Point(168, 214);
            this.lblPrecioPCasioDigi.Name = "lblPrecioPCasioDigi";
            this.lblPrecioPCasioDigi.Size = new System.Drawing.Size(46, 13);
            this.lblPrecioPCasioDigi.TabIndex = 7;
            this.lblPrecioPCasioDigi.Text = "$22,883";
            // 
            // btnPCasioDigi
            // 
            this.btnPCasioDigi.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPCasioDigi.Location = new System.Drawing.Point(673, 198);
            this.btnPCasioDigi.Name = "btnPCasioDigi";
            this.btnPCasioDigi.Size = new System.Drawing.Size(75, 23);
            this.btnPCasioDigi.TabIndex = 8;
            this.btnPCasioDigi.Text = "Agregar";
            this.btnPCasioDigi.UseVisualStyleBackColor = true;
            // 
            // comboBoxPCasioDigi
            // 
            this.comboBoxPCasioDigi.FormattingEnabled = true;
            this.comboBoxPCasioDigi.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxPCasioDigi.Location = new System.Drawing.Point(611, 200);
            this.comboBoxPCasioDigi.Name = "comboBoxPCasioDigi";
            this.comboBoxPCasioDigi.Size = new System.Drawing.Size(44, 21);
            this.comboBoxPCasioDigi.TabIndex = 9;
            // 
            // lblDescripcionPYamahaDigi2
            // 
            this.lblDescripcionPYamahaDigi2.AutoSize = true;
            this.lblDescripcionPYamahaDigi2.Location = new System.Drawing.Point(168, 297);
            this.lblDescripcionPYamahaDigi2.Name = "lblDescripcionPYamahaDigi2";
            this.lblDescripcionPYamahaDigi2.Size = new System.Drawing.Size(278, 13);
            this.lblDescripcionPYamahaDigi2.TabIndex = 11;
            this.lblDescripcionPYamahaDigi2.Text = "Pianos Digital Yamaha Portatil Piaggero Mod. SNPV60PA";
            // 
            // lblPrecioPYamahaDigi2
            // 
            this.lblPrecioPYamahaDigi2.AutoSize = true;
            this.lblPrecioPYamahaDigi2.Location = new System.Drawing.Point(168, 329);
            this.lblPrecioPYamahaDigi2.Name = "lblPrecioPYamahaDigi2";
            this.lblPrecioPYamahaDigi2.Size = new System.Drawing.Size(40, 13);
            this.lblPrecioPYamahaDigi2.TabIndex = 12;
            this.lblPrecioPYamahaDigi2.Text = "$9,605";
            // 
            // btnPYamahaDigi2
            // 
            this.btnPYamahaDigi2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPYamahaDigi2.Location = new System.Drawing.Point(673, 308);
            this.btnPYamahaDigi2.Name = "btnPYamahaDigi2";
            this.btnPYamahaDigi2.Size = new System.Drawing.Size(75, 23);
            this.btnPYamahaDigi2.TabIndex = 13;
            this.btnPYamahaDigi2.Text = "Agregar";
            this.btnPYamahaDigi2.UseVisualStyleBackColor = true;
            // 
            // comboBoxPYamahaDigi2
            // 
            this.comboBoxPYamahaDigi2.FormattingEnabled = true;
            this.comboBoxPYamahaDigi2.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxPYamahaDigi2.Location = new System.Drawing.Point(611, 310);
            this.comboBoxPYamahaDigi2.Name = "comboBoxPYamahaDigi2";
            this.comboBoxPYamahaDigi2.Size = new System.Drawing.Size(44, 21);
            this.comboBoxPYamahaDigi2.TabIndex = 14;
            // 
            // lblPianoPag1
            // 
            this.lblPianoPag1.AutoSize = true;
            this.lblPianoPag1.Location = new System.Drawing.Point(370, 417);
            this.lblPianoPag1.Name = "lblPianoPag1";
            this.lblPianoPag1.Size = new System.Drawing.Size(13, 13);
            this.lblPianoPag1.TabIndex = 15;
            this.lblPianoPag1.Text = "1";
            // 
            // btnPianoPag2
            // 
            this.btnPianoPag2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPianoPag2.Location = new System.Drawing.Point(392, 412);
            this.btnPianoPag2.Name = "btnPianoPag2";
            this.btnPianoPag2.Size = new System.Drawing.Size(27, 23);
            this.btnPianoPag2.TabIndex = 16;
            this.btnPianoPag2.Text = "2";
            this.btnPianoPag2.UseVisualStyleBackColor = true;
            this.btnPianoPag2.Click += new System.EventHandler(this.btnPianoPag2_Click);
            // 
            // btnPianoPag3
            // 
            this.btnPianoPag3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPianoPag3.Location = new System.Drawing.Point(425, 412);
            this.btnPianoPag3.Name = "btnPianoPag3";
            this.btnPianoPag3.Size = new System.Drawing.Size(27, 23);
            this.btnPianoPag3.TabIndex = 17;
            this.btnPianoPag3.Text = "3";
            this.btnPianoPag3.UseVisualStyleBackColor = true;
            this.btnPianoPag3.Click += new System.EventHandler(this.btnPianoPag3_Click);
            // 
            // pictureBoxPYamahaDigi2
            // 
            this.pictureBoxPYamahaDigi2.Image = global::Comercializadora.Properties.Resources.SNPV60PA;
            this.pictureBoxPYamahaDigi2.Location = new System.Drawing.Point(30, 259);
            this.pictureBoxPYamahaDigi2.Name = "pictureBoxPYamahaDigi2";
            this.pictureBoxPYamahaDigi2.Size = new System.Drawing.Size(101, 102);
            this.pictureBoxPYamahaDigi2.TabIndex = 10;
            this.pictureBoxPYamahaDigi2.TabStop = false;
            // 
            // pictureBoxPCasioDigi
            // 
            this.pictureBoxPCasioDigi.Image = global::Comercializadora.Properties.Resources.ITCASPX5WE;
            this.pictureBoxPCasioDigi.Location = new System.Drawing.Point(30, 151);
            this.pictureBoxPCasioDigi.Name = "pictureBoxPCasioDigi";
            this.pictureBoxPCasioDigi.Size = new System.Drawing.Size(101, 102);
            this.pictureBoxPCasioDigi.TabIndex = 5;
            this.pictureBoxPCasioDigi.TabStop = false;
            // 
            // pictureBoxPYamahaDigi
            // 
            this.pictureBoxPYamahaDigi.Image = global::Comercializadora.Properties.Resources.NCLP535R;
            this.pictureBoxPYamahaDigi.Location = new System.Drawing.Point(30, 44);
            this.pictureBoxPYamahaDigi.Name = "pictureBoxPYamahaDigi";
            this.pictureBoxPYamahaDigi.Size = new System.Drawing.Size(101, 101);
            this.pictureBoxPYamahaDigi.TabIndex = 0;
            this.pictureBoxPYamahaDigi.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources._2569f;
            this.pictureBox1.Location = new System.Drawing.Point(721, 402);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(27, 28);
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // PianosPag1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 465);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnPianoPag3);
            this.Controls.Add(this.btnPianoPag2);
            this.Controls.Add(this.lblPianoPag1);
            this.Controls.Add(this.comboBoxPYamahaDigi2);
            this.Controls.Add(this.btnPYamahaDigi2);
            this.Controls.Add(this.lblPrecioPYamahaDigi2);
            this.Controls.Add(this.lblDescripcionPYamahaDigi2);
            this.Controls.Add(this.pictureBoxPYamahaDigi2);
            this.Controls.Add(this.comboBoxPCasioDigi);
            this.Controls.Add(this.btnPCasioDigi);
            this.Controls.Add(this.lblPrecioPCasioDigi);
            this.Controls.Add(this.lblDescripcionPCasioDigi);
            this.Controls.Add(this.pictureBoxPCasioDigi);
            this.Controls.Add(this.btnPYamahaDigi);
            this.Controls.Add(this.comboBoxPYamahaDigi);
            this.Controls.Add(this.lblPrecioPYamahaDigi);
            this.Controls.Add(this.lblDescripcionPYamahaDigi);
            this.Controls.Add(this.pictureBoxPYamahaDigi);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PianosPag1";
            this.Text = "PianosPag1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPYamahaDigi2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPCasioDigi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPYamahaDigi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxPYamahaDigi;
        private System.Windows.Forms.Label lblDescripcionPYamahaDigi;
        private System.Windows.Forms.Label lblPrecioPYamahaDigi;
        private System.Windows.Forms.ComboBox comboBoxPYamahaDigi;
        private System.Windows.Forms.Button btnPYamahaDigi;
        private System.Windows.Forms.PictureBox pictureBoxPCasioDigi;
        private System.Windows.Forms.Label lblDescripcionPCasioDigi;
        private System.Windows.Forms.Label lblPrecioPCasioDigi;
        private System.Windows.Forms.Button btnPCasioDigi;
        private System.Windows.Forms.ComboBox comboBoxPCasioDigi;
        private System.Windows.Forms.PictureBox pictureBoxPYamahaDigi2;
        private System.Windows.Forms.Label lblDescripcionPYamahaDigi2;
        private System.Windows.Forms.Label lblPrecioPYamahaDigi2;
        private System.Windows.Forms.Button btnPYamahaDigi2;
        private System.Windows.Forms.ComboBox comboBoxPYamahaDigi2;
        private System.Windows.Forms.Label lblPianoPag1;
        private System.Windows.Forms.Button btnPianoPag2;
        private System.Windows.Forms.Button btnPianoPag3;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}