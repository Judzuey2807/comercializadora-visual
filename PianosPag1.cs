﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comercializadora
{
    public partial class PianosPag1 : Form
    {
        public PianosPag1()
        {
            InitializeComponent();
        }

        private void btnPianoPag2_Click(object sender, EventArgs e)
        {
            this.Close();
            PianosPag2 pp2 = new PianosPag2();
            pp2.Show();
        }

        private void btnPianoPag3_Click(object sender, EventArgs e)
        {
            this.Close();
            PianosPag3 pp3 = new PianosPag3();
            pp3.Show();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
