﻿namespace Comercializadora
{
    partial class Alcohol
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Alcohol));
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnRegresar = new System.Windows.Forms.Button();
            this.pictureBoxTeclado = new System.Windows.Forms.PictureBox();
            this.lblBateria = new System.Windows.Forms.Label();
            this.pictureBoxBateria = new System.Windows.Forms.PictureBox();
            this.lblTeclados = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTeclado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBateria)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 25);
            this.label1.TabIndex = 2;
            this.label1.Text = "Vinos y Licores";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Controls.Add(this.btnRegresar);
            this.groupBox1.Controls.Add(this.pictureBoxTeclado);
            this.groupBox1.Controls.Add(this.lblBateria);
            this.groupBox1.Controls.Add(this.pictureBoxBateria);
            this.groupBox1.Controls.Add(this.lblTeclados);
            this.groupBox1.Font = new System.Drawing.Font("Century Gothic", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.groupBox1.Location = new System.Drawing.Point(2, 37);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(486, 351);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Menu";
            // 
            // btnRegresar
            // 
            this.btnRegresar.Font = new System.Drawing.Font("Microsoft New Tai Lue", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegresar.Location = new System.Drawing.Point(207, 293);
            this.btnRegresar.Name = "btnRegresar";
            this.btnRegresar.Size = new System.Drawing.Size(75, 23);
            this.btnRegresar.TabIndex = 15;
            this.btnRegresar.Text = "Regresar";
            this.btnRegresar.UseVisualStyleBackColor = true;
            this.btnRegresar.Click += new System.EventHandler(this.btnRegresar_Click);
            // 
            // pictureBoxTeclado
            // 
            this.pictureBoxTeclado.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxTeclado.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxTeclado.Image")));
            this.pictureBoxTeclado.Location = new System.Drawing.Point(295, 103);
            this.pictureBoxTeclado.Name = "pictureBoxTeclado";
            this.pictureBoxTeclado.Size = new System.Drawing.Size(101, 103);
            this.pictureBoxTeclado.TabIndex = 12;
            this.pictureBoxTeclado.TabStop = false;
            this.pictureBoxTeclado.Click += new System.EventHandler(this.pictureBoxTeclado_Click);
            // 
            // lblBateria
            // 
            this.lblBateria.AutoSize = true;
            this.lblBateria.Font = new System.Drawing.Font("Century Gothic", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBateria.Location = new System.Drawing.Point(79, 209);
            this.lblBateria.Name = "lblBateria";
            this.lblBateria.Size = new System.Drawing.Size(112, 15);
            this.lblBateria.TabIndex = 14;
            this.lblBateria.Text = "Licores y Destilados";
            // 
            // pictureBoxBateria
            // 
            this.pictureBoxBateria.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBoxBateria.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxBateria.Image")));
            this.pictureBoxBateria.Location = new System.Drawing.Point(82, 103);
            this.pictureBoxBateria.Name = "pictureBoxBateria";
            this.pictureBoxBateria.Size = new System.Drawing.Size(103, 103);
            this.pictureBoxBateria.TabIndex = 13;
            this.pictureBoxBateria.TabStop = false;
            this.pictureBoxBateria.Click += new System.EventHandler(this.pictureBoxBateria_Click);
            // 
            // lblTeclados
            // 
            this.lblTeclados.AutoSize = true;
            this.lblTeclados.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblTeclados.Font = new System.Drawing.Font("Century Gothic", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTeclados.Location = new System.Drawing.Point(326, 209);
            this.lblTeclados.Name = "lblTeclados";
            this.lblTeclados.Size = new System.Drawing.Size(37, 15);
            this.lblTeclados.TabIndex = 11;
            this.lblTeclados.Text = "Vinos";
            // 
            // Alcohol
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 400);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Alcohol";
            this.Text = "Licores";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTeclado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBateria)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnRegresar;
        private System.Windows.Forms.PictureBox pictureBoxTeclado;
        private System.Windows.Forms.Label lblBateria;
        private System.Windows.Forms.PictureBox pictureBoxBateria;
        private System.Windows.Forms.Label lblTeclados;
    }
}