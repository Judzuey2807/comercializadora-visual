﻿namespace Comercializadora
{
    partial class PercusionesPag2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDescripcionSample = new System.Windows.Forms.Label();
            this.lblPrecioSample = new System.Windows.Forms.Label();
            this.btnSample = new System.Windows.Forms.Button();
            this.comboBoxSample = new System.Windows.Forms.ComboBox();
            this.lblDescripcionPZil = new System.Windows.Forms.Label();
            this.lblPrecioPZil = new System.Windows.Forms.Label();
            this.btnPZil = new System.Windows.Forms.Button();
            this.comboBoxPZil = new System.Windows.Forms.ComboBox();
            this.lblDescripcionPZilBronce = new System.Windows.Forms.Label();
            this.lblPrecioZilBronce = new System.Windows.Forms.Label();
            this.comboBoxPZilBronce = new System.Windows.Forms.ComboBox();
            this.btnPZilBronce = new System.Windows.Forms.Button();
            this.btnBPag1 = new System.Windows.Forms.Button();
            this.btnBPag3 = new System.Windows.Forms.Button();
            this.lblBPag2 = new System.Windows.Forms.Label();
            this.pictureBoxPZilBronce = new System.Windows.Forms.PictureBox();
            this.pictureBoxPZil = new System.Windows.Forms.PictureBox();
            this.pictureBoxSample = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPZilBronce)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPZil)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDescripcionSample
            // 
            this.lblDescripcionSample.AutoSize = true;
            this.lblDescripcionSample.Location = new System.Drawing.Point(168, 79);
            this.lblDescripcionSample.Name = "lblDescripcionSample";
            this.lblDescripcionSample.Size = new System.Drawing.Size(128, 13);
            this.lblDescripcionSample.TabIndex = 1;
            this.lblDescripcionSample.Text = "Alesis SAMPLEPAD PRO";
            // 
            // lblPrecioSample
            // 
            this.lblPrecioSample.AutoSize = true;
            this.lblPrecioSample.Location = new System.Drawing.Point(168, 104);
            this.lblPrecioSample.Name = "lblPrecioSample";
            this.lblPrecioSample.Size = new System.Drawing.Size(40, 13);
            this.lblPrecioSample.TabIndex = 2;
            this.lblPrecioSample.Text = "$6,960";
            // 
            // btnSample
            // 
            this.btnSample.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSample.Location = new System.Drawing.Point(673, 92);
            this.btnSample.Name = "btnSample";
            this.btnSample.Size = new System.Drawing.Size(75, 23);
            this.btnSample.TabIndex = 3;
            this.btnSample.Text = "Agregar";
            this.btnSample.UseVisualStyleBackColor = true;
            // 
            // comboBoxSample
            // 
            this.comboBoxSample.FormattingEnabled = true;
            this.comboBoxSample.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxSample.Location = new System.Drawing.Point(611, 94);
            this.comboBoxSample.Name = "comboBoxSample";
            this.comboBoxSample.Size = new System.Drawing.Size(44, 21);
            this.comboBoxSample.TabIndex = 4;
            // 
            // lblDescripcionPZil
            // 
            this.lblDescripcionPZil.AutoSize = true;
            this.lblDescripcionPZil.Location = new System.Drawing.Point(168, 188);
            this.lblDescripcionPZil.Name = "lblDescripcionPZil";
            this.lblDescripcionPZil.Size = new System.Drawing.Size(157, 13);
            this.lblDescripcionPZil.TabIndex = 6;
            this.lblDescripcionPZil.Text = "Set de platillos, Zildjian PLZ4PK";
            // 
            // lblPrecioPZil
            // 
            this.lblPrecioPZil.AutoSize = true;
            this.lblPrecioPZil.Location = new System.Drawing.Point(168, 214);
            this.lblPrecioPZil.Name = "lblPrecioPZil";
            this.lblPrecioPZil.Size = new System.Drawing.Size(40, 13);
            this.lblPrecioPZil.TabIndex = 7;
            this.lblPrecioPZil.Text = "$4,040";
            // 
            // btnPZil
            // 
            this.btnPZil.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPZil.Location = new System.Drawing.Point(673, 198);
            this.btnPZil.Name = "btnPZil";
            this.btnPZil.Size = new System.Drawing.Size(75, 23);
            this.btnPZil.TabIndex = 8;
            this.btnPZil.Text = "Agregar";
            this.btnPZil.UseVisualStyleBackColor = true;
            // 
            // comboBoxPZil
            // 
            this.comboBoxPZil.FormattingEnabled = true;
            this.comboBoxPZil.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxPZil.Location = new System.Drawing.Point(611, 200);
            this.comboBoxPZil.Name = "comboBoxPZil";
            this.comboBoxPZil.Size = new System.Drawing.Size(44, 21);
            this.comboBoxPZil.TabIndex = 9;
            // 
            // lblDescripcionPZilBronce
            // 
            this.lblDescripcionPZilBronce.AutoSize = true;
            this.lblDescripcionPZilBronce.Location = new System.Drawing.Point(168, 297);
            this.lblDescripcionPZilBronce.Name = "lblDescripcionPZilBronce";
            this.lblDescripcionPZilBronce.Size = new System.Drawing.Size(142, 13);
            this.lblDescripcionPZilBronce.TabIndex = 11;
            this.lblDescripcionPZilBronce.Text = "Set de platillos, Zildjian S390";
            // 
            // lblPrecioZilBronce
            // 
            this.lblPrecioZilBronce.AutoSize = true;
            this.lblPrecioZilBronce.Location = new System.Drawing.Point(168, 329);
            this.lblPrecioZilBronce.Name = "lblPrecioZilBronce";
            this.lblPrecioZilBronce.Size = new System.Drawing.Size(40, 13);
            this.lblPrecioZilBronce.TabIndex = 12;
            this.lblPrecioZilBronce.Text = "$8,700";
            // 
            // comboBoxPZilBronce
            // 
            this.comboBoxPZilBronce.FormattingEnabled = true;
            this.comboBoxPZilBronce.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxPZilBronce.Location = new System.Drawing.Point(611, 310);
            this.comboBoxPZilBronce.Name = "comboBoxPZilBronce";
            this.comboBoxPZilBronce.Size = new System.Drawing.Size(44, 21);
            this.comboBoxPZilBronce.TabIndex = 13;
            // 
            // btnPZilBronce
            // 
            this.btnPZilBronce.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPZilBronce.Location = new System.Drawing.Point(673, 308);
            this.btnPZilBronce.Name = "btnPZilBronce";
            this.btnPZilBronce.Size = new System.Drawing.Size(75, 23);
            this.btnPZilBronce.TabIndex = 14;
            this.btnPZilBronce.Text = "Agregar";
            this.btnPZilBronce.UseVisualStyleBackColor = true;
            // 
            // btnBPag1
            // 
            this.btnBPag1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBPag1.Location = new System.Drawing.Point(359, 412);
            this.btnBPag1.Name = "btnBPag1";
            this.btnBPag1.Size = new System.Drawing.Size(27, 23);
            this.btnBPag1.TabIndex = 15;
            this.btnBPag1.Text = "1";
            this.btnBPag1.UseVisualStyleBackColor = true;
            this.btnBPag1.Click += new System.EventHandler(this.btnBPag1_Click);
            // 
            // btnBPag3
            // 
            this.btnBPag3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBPag3.Location = new System.Drawing.Point(411, 412);
            this.btnBPag3.Name = "btnBPag3";
            this.btnBPag3.Size = new System.Drawing.Size(27, 23);
            this.btnBPag3.TabIndex = 16;
            this.btnBPag3.Text = "3";
            this.btnBPag3.UseVisualStyleBackColor = true;
            this.btnBPag3.Click += new System.EventHandler(this.btnBPag3_Click);
            // 
            // lblBPag2
            // 
            this.lblBPag2.AutoSize = true;
            this.lblBPag2.Location = new System.Drawing.Point(392, 417);
            this.lblBPag2.Name = "lblBPag2";
            this.lblBPag2.Size = new System.Drawing.Size(13, 13);
            this.lblBPag2.TabIndex = 17;
            this.lblBPag2.Text = "2";
            // 
            // pictureBoxPZilBronce
            // 
            this.pictureBoxPZilBronce.Image = global::Comercializadora.Properties.Resources.s390_2;
            this.pictureBoxPZilBronce.Location = new System.Drawing.Point(30, 259);
            this.pictureBoxPZilBronce.Name = "pictureBoxPZilBronce";
            this.pictureBoxPZilBronce.Size = new System.Drawing.Size(100, 103);
            this.pictureBoxPZilBronce.TabIndex = 10;
            this.pictureBoxPZilBronce.TabStop = false;
            // 
            // pictureBoxPZil
            // 
            this.pictureBoxPZil.Image = global::Comercializadora.Properties.Resources.zildjian_plz4pk_zet_de_platillos_planet_z;
            this.pictureBoxPZil.Location = new System.Drawing.Point(30, 151);
            this.pictureBoxPZil.Name = "pictureBoxPZil";
            this.pictureBoxPZil.Size = new System.Drawing.Size(100, 102);
            this.pictureBoxPZil.TabIndex = 5;
            this.pictureBoxPZil.TabStop = false;
            // 
            // pictureBoxSample
            // 
            this.pictureBoxSample.Image = global::Comercializadora.Properties.Resources.alesis_samplepadpro_01;
            this.pictureBoxSample.Location = new System.Drawing.Point(30, 44);
            this.pictureBoxSample.Name = "pictureBoxSample";
            this.pictureBoxSample.Size = new System.Drawing.Size(102, 104);
            this.pictureBoxSample.TabIndex = 0;
            this.pictureBoxSample.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources._2569f;
            this.pictureBox1.Location = new System.Drawing.Point(721, 402);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(27, 28);
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // PercusionesPag2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 465);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblBPag2);
            this.Controls.Add(this.btnBPag3);
            this.Controls.Add(this.btnBPag1);
            this.Controls.Add(this.btnPZilBronce);
            this.Controls.Add(this.comboBoxPZilBronce);
            this.Controls.Add(this.lblPrecioZilBronce);
            this.Controls.Add(this.lblDescripcionPZilBronce);
            this.Controls.Add(this.pictureBoxPZilBronce);
            this.Controls.Add(this.comboBoxPZil);
            this.Controls.Add(this.btnPZil);
            this.Controls.Add(this.lblPrecioPZil);
            this.Controls.Add(this.lblDescripcionPZil);
            this.Controls.Add(this.pictureBoxPZil);
            this.Controls.Add(this.comboBoxSample);
            this.Controls.Add(this.btnSample);
            this.Controls.Add(this.lblPrecioSample);
            this.Controls.Add(this.lblDescripcionSample);
            this.Controls.Add(this.pictureBoxSample);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PercusionesPag2";
            this.Text = "PercusionesPag2";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPZilBronce)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPZil)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSample)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxSample;
        private System.Windows.Forms.Label lblDescripcionSample;
        private System.Windows.Forms.Label lblPrecioSample;
        private System.Windows.Forms.Button btnSample;
        private System.Windows.Forms.ComboBox comboBoxSample;
        private System.Windows.Forms.PictureBox pictureBoxPZil;
        private System.Windows.Forms.Label lblDescripcionPZil;
        private System.Windows.Forms.Label lblPrecioPZil;
        private System.Windows.Forms.Button btnPZil;
        private System.Windows.Forms.ComboBox comboBoxPZil;
        private System.Windows.Forms.PictureBox pictureBoxPZilBronce;
        private System.Windows.Forms.Label lblDescripcionPZilBronce;
        private System.Windows.Forms.Label lblPrecioZilBronce;
        private System.Windows.Forms.ComboBox comboBoxPZilBronce;
        private System.Windows.Forms.Button btnPZilBronce;
        private System.Windows.Forms.Button btnBPag1;
        private System.Windows.Forms.Button btnBPag3;
        private System.Windows.Forms.Label lblBPag2;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}