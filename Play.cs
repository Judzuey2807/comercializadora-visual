﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comercializadora
{
    public partial class Play : Form
    {
        public Play()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Games x = new Games();
            x.Show();
            this.Close();
        }

        private void label7_Click(object sender, EventArgs e)
        {
            PlayC2 x = new PlayC2();
            x.Show();
            this.Close();
        }

        private void label8_Click(object sender, EventArgs e)
        {
            PlayC3 x = new PlayC3();
            x.Show();
            this.Close();
        }

        private void label9_Click(object sender, EventArgs e)
        {
            PlayC4 x = new PlayC4();
            x.Show();
            this.Close();
        }
    }
}
