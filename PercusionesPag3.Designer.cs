﻿namespace Comercializadora
{
    partial class PercusionesPag3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDescripcionBanco = new System.Windows.Forms.Label();
            this.lblPrecioBanco = new System.Windows.Forms.Label();
            this.comboBoxBanco = new System.Windows.Forms.ComboBox();
            this.btnBanco = new System.Windows.Forms.Button();
            this.lblDescripcionPedal = new System.Windows.Forms.Label();
            this.lblPrecioPedal = new System.Windows.Forms.Label();
            this.comboBoxPedal = new System.Windows.Forms.ComboBox();
            this.btnPedal = new System.Windows.Forms.Button();
            this.lblDescripcionBase = new System.Windows.Forms.Label();
            this.lblPrecioBase = new System.Windows.Forms.Label();
            this.btnBase = new System.Windows.Forms.Button();
            this.comboBoxBase = new System.Windows.Forms.ComboBox();
            this.pictureBoxBase = new System.Windows.Forms.PictureBox();
            this.pictureBoxPedal = new System.Windows.Forms.PictureBox();
            this.pictureBoxBanco = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnBPag1 = new System.Windows.Forms.Button();
            this.btnBPag2 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPedal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBanco)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDescripcionBanco
            // 
            this.lblDescripcionBanco.AutoSize = true;
            this.lblDescripcionBanco.Location = new System.Drawing.Point(168, 79);
            this.lblDescripcionBanco.Name = "lblDescripcionBanco";
            this.lblDescripcionBanco.Size = new System.Drawing.Size(156, 13);
            this.lblDescripcionBanco.TabIndex = 1;
            this.lblDescripcionBanco.Text = "Banco Bateria DW DWCP5120";
            // 
            // lblPrecioBanco
            // 
            this.lblPrecioBanco.AutoSize = true;
            this.lblPrecioBanco.Location = new System.Drawing.Point(168, 104);
            this.lblPrecioBanco.Name = "lblPrecioBanco";
            this.lblPrecioBanco.Size = new System.Drawing.Size(40, 13);
            this.lblPrecioBanco.TabIndex = 2;
            this.lblPrecioBanco.Text = "$3,090";
            // 
            // comboBoxBanco
            // 
            this.comboBoxBanco.FormattingEnabled = true;
            this.comboBoxBanco.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxBanco.Location = new System.Drawing.Point(611, 94);
            this.comboBoxBanco.Name = "comboBoxBanco";
            this.comboBoxBanco.Size = new System.Drawing.Size(44, 21);
            this.comboBoxBanco.TabIndex = 3;
            // 
            // btnBanco
            // 
            this.btnBanco.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBanco.Location = new System.Drawing.Point(673, 92);
            this.btnBanco.Name = "btnBanco";
            this.btnBanco.Size = new System.Drawing.Size(75, 23);
            this.btnBanco.TabIndex = 4;
            this.btnBanco.Text = "Agregar";
            this.btnBanco.UseVisualStyleBackColor = true;
            // 
            // lblDescripcionPedal
            // 
            this.lblDescripcionPedal.AutoSize = true;
            this.lblDescripcionPedal.Location = new System.Drawing.Point(168, 188);
            this.lblDescripcionPedal.Name = "lblDescripcionPedal";
            this.lblDescripcionPedal.Size = new System.Drawing.Size(236, 13);
            this.lblDescripcionPedal.TabIndex = 6;
            this.lblDescripcionPedal.Text = "Doble Pedal de bombo cadena DW DWCP2002";
            // 
            // lblPrecioPedal
            // 
            this.lblPrecioPedal.AutoSize = true;
            this.lblPrecioPedal.Location = new System.Drawing.Point(168, 214);
            this.lblPrecioPedal.Name = "lblPrecioPedal";
            this.lblPrecioPedal.Size = new System.Drawing.Size(40, 13);
            this.lblPrecioPedal.TabIndex = 7;
            this.lblPrecioPedal.Text = "$4,385";
            // 
            // comboBoxPedal
            // 
            this.comboBoxPedal.FormattingEnabled = true;
            this.comboBoxPedal.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxPedal.Location = new System.Drawing.Point(611, 200);
            this.comboBoxPedal.Name = "comboBoxPedal";
            this.comboBoxPedal.Size = new System.Drawing.Size(44, 21);
            this.comboBoxPedal.TabIndex = 8;
            // 
            // btnPedal
            // 
            this.btnPedal.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPedal.Location = new System.Drawing.Point(673, 198);
            this.btnPedal.Name = "btnPedal";
            this.btnPedal.Size = new System.Drawing.Size(75, 23);
            this.btnPedal.TabIndex = 9;
            this.btnPedal.Text = "Agregar";
            this.btnPedal.UseVisualStyleBackColor = true;
            // 
            // lblDescripcionBase
            // 
            this.lblDescripcionBase.AutoSize = true;
            this.lblDescripcionBase.Location = new System.Drawing.Point(168, 297);
            this.lblDescripcionBase.Name = "lblDescripcionBase";
            this.lblDescripcionBase.Size = new System.Drawing.Size(300, 13);
            this.lblDescripcionBase.TabIndex = 11;
            this.lblDescripcionBase.Text = "Base HI-HAT Estandar 2 patas concep series PDP PDHHC20";
            // 
            // lblPrecioBase
            // 
            this.lblPrecioBase.AutoSize = true;
            this.lblPrecioBase.Location = new System.Drawing.Point(168, 329);
            this.lblPrecioBase.Name = "lblPrecioBase";
            this.lblPrecioBase.Size = new System.Drawing.Size(40, 13);
            this.lblPrecioBase.TabIndex = 12;
            this.lblPrecioBase.Text = "$2,529";
            // 
            // btnBase
            // 
            this.btnBase.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBase.Location = new System.Drawing.Point(673, 308);
            this.btnBase.Name = "btnBase";
            this.btnBase.Size = new System.Drawing.Size(75, 23);
            this.btnBase.TabIndex = 13;
            this.btnBase.Text = "Agregar";
            this.btnBase.UseVisualStyleBackColor = true;
            // 
            // comboBoxBase
            // 
            this.comboBoxBase.FormattingEnabled = true;
            this.comboBoxBase.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxBase.Location = new System.Drawing.Point(611, 310);
            this.comboBoxBase.Name = "comboBoxBase";
            this.comboBoxBase.Size = new System.Drawing.Size(43, 21);
            this.comboBoxBase.TabIndex = 14;
            // 
            // pictureBoxBase
            // 
            this.pictureBoxBase.Image = global::Comercializadora.Properties.Resources.pdp_pdhhc00_concept_series_hi_hat_stand_1;
            this.pictureBoxBase.Location = new System.Drawing.Point(30, 259);
            this.pictureBoxBase.Name = "pictureBoxBase";
            this.pictureBoxBase.Size = new System.Drawing.Size(103, 101);
            this.pictureBoxBase.TabIndex = 10;
            this.pictureBoxBase.TabStop = false;
            // 
            // pictureBoxPedal
            // 
            this.pictureBoxPedal.Image = global::Comercializadora.Properties.Resources.dw_dwcp2002__1_;
            this.pictureBoxPedal.Location = new System.Drawing.Point(30, 151);
            this.pictureBoxPedal.Name = "pictureBoxPedal";
            this.pictureBoxPedal.Size = new System.Drawing.Size(101, 100);
            this.pictureBoxPedal.TabIndex = 5;
            this.pictureBoxPedal.TabStop = false;
            // 
            // pictureBoxBanco
            // 
            this.pictureBoxBanco.Image = global::Comercializadora.Properties.Resources.dw_dwcp5120__1_;
            this.pictureBoxBanco.Location = new System.Drawing.Point(30, 44);
            this.pictureBoxBanco.Name = "pictureBoxBanco";
            this.pictureBoxBanco.Size = new System.Drawing.Size(101, 103);
            this.pictureBoxBanco.TabIndex = 0;
            this.pictureBoxBanco.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(423, 417);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "3";
            // 
            // btnBPag1
            // 
            this.btnBPag1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBPag1.Location = new System.Drawing.Point(354, 412);
            this.btnBPag1.Name = "btnBPag1";
            this.btnBPag1.Size = new System.Drawing.Size(27, 23);
            this.btnBPag1.TabIndex = 16;
            this.btnBPag1.Text = "1";
            this.btnBPag1.UseVisualStyleBackColor = true;
            this.btnBPag1.Click += new System.EventHandler(this.btnBPag1_Click);
            // 
            // btnBPag2
            // 
            this.btnBPag2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBPag2.Location = new System.Drawing.Point(387, 412);
            this.btnBPag2.Name = "btnBPag2";
            this.btnBPag2.Size = new System.Drawing.Size(27, 23);
            this.btnBPag2.TabIndex = 17;
            this.btnBPag2.Text = "2";
            this.btnBPag2.UseVisualStyleBackColor = true;
            this.btnBPag2.Click += new System.EventHandler(this.btnBPag2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources._2569f;
            this.pictureBox1.Location = new System.Drawing.Point(723, 402);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(25, 28);
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // PercusionesPag3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 465);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnBPag2);
            this.Controls.Add(this.btnBPag1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxBase);
            this.Controls.Add(this.btnBase);
            this.Controls.Add(this.lblPrecioBase);
            this.Controls.Add(this.lblDescripcionBase);
            this.Controls.Add(this.pictureBoxBase);
            this.Controls.Add(this.btnPedal);
            this.Controls.Add(this.comboBoxPedal);
            this.Controls.Add(this.lblPrecioPedal);
            this.Controls.Add(this.lblDescripcionPedal);
            this.Controls.Add(this.pictureBoxPedal);
            this.Controls.Add(this.btnBanco);
            this.Controls.Add(this.comboBoxBanco);
            this.Controls.Add(this.lblPrecioBanco);
            this.Controls.Add(this.lblDescripcionBanco);
            this.Controls.Add(this.pictureBoxBanco);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PercusionesPag3";
            this.Text = "PercusionesPag3";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPedal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBanco)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxBanco;
        private System.Windows.Forms.Label lblDescripcionBanco;
        private System.Windows.Forms.Label lblPrecioBanco;
        private System.Windows.Forms.ComboBox comboBoxBanco;
        private System.Windows.Forms.Button btnBanco;
        private System.Windows.Forms.PictureBox pictureBoxPedal;
        private System.Windows.Forms.Label lblDescripcionPedal;
        private System.Windows.Forms.Label lblPrecioPedal;
        private System.Windows.Forms.ComboBox comboBoxPedal;
        private System.Windows.Forms.Button btnPedal;
        private System.Windows.Forms.PictureBox pictureBoxBase;
        private System.Windows.Forms.Label lblDescripcionBase;
        private System.Windows.Forms.Label lblPrecioBase;
        private System.Windows.Forms.Button btnBase;
        private System.Windows.Forms.ComboBox comboBoxBase;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnBPag1;
        private System.Windows.Forms.Button btnBPag2;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}