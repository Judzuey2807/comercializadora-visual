﻿namespace Comercializadora
{
    partial class PianosPag3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDescripcionPYamahaRojo = new System.Windows.Forms.Label();
            this.lblPrecioPYamahaRojo = new System.Windows.Forms.Label();
            this.btnPYamahaRojo = new System.Windows.Forms.Button();
            this.comboBoxPYamahaRojo = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDescripcionPYamahaCola = new System.Windows.Forms.Label();
            this.lblPrecioPYamahaCola = new System.Windows.Forms.Label();
            this.btnPYamahaCola = new System.Windows.Forms.Button();
            this.comboBoxPYamahaCola = new System.Windows.Forms.ComboBox();
            this.lblDescripcionPPearl = new System.Windows.Forms.Label();
            this.lblPrecioPPearl = new System.Windows.Forms.Label();
            this.btnPPearl = new System.Windows.Forms.Button();
            this.comboBoxPPearl = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxPYamahaCola = new System.Windows.Forms.PictureBox();
            this.pictureBoxPRojo = new System.Windows.Forms.PictureBox();
            this.btnPag1Piano = new System.Windows.Forms.Button();
            this.btnPag2Piano = new System.Windows.Forms.Button();
            this.lblPag3 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPYamahaCola)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPRojo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDescripcionPYamahaRojo
            // 
            this.lblDescripcionPYamahaRojo.AutoSize = true;
            this.lblDescripcionPYamahaRojo.Location = new System.Drawing.Point(168, 79);
            this.lblDescripcionPYamahaRojo.Name = "lblDescripcionPYamahaRojo";
            this.lblDescripcionPYamahaRojo.Size = new System.Drawing.Size(338, 13);
            this.lblDescripcionPYamahaRojo.TabIndex = 1;
            this.lblDescripcionPYamahaRojo.Text = "Pianos Acustico Yamaha Piano Vertical 110 Cm Acabado Ornamental ";
            // 
            // lblPrecioPYamahaRojo
            // 
            this.lblPrecioPYamahaRojo.AutoSize = true;
            this.lblPrecioPYamahaRojo.Location = new System.Drawing.Point(168, 113);
            this.lblPrecioPYamahaRojo.Name = "lblPrecioPYamahaRojo";
            this.lblPrecioPYamahaRojo.Size = new System.Drawing.Size(46, 13);
            this.lblPrecioPYamahaRojo.TabIndex = 2;
            this.lblPrecioPYamahaRojo.Text = "$88,829";
            this.lblPrecioPYamahaRojo.Click += new System.EventHandler(this.label2_Click);
            // 
            // btnPYamahaRojo
            // 
            this.btnPYamahaRojo.Location = new System.Drawing.Point(673, 92);
            this.btnPYamahaRojo.Name = "btnPYamahaRojo";
            this.btnPYamahaRojo.Size = new System.Drawing.Size(75, 23);
            this.btnPYamahaRojo.TabIndex = 3;
            this.btnPYamahaRojo.Text = "Agregar";
            this.btnPYamahaRojo.UseVisualStyleBackColor = true;
            // 
            // comboBoxPYamahaRojo
            // 
            this.comboBoxPYamahaRojo.FormattingEnabled = true;
            this.comboBoxPYamahaRojo.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxPYamahaRojo.Location = new System.Drawing.Point(611, 94);
            this.comboBoxPYamahaRojo.Name = "comboBoxPYamahaRojo";
            this.comboBoxPYamahaRojo.Size = new System.Drawing.Size(44, 21);
            this.comboBoxPYamahaRojo.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(168, 94);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(157, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "(Caoba Satinado) Mod. PM2SM";
            // 
            // lblDescripcionPYamahaCola
            // 
            this.lblDescripcionPYamahaCola.AutoSize = true;
            this.lblDescripcionPYamahaCola.Location = new System.Drawing.Point(168, 177);
            this.lblDescripcionPYamahaCola.Name = "lblDescripcionPYamahaCola";
            this.lblDescripcionPYamahaCola.Size = new System.Drawing.Size(440, 13);
            this.lblDescripcionPYamahaCola.TabIndex = 7;
            this.lblDescripcionPYamahaCola.Text = "Pianos Acustico Yamaha Piano De Cola 200 Cm (Negro Brillante) Con Banco Mod. PCX5" +
    "PE";
            // 
            // lblPrecioPYamahaCola
            // 
            this.lblPrecioPYamahaCola.AutoSize = true;
            this.lblPrecioPYamahaCola.Location = new System.Drawing.Point(168, 214);
            this.lblPrecioPYamahaCola.Name = "lblPrecioPYamahaCola";
            this.lblPrecioPYamahaCola.Size = new System.Drawing.Size(52, 13);
            this.lblPrecioPYamahaCola.TabIndex = 8;
            this.lblPrecioPYamahaCola.Text = "$629,369";
            // 
            // btnPYamahaCola
            // 
            this.btnPYamahaCola.Location = new System.Drawing.Point(673, 198);
            this.btnPYamahaCola.Name = "btnPYamahaCola";
            this.btnPYamahaCola.Size = new System.Drawing.Size(75, 23);
            this.btnPYamahaCola.TabIndex = 9;
            this.btnPYamahaCola.Text = "Agregar";
            this.btnPYamahaCola.UseVisualStyleBackColor = true;
            // 
            // comboBoxPYamahaCola
            // 
            this.comboBoxPYamahaCola.FormattingEnabled = true;
            this.comboBoxPYamahaCola.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxPYamahaCola.Location = new System.Drawing.Point(611, 200);
            this.comboBoxPYamahaCola.Name = "comboBoxPYamahaCola";
            this.comboBoxPYamahaCola.Size = new System.Drawing.Size(44, 21);
            this.comboBoxPYamahaCola.TabIndex = 10;
            // 
            // lblDescripcionPPearl
            // 
            this.lblDescripcionPPearl.AutoSize = true;
            this.lblDescripcionPPearl.Location = new System.Drawing.Point(168, 297);
            this.lblDescripcionPPearl.Name = "lblDescripcionPPearl";
            this.lblDescripcionPPearl.Size = new System.Drawing.Size(339, 13);
            this.lblDescripcionPPearl.TabIndex = 12;
            this.lblDescripcionPPearl.Text = "Pianos Acustico Pearl River Piano Vertical 115 Caoba Estudio Cbanca";
            // 
            // lblPrecioPPearl
            // 
            this.lblPrecioPPearl.AutoSize = true;
            this.lblPrecioPPearl.Location = new System.Drawing.Point(168, 329);
            this.lblPrecioPPearl.Name = "lblPrecioPPearl";
            this.lblPrecioPPearl.Size = new System.Drawing.Size(46, 13);
            this.lblPrecioPPearl.TabIndex = 13;
            this.lblPrecioPPearl.Text = "$57,405";
            // 
            // btnPPearl
            // 
            this.btnPPearl.Location = new System.Drawing.Point(673, 308);
            this.btnPPearl.Name = "btnPPearl";
            this.btnPPearl.Size = new System.Drawing.Size(75, 23);
            this.btnPPearl.TabIndex = 14;
            this.btnPPearl.Text = "Agregar";
            this.btnPPearl.UseVisualStyleBackColor = true;
            // 
            // comboBoxPPearl
            // 
            this.comboBoxPPearl.FormattingEnabled = true;
            this.comboBoxPPearl.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxPPearl.Location = new System.Drawing.Point(611, 310);
            this.comboBoxPPearl.Name = "comboBoxPPearl";
            this.comboBoxPPearl.Size = new System.Drawing.Size(44, 21);
            this.comboBoxPPearl.TabIndex = 15;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources.PRUP015S;
            this.pictureBox1.Location = new System.Drawing.Point(30, 261);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(101, 103);
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBoxPYamahaCola
            // 
            this.pictureBoxPYamahaCola.Image = global::Comercializadora.Properties.Resources.cola;
            this.pictureBoxPYamahaCola.Location = new System.Drawing.Point(30, 152);
            this.pictureBoxPYamahaCola.Name = "pictureBoxPYamahaCola";
            this.pictureBoxPYamahaCola.Size = new System.Drawing.Size(100, 104);
            this.pictureBoxPYamahaCola.TabIndex = 6;
            this.pictureBoxPYamahaCola.TabStop = false;
            // 
            // pictureBoxPRojo
            // 
            this.pictureBoxPRojo.Image = global::Comercializadora.Properties.Resources.rojo;
            this.pictureBoxPRojo.Location = new System.Drawing.Point(30, 44);
            this.pictureBoxPRojo.Name = "pictureBoxPRojo";
            this.pictureBoxPRojo.Size = new System.Drawing.Size(101, 102);
            this.pictureBoxPRojo.TabIndex = 0;
            this.pictureBoxPRojo.TabStop = false;
            // 
            // btnPag1Piano
            // 
            this.btnPag1Piano.Location = new System.Drawing.Point(354, 412);
            this.btnPag1Piano.Name = "btnPag1Piano";
            this.btnPag1Piano.Size = new System.Drawing.Size(27, 23);
            this.btnPag1Piano.TabIndex = 16;
            this.btnPag1Piano.Text = "1";
            this.btnPag1Piano.UseVisualStyleBackColor = true;
            this.btnPag1Piano.Click += new System.EventHandler(this.btnPag1Piano_Click);
            // 
            // btnPag2Piano
            // 
            this.btnPag2Piano.Location = new System.Drawing.Point(387, 412);
            this.btnPag2Piano.Name = "btnPag2Piano";
            this.btnPag2Piano.Size = new System.Drawing.Size(27, 23);
            this.btnPag2Piano.TabIndex = 17;
            this.btnPag2Piano.Text = "2";
            this.btnPag2Piano.UseVisualStyleBackColor = true;
            this.btnPag2Piano.Click += new System.EventHandler(this.btnPag2Piano_Click);
            // 
            // lblPag3
            // 
            this.lblPag3.AutoSize = true;
            this.lblPag3.Location = new System.Drawing.Point(423, 417);
            this.lblPag3.Name = "lblPag3";
            this.lblPag3.Size = new System.Drawing.Size(13, 13);
            this.lblPag3.TabIndex = 18;
            this.lblPag3.Text = "3";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Image = global::Comercializadora.Properties.Resources._2569f;
            this.pictureBox2.Location = new System.Drawing.Point(721, 403);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(27, 27);
            this.pictureBox2.TabIndex = 19;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // PianosPag3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 465);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.lblPag3);
            this.Controls.Add(this.btnPag2Piano);
            this.Controls.Add(this.btnPag1Piano);
            this.Controls.Add(this.comboBoxPPearl);
            this.Controls.Add(this.btnPPearl);
            this.Controls.Add(this.lblPrecioPPearl);
            this.Controls.Add(this.lblDescripcionPPearl);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.comboBoxPYamahaCola);
            this.Controls.Add(this.btnPYamahaCola);
            this.Controls.Add(this.lblPrecioPYamahaCola);
            this.Controls.Add(this.lblDescripcionPYamahaCola);
            this.Controls.Add(this.pictureBoxPYamahaCola);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxPYamahaRojo);
            this.Controls.Add(this.btnPYamahaRojo);
            this.Controls.Add(this.lblPrecioPYamahaRojo);
            this.Controls.Add(this.lblDescripcionPYamahaRojo);
            this.Controls.Add(this.pictureBoxPRojo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PianosPag3";
            this.Text = "PianosPag3";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPYamahaCola)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPRojo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxPRojo;
        private System.Windows.Forms.Label lblDescripcionPYamahaRojo;
        private System.Windows.Forms.Label lblPrecioPYamahaRojo;
        private System.Windows.Forms.Button btnPYamahaRojo;
        private System.Windows.Forms.ComboBox comboBoxPYamahaRojo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBoxPYamahaCola;
        private System.Windows.Forms.Label lblDescripcionPYamahaCola;
        private System.Windows.Forms.Label lblPrecioPYamahaCola;
        private System.Windows.Forms.Button btnPYamahaCola;
        private System.Windows.Forms.ComboBox comboBoxPYamahaCola;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblDescripcionPPearl;
        private System.Windows.Forms.Label lblPrecioPPearl;
        private System.Windows.Forms.Button btnPPearl;
        private System.Windows.Forms.ComboBox comboBoxPPearl;
        private System.Windows.Forms.Button btnPag1Piano;
        private System.Windows.Forms.Button btnPag2Piano;
        private System.Windows.Forms.Label lblPag3;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}