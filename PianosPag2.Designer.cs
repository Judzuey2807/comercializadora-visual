﻿namespace Comercializadora
{
    partial class PianosPag2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDescripcionOYamaha = new System.Windows.Forms.Label();
            this.lblPrecioOYamaha = new System.Windows.Forms.Label();
            this.comboBoxOYamaha = new System.Windows.Forms.ComboBox();
            this.btnOYamaha = new System.Windows.Forms.Button();
            this.lblOYamaha2 = new System.Windows.Forms.Label();
            this.lblPrecioYamaha2 = new System.Windows.Forms.Label();
            this.btnOYamaha2 = new System.Windows.Forms.Button();
            this.comboBoxOYamaha2 = new System.Windows.Forms.ComboBox();
            this.lblDescripcionPNegro = new System.Windows.Forms.Label();
            this.lblPrecioPNegro = new System.Windows.Forms.Label();
            this.comboBoxPNegro = new System.Windows.Forms.ComboBox();
            this.btnPNegro = new System.Windows.Forms.Button();
            this.btnPianoPag1 = new System.Windows.Forms.Button();
            this.btnPianoPag3 = new System.Windows.Forms.Button();
            this.lblPianoPag2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxOYamaha2 = new System.Windows.Forms.PictureBox();
            this.pictureBoxOYamaha = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxOYamaha2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxOYamaha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDescripcionOYamaha
            // 
            this.lblDescripcionOYamaha.AutoSize = true;
            this.lblDescripcionOYamaha.Location = new System.Drawing.Point(168, 79);
            this.lblDescripcionOYamaha.Name = "lblDescripcionOYamaha";
            this.lblDescripcionOYamaha.Size = new System.Drawing.Size(295, 13);
            this.lblDescripcionOYamaha.TabIndex = 1;
            this.lblDescripcionOYamaha.Text = "Organo Yamaha Organo Electone Stagea Mini Mod. EELB01";
            // 
            // lblPrecioOYamaha
            // 
            this.lblPrecioOYamaha.AutoSize = true;
            this.lblPrecioOYamaha.Location = new System.Drawing.Point(168, 104);
            this.lblPrecioOYamaha.Name = "lblPrecioOYamaha";
            this.lblPrecioOYamaha.Size = new System.Drawing.Size(46, 13);
            this.lblPrecioOYamaha.TabIndex = 2;
            this.lblPrecioOYamaha.Text = "$48,791";
            // 
            // comboBoxOYamaha
            // 
            this.comboBoxOYamaha.FormattingEnabled = true;
            this.comboBoxOYamaha.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxOYamaha.Location = new System.Drawing.Point(611, 94);
            this.comboBoxOYamaha.Name = "comboBoxOYamaha";
            this.comboBoxOYamaha.Size = new System.Drawing.Size(44, 21);
            this.comboBoxOYamaha.TabIndex = 3;
            // 
            // btnOYamaha
            // 
            this.btnOYamaha.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOYamaha.Location = new System.Drawing.Point(673, 92);
            this.btnOYamaha.Name = "btnOYamaha";
            this.btnOYamaha.Size = new System.Drawing.Size(75, 23);
            this.btnOYamaha.TabIndex = 4;
            this.btnOYamaha.Text = "Agregar";
            this.btnOYamaha.UseVisualStyleBackColor = true;
            this.btnOYamaha.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblOYamaha2
            // 
            this.lblOYamaha2.AutoSize = true;
            this.lblOYamaha2.Location = new System.Drawing.Point(168, 188);
            this.lblOYamaha2.Name = "lblOYamaha2";
            this.lblOYamaha2.Size = new System.Drawing.Size(326, 13);
            this.lblOYamaha2.TabIndex = 6;
            this.lblOYamaha2.Text = "Organo Yamaha Organo Electone Stagea Custom Mod. EELS01CU";
            // 
            // lblPrecioYamaha2
            // 
            this.lblPrecioYamaha2.AutoSize = true;
            this.lblPrecioYamaha2.Location = new System.Drawing.Point(168, 214);
            this.lblPrecioYamaha2.Name = "lblPrecioYamaha2";
            this.lblPrecioYamaha2.Size = new System.Drawing.Size(52, 13);
            this.lblPrecioYamaha2.TabIndex = 7;
            this.lblPrecioYamaha2.Text = "$267,038";
            // 
            // btnOYamaha2
            // 
            this.btnOYamaha2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOYamaha2.Location = new System.Drawing.Point(673, 198);
            this.btnOYamaha2.Name = "btnOYamaha2";
            this.btnOYamaha2.Size = new System.Drawing.Size(75, 23);
            this.btnOYamaha2.TabIndex = 8;
            this.btnOYamaha2.Text = "Agregar";
            this.btnOYamaha2.UseVisualStyleBackColor = true;
            this.btnOYamaha2.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // comboBoxOYamaha2
            // 
            this.comboBoxOYamaha2.FormattingEnabled = true;
            this.comboBoxOYamaha2.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxOYamaha2.Location = new System.Drawing.Point(611, 200);
            this.comboBoxOYamaha2.Name = "comboBoxOYamaha2";
            this.comboBoxOYamaha2.Size = new System.Drawing.Size(43, 21);
            this.comboBoxOYamaha2.TabIndex = 9;
            // 
            // lblDescripcionPNegro
            // 
            this.lblDescripcionPNegro.AutoSize = true;
            this.lblDescripcionPNegro.Location = new System.Drawing.Point(168, 297);
            this.lblDescripcionPNegro.Name = "lblDescripcionPNegro";
            this.lblDescripcionPNegro.Size = new System.Drawing.Size(392, 13);
            this.lblDescripcionPNegro.TabIndex = 11;
            this.lblDescripcionPNegro.Text = "Pianos Acustico Yamaha Piano Vertical 109 Cm (Negro Brillante) Mod. PJU109PE";
            // 
            // lblPrecioPNegro
            // 
            this.lblPrecioPNegro.AutoSize = true;
            this.lblPrecioPNegro.Location = new System.Drawing.Point(168, 329);
            this.lblPrecioPNegro.Name = "lblPrecioPNegro";
            this.lblPrecioPNegro.Size = new System.Drawing.Size(46, 13);
            this.lblPrecioPNegro.TabIndex = 12;
            this.lblPrecioPNegro.Text = "$62,369";
            // 
            // comboBoxPNegro
            // 
            this.comboBoxPNegro.FormattingEnabled = true;
            this.comboBoxPNegro.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxPNegro.Location = new System.Drawing.Point(611, 310);
            this.comboBoxPNegro.Name = "comboBoxPNegro";
            this.comboBoxPNegro.Size = new System.Drawing.Size(42, 21);
            this.comboBoxPNegro.TabIndex = 13;
            // 
            // btnPNegro
            // 
            this.btnPNegro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPNegro.Location = new System.Drawing.Point(673, 308);
            this.btnPNegro.Name = "btnPNegro";
            this.btnPNegro.Size = new System.Drawing.Size(75, 23);
            this.btnPNegro.TabIndex = 14;
            this.btnPNegro.Text = "Agregar";
            this.btnPNegro.UseVisualStyleBackColor = true;
            // 
            // btnPianoPag1
            // 
            this.btnPianoPag1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPianoPag1.Location = new System.Drawing.Point(359, 412);
            this.btnPianoPag1.Name = "btnPianoPag1";
            this.btnPianoPag1.Size = new System.Drawing.Size(27, 23);
            this.btnPianoPag1.TabIndex = 15;
            this.btnPianoPag1.Text = "1";
            this.btnPianoPag1.UseVisualStyleBackColor = true;
            this.btnPianoPag1.Click += new System.EventHandler(this.btnPianoPag1_Click);
            // 
            // btnPianoPag3
            // 
            this.btnPianoPag3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPianoPag3.Location = new System.Drawing.Point(411, 412);
            this.btnPianoPag3.Name = "btnPianoPag3";
            this.btnPianoPag3.Size = new System.Drawing.Size(27, 23);
            this.btnPianoPag3.TabIndex = 16;
            this.btnPianoPag3.Text = "3";
            this.btnPianoPag3.UseVisualStyleBackColor = true;
            this.btnPianoPag3.Click += new System.EventHandler(this.btnPianoPag3_Click);
            // 
            // lblPianoPag2
            // 
            this.lblPianoPag2.AutoSize = true;
            this.lblPianoPag2.Location = new System.Drawing.Point(392, 417);
            this.lblPianoPag2.Name = "lblPianoPag2";
            this.lblPianoPag2.Size = new System.Drawing.Size(13, 13);
            this.lblPianoPag2.TabIndex = 17;
            this.lblPianoPag2.Text = "2";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources.PJU109PE;
            this.pictureBox1.Location = new System.Drawing.Point(31, 262);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(103, 104);
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBoxOYamaha2
            // 
            this.pictureBoxOYamaha2.Image = global::Comercializadora.Properties.Resources.EELS01CU;
            this.pictureBoxOYamaha2.Location = new System.Drawing.Point(31, 153);
            this.pictureBoxOYamaha2.Name = "pictureBoxOYamaha2";
            this.pictureBoxOYamaha2.Size = new System.Drawing.Size(102, 103);
            this.pictureBoxOYamaha2.TabIndex = 5;
            this.pictureBoxOYamaha2.TabStop = false;
            // 
            // pictureBoxOYamaha
            // 
            this.pictureBoxOYamaha.Image = global::Comercializadora.Properties.Resources.EELB01;
            this.pictureBoxOYamaha.Location = new System.Drawing.Point(30, 44);
            this.pictureBoxOYamaha.Name = "pictureBoxOYamaha";
            this.pictureBoxOYamaha.Size = new System.Drawing.Size(103, 103);
            this.pictureBoxOYamaha.TabIndex = 0;
            this.pictureBoxOYamaha.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Image = global::Comercializadora.Properties.Resources._2569f;
            this.pictureBox2.Location = new System.Drawing.Point(720, 403);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(28, 27);
            this.pictureBox2.TabIndex = 18;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // PianosPag2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 465);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.lblPianoPag2);
            this.Controls.Add(this.btnPianoPag3);
            this.Controls.Add(this.btnPianoPag1);
            this.Controls.Add(this.btnPNegro);
            this.Controls.Add(this.comboBoxPNegro);
            this.Controls.Add(this.lblPrecioPNegro);
            this.Controls.Add(this.lblDescripcionPNegro);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.comboBoxOYamaha2);
            this.Controls.Add(this.btnOYamaha2);
            this.Controls.Add(this.lblPrecioYamaha2);
            this.Controls.Add(this.lblOYamaha2);
            this.Controls.Add(this.pictureBoxOYamaha2);
            this.Controls.Add(this.btnOYamaha);
            this.Controls.Add(this.comboBoxOYamaha);
            this.Controls.Add(this.lblPrecioOYamaha);
            this.Controls.Add(this.lblDescripcionOYamaha);
            this.Controls.Add(this.pictureBoxOYamaha);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PianosPag2";
            this.Text = "PianosPag2";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxOYamaha2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxOYamaha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxOYamaha;
        private System.Windows.Forms.Label lblDescripcionOYamaha;
        private System.Windows.Forms.Label lblPrecioOYamaha;
        private System.Windows.Forms.ComboBox comboBoxOYamaha;
        private System.Windows.Forms.Button btnOYamaha;
        private System.Windows.Forms.PictureBox pictureBoxOYamaha2;
        private System.Windows.Forms.Label lblOYamaha2;
        private System.Windows.Forms.Label lblPrecioYamaha2;
        private System.Windows.Forms.Button btnOYamaha2;
        private System.Windows.Forms.ComboBox comboBoxOYamaha2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblDescripcionPNegro;
        private System.Windows.Forms.Label lblPrecioPNegro;
        private System.Windows.Forms.ComboBox comboBoxPNegro;
        private System.Windows.Forms.Button btnPNegro;
        private System.Windows.Forms.Button btnPianoPag1;
        private System.Windows.Forms.Button btnPianoPag3;
        private System.Windows.Forms.Label lblPianoPag2;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}