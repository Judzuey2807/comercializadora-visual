﻿namespace Comercializadora
{
    partial class VerTodosLosProductos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VerTodosLosProductos));
            this.VerTodosComprarSamsungAire3 = new System.Windows.Forms.Button();
            this.VerTodosAgregarSamsungAire3 = new System.Windows.Forms.Button();
            this.VerTodosAgregarSamsungAire2 = new System.Windows.Forms.Button();
            this.VerTodosComprarSamsungAire2 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.MiniSplitSamsung = new System.Windows.Forms.Label();
            this.VerTodosAgregarSamsungAire = new System.Windows.Forms.Button();
            this.VerTodosComprarSamsungAire = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SamsungAR12MVFHEWK = new System.Windows.Forms.Label();
            this.VerTodosAgregarMabe3 = new System.Windows.Forms.Button();
            this.VerTodosComprarMabe3 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.VerTodosAgregarMabe2 = new System.Windows.Forms.Button();
            this.VerTodosComprarMabe2 = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.VerTodoAgregarMabe1 = new System.Windows.Forms.Button();
            this.VerTodosComprarMabe1 = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.VerTodosComprarLg3 = new System.Windows.Forms.Button();
            this.VerTodosaAgregarLg = new System.Windows.Forms.Button();
            this.VerTodosAgregarLg2 = new System.Windows.Forms.Button();
            this.VerTodosComprarLg2 = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.VerTodosAgregarLg1 = new System.Windows.Forms.Button();
            this.VerTodosComprarLg1 = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.VerTodosComprarCarrier3 = new System.Windows.Forms.Button();
            this.VerTodosAgregarCarrier3 = new System.Windows.Forms.Button();
            this.VerTodosAgregarCarrier2 = new System.Windows.Forms.Button();
            this.VerTodosComprarCarrier2 = new System.Windows.Forms.Button();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.VerTodosAgregarCarrier1 = new System.Windows.Forms.Button();
            this.VerTodosComprarCarrier1 = new System.Windows.Forms.Button();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.VerTodosAgregarWhirlpool3 = new System.Windows.Forms.Button();
            this.VerTodosComprarWhirlpool3 = new System.Windows.Forms.Button();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.VerTodosAgregarWhirlpool2 = new System.Windows.Forms.Button();
            this.VerTodosComprarWhirlpool2 = new System.Windows.Forms.Button();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.VerTodosAgregarWhirlpool1 = new System.Windows.Forms.Button();
            this.VerTodosComprarWhirlpool1 = new System.Windows.Forms.Button();
            this.label57 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.VerTodosComprarSamsung3 = new System.Windows.Forms.Button();
            this.VerTodosAgregarSamsung3 = new System.Windows.Forms.Button();
            this.VerTodosAgregarSamsung2 = new System.Windows.Forms.Button();
            this.VerTodosComprarSamsung2 = new System.Windows.Forms.Button();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.VerTodosAgregarSamsung1 = new System.Windows.Forms.Button();
            this.VerTodosComprarSamsung1 = new System.Windows.Forms.Button();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.VerTodosAgregarLavaLg3 = new System.Windows.Forms.Button();
            this.VerTodosComprarLavaLg3 = new System.Windows.Forms.Button();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.VerTodosAgregarLavaLg2 = new System.Windows.Forms.Button();
            this.VerTodosComprarLlavaLg2 = new System.Windows.Forms.Button();
            this.label77 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.VerTodosAgregarLavaLg1 = new System.Windows.Forms.Button();
            this.VerTodosComprarLavaLg1 = new System.Windows.Forms.Button();
            this.label81 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.VerTodosComprarLavaGeneral3 = new System.Windows.Forms.Button();
            this.VerTodosAgregarLavaGeneral3 = new System.Windows.Forms.Button();
            this.VerTodosAgreagrLavaGeneral2 = new System.Windows.Forms.Button();
            this.VerTodosComprarLavaGeneral2 = new System.Windows.Forms.Button();
            this.label85 = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.VerTodosAgregarLavaGeneral1 = new System.Windows.Forms.Button();
            this.VerTodosComprarLavaGeneral1 = new System.Windows.Forms.Button();
            this.label93 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.VerTodosAgregarRefriWhirlpool3 = new System.Windows.Forms.Button();
            this.VerTodosComprarRefriWhirlpool3 = new System.Windows.Forms.Button();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.VerTodosAgregarRefriWhirlpool2 = new System.Windows.Forms.Button();
            this.VerTodosComprarRefriWhirlpool2 = new System.Windows.Forms.Button();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.VerTodosAgregarRefriWhirlpool1 = new System.Windows.Forms.Button();
            this.VerTodosComprarRefriWhirlpool1 = new System.Windows.Forms.Button();
            this.label105 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.label119 = new System.Windows.Forms.Label();
            this.label120 = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.VerTodosAgregarRefriSamsung3 = new System.Windows.Forms.Button();
            this.VerTodosComprarRefriSamsung3 = new System.Windows.Forms.Button();
            this.VerTodosAgregarRefriSamsung2 = new System.Windows.Forms.Button();
            this.VerTodosComprarRefriSamsung2 = new System.Windows.Forms.Button();
            this.VerTodosAgregarRefriSmsung1 = new System.Windows.Forms.Button();
            this.VerTodosComprarRefriSamsung1 = new System.Windows.Forms.Button();
            this.VerTodosAgregarRefriMabe1 = new System.Windows.Forms.Button();
            this.VerTodosComprarRefriMabe1 = new System.Windows.Forms.Button();
            this.VerTodosAgregarRefriMabe2 = new System.Windows.Forms.Button();
            this.VerTodosComprarRefriMabe2 = new System.Windows.Forms.Button();
            this.VerTodosAgregarRefriMabe3 = new System.Windows.Forms.Button();
            this.VerTodosComprarRefriMabe3 = new System.Windows.Forms.Button();
            this.label122 = new System.Windows.Forms.Label();
            this.label123 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.label126 = new System.Windows.Forms.Label();
            this.label127 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.label133 = new System.Windows.Forms.Label();
            this.VerTodosAgregarRefriLg3 = new System.Windows.Forms.Button();
            this.VerTodosComprarRefriLg3 = new System.Windows.Forms.Button();
            this.label134 = new System.Windows.Forms.Label();
            this.label135 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.label137 = new System.Windows.Forms.Label();
            this.VerTodosAgregarRefriLg2 = new System.Windows.Forms.Button();
            this.VerTodosComprarRefriLg2 = new System.Windows.Forms.Button();
            this.label138 = new System.Windows.Forms.Label();
            this.label139 = new System.Windows.Forms.Label();
            this.label140 = new System.Windows.Forms.Label();
            this.label141 = new System.Windows.Forms.Label();
            this.VerTodosAgregarRefriLg1 = new System.Windows.Forms.Button();
            this.VerTodosComprarRefriLg1 = new System.Windows.Forms.Button();
            this.label142 = new System.Windows.Forms.Label();
            this.label143 = new System.Windows.Forms.Label();
            this.label144 = new System.Windows.Forms.Label();
            this.label145 = new System.Windows.Forms.Label();
            this.VerTodosComrarMicroSamsung3 = new System.Windows.Forms.Button();
            this.VerTodosAgregarMicroSamsung = new System.Windows.Forms.Button();
            this.VerTodosAgregarMicroSamsung2 = new System.Windows.Forms.Button();
            this.VerTodosComprarMicroSamsung2 = new System.Windows.Forms.Button();
            this.label146 = new System.Windows.Forms.Label();
            this.label147 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.label149 = new System.Windows.Forms.Label();
            this.label150 = new System.Windows.Forms.Label();
            this.label151 = new System.Windows.Forms.Label();
            this.label152 = new System.Windows.Forms.Label();
            this.label153 = new System.Windows.Forms.Label();
            this.VerTodosAgregarMicroSamsung1 = new System.Windows.Forms.Button();
            this.VerTodosComprarMicroSamsung1 = new System.Windows.Forms.Button();
            this.label154 = new System.Windows.Forms.Label();
            this.label155 = new System.Windows.Forms.Label();
            this.label156 = new System.Windows.Forms.Label();
            this.label157 = new System.Windows.Forms.Label();
            this.VerTodosComprarMicroWhirlpool = new System.Windows.Forms.Button();
            this.VerTodosAgregarMicroWhirlpool3 = new System.Windows.Forms.Button();
            this.VerTodosAgregarMicroWhirlpool2 = new System.Windows.Forms.Button();
            this.VerTodosComprarMicroWhirlpool2 = new System.Windows.Forms.Button();
            this.label158 = new System.Windows.Forms.Label();
            this.label159 = new System.Windows.Forms.Label();
            this.label160 = new System.Windows.Forms.Label();
            this.label161 = new System.Windows.Forms.Label();
            this.label162 = new System.Windows.Forms.Label();
            this.label163 = new System.Windows.Forms.Label();
            this.label164 = new System.Windows.Forms.Label();
            this.label165 = new System.Windows.Forms.Label();
            this.VerTodosAgregarMicroWhirlpool = new System.Windows.Forms.Button();
            this.VerTodosComprarMicroWhirlpool1 = new System.Windows.Forms.Button();
            this.label166 = new System.Windows.Forms.Label();
            this.label167 = new System.Windows.Forms.Label();
            this.label168 = new System.Windows.Forms.Label();
            this.label169 = new System.Windows.Forms.Label();
            this.VerTodosComprarMicroMabe3 = new System.Windows.Forms.Button();
            this.VerTodosAgregarMicroMabe3 = new System.Windows.Forms.Button();
            this.VerTodosAgreagrMicroMabe2 = new System.Windows.Forms.Button();
            this.VerTodosComprarMicroMabe2 = new System.Windows.Forms.Button();
            this.label170 = new System.Windows.Forms.Label();
            this.label171 = new System.Windows.Forms.Label();
            this.label172 = new System.Windows.Forms.Label();
            this.label173 = new System.Windows.Forms.Label();
            this.label174 = new System.Windows.Forms.Label();
            this.label175 = new System.Windows.Forms.Label();
            this.label176 = new System.Windows.Forms.Label();
            this.label177 = new System.Windows.Forms.Label();
            this.VerTodosAgregarMicroMabe1 = new System.Windows.Forms.Button();
            this.VerTodosComprarMicroMabe1 = new System.Windows.Forms.Button();
            this.label178 = new System.Windows.Forms.Label();
            this.label179 = new System.Windows.Forms.Label();
            this.label180 = new System.Windows.Forms.Label();
            this.label181 = new System.Windows.Forms.Label();
            this.VerTodosComprarMicroDaewoo3 = new System.Windows.Forms.Button();
            this.VerTodosAgregarMicroDaewoo3 = new System.Windows.Forms.Button();
            this.VerTodosAgregarMicroDaewoo2 = new System.Windows.Forms.Button();
            this.VerTodosComprarMicroDaewoo2 = new System.Windows.Forms.Button();
            this.label182 = new System.Windows.Forms.Label();
            this.label183 = new System.Windows.Forms.Label();
            this.label184 = new System.Windows.Forms.Label();
            this.label185 = new System.Windows.Forms.Label();
            this.label186 = new System.Windows.Forms.Label();
            this.label187 = new System.Windows.Forms.Label();
            this.label188 = new System.Windows.Forms.Label();
            this.label189 = new System.Windows.Forms.Label();
            this.VerTodosAgregarMicroDaewoo = new System.Windows.Forms.Button();
            this.VerTodosComprarMicoDaewoo1 = new System.Windows.Forms.Button();
            this.label190 = new System.Windows.Forms.Label();
            this.label191 = new System.Windows.Forms.Label();
            this.label192 = new System.Windows.Forms.Label();
            this.label193 = new System.Windows.Forms.Label();
            this.BtnCarrito = new System.Windows.Forms.Button();
            this.pictureBox61 = new System.Windows.Forms.PictureBox();
            this.pictureBox62 = new System.Windows.Forms.PictureBox();
            this.pictureBox63 = new System.Windows.Forms.PictureBox();
            this.pictureBox64 = new System.Windows.Forms.PictureBox();
            this.pictureBox57 = new System.Windows.Forms.PictureBox();
            this.pictureBox58 = new System.Windows.Forms.PictureBox();
            this.pictureBox59 = new System.Windows.Forms.PictureBox();
            this.pictureBox60 = new System.Windows.Forms.PictureBox();
            this.pictureBox53 = new System.Windows.Forms.PictureBox();
            this.pictureBox54 = new System.Windows.Forms.PictureBox();
            this.pictureBox55 = new System.Windows.Forms.PictureBox();
            this.pictureBox56 = new System.Windows.Forms.PictureBox();
            this.pictureBox49 = new System.Windows.Forms.PictureBox();
            this.pictureBox50 = new System.Windows.Forms.PictureBox();
            this.pictureBox51 = new System.Windows.Forms.PictureBox();
            this.pictureBox52 = new System.Windows.Forms.PictureBox();
            this.pictureBox45 = new System.Windows.Forms.PictureBox();
            this.pictureBox46 = new System.Windows.Forms.PictureBox();
            this.pictureBox47 = new System.Windows.Forms.PictureBox();
            this.pictureBox48 = new System.Windows.Forms.PictureBox();
            this.pictureBox41 = new System.Windows.Forms.PictureBox();
            this.pictureBox42 = new System.Windows.Forms.PictureBox();
            this.pictureBox43 = new System.Windows.Forms.PictureBox();
            this.pictureBox44 = new System.Windows.Forms.PictureBox();
            this.pictureBox37 = new System.Windows.Forms.PictureBox();
            this.pictureBox38 = new System.Windows.Forms.PictureBox();
            this.pictureBox39 = new System.Windows.Forms.PictureBox();
            this.pictureBox40 = new System.Windows.Forms.PictureBox();
            this.pictureBox33 = new System.Windows.Forms.PictureBox();
            this.pictureBox34 = new System.Windows.Forms.PictureBox();
            this.pictureBox35 = new System.Windows.Forms.PictureBox();
            this.pictureBox36 = new System.Windows.Forms.PictureBox();
            this.pictureBox29 = new System.Windows.Forms.PictureBox();
            this.pictureBox30 = new System.Windows.Forms.PictureBox();
            this.pictureBox31 = new System.Windows.Forms.PictureBox();
            this.pictureBox32 = new System.Windows.Forms.PictureBox();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.pictureBox27 = new System.Windows.Forms.PictureBox();
            this.pictureBox28 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BtRegresarMabe = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox63)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // VerTodosComprarSamsungAire3
            // 
            this.VerTodosComprarSamsungAire3.Location = new System.Drawing.Point(241, 718);
            this.VerTodosComprarSamsungAire3.Name = "VerTodosComprarSamsungAire3";
            this.VerTodosComprarSamsungAire3.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarSamsungAire3.TabIndex = 181;
            this.VerTodosComprarSamsungAire3.Text = "Comprar";
            this.VerTodosComprarSamsungAire3.UseVisualStyleBackColor = true;
            // 
            // VerTodosAgregarSamsungAire3
            // 
            this.VerTodosAgregarSamsungAire3.Location = new System.Drawing.Point(370, 718);
            this.VerTodosAgregarSamsungAire3.Name = "VerTodosAgregarSamsungAire3";
            this.VerTodosAgregarSamsungAire3.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarSamsungAire3.TabIndex = 180;
            this.VerTodosAgregarSamsungAire3.Text = "Agregar al carrito";
            this.VerTodosAgregarSamsungAire3.UseVisualStyleBackColor = true;
            // 
            // VerTodosAgregarSamsungAire2
            // 
            this.VerTodosAgregarSamsungAire2.Location = new System.Drawing.Point(370, 500);
            this.VerTodosAgregarSamsungAire2.Name = "VerTodosAgregarSamsungAire2";
            this.VerTodosAgregarSamsungAire2.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarSamsungAire2.TabIndex = 179;
            this.VerTodosAgregarSamsungAire2.Text = "Agregar al carrito";
            this.VerTodosAgregarSamsungAire2.UseVisualStyleBackColor = true;
            this.VerTodosAgregarSamsungAire2.Click += new System.EventHandler(this.VerTodosAgregarSamsungAire2_Click);
            // 
            // VerTodosComprarSamsungAire2
            // 
            this.VerTodosComprarSamsungAire2.Location = new System.Drawing.Point(241, 500);
            this.VerTodosComprarSamsungAire2.Name = "VerTodosComprarSamsungAire2";
            this.VerTodosComprarSamsungAire2.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarSamsungAire2.TabIndex = 178;
            this.VerTodosComprarSamsungAire2.Text = "Comprar";
            this.VerTodosComprarSamsungAire2.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(239, 617);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(383, 78);
            this.label12.TabIndex = 177;
            this.label12.Text = resources.GetString("label12.Text");
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(239, 595);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 15);
            this.label11.TabIndex = 176;
            this.label11.Text = "$ 13,691.30";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(239, 565);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(107, 15);
            this.label10.TabIndex = 175;
            this.label10.Text = "Marca: SAMSUNG";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(238, 543);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(555, 22);
            this.label9.TabIndex = 174;
            this.label9.Text = "AIRE ACONDICIONADO MINISPLIT 18000 BTU\'S PLATEADO";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(239, 426);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(270, 39);
            this.label8.TabIndex = 173;
            this.label8.Text = "Enfría hasta un 38% más rápido que un mini split común\r\nTecnología de iones S-Pla" +
    "sma\r\n70% menos ruido";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(239, 407);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 15);
            this.label7.TabIndex = 172;
            this.label7.Text = "$ 15,990.00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(239, 378);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 15);
            this.label6.TabIndex = 171;
            this.label6.Text = "Marca: SAMSUNG";
            // 
            // MiniSplitSamsung
            // 
            this.MiniSplitSamsung.AutoSize = true;
            this.MiniSplitSamsung.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.MiniSplitSamsung.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.MiniSplitSamsung.Location = new System.Drawing.Point(244, 356);
            this.MiniSplitSamsung.Name = "MiniSplitSamsung";
            this.MiniSplitSamsung.Size = new System.Drawing.Size(488, 22);
            this.MiniSplitSamsung.TabIndex = 170;
            this.MiniSplitSamsung.Text = "Mini Split Samsung Inverter Frío y Calor 12,000 BTU";
            // 
            // VerTodosAgregarSamsungAire
            // 
            this.VerTodosAgregarSamsungAire.Location = new System.Drawing.Point(370, 303);
            this.VerTodosAgregarSamsungAire.Name = "VerTodosAgregarSamsungAire";
            this.VerTodosAgregarSamsungAire.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarSamsungAire.TabIndex = 169;
            this.VerTodosAgregarSamsungAire.Text = "Agregar al carrito";
            this.VerTodosAgregarSamsungAire.UseVisualStyleBackColor = true;
            this.VerTodosAgregarSamsungAire.Click += new System.EventHandler(this.VerTodosAgregarSamsungAire_Click);
            // 
            // VerTodosComprarSamsungAire
            // 
            this.VerTodosComprarSamsungAire.Location = new System.Drawing.Point(241, 303);
            this.VerTodosComprarSamsungAire.Name = "VerTodosComprarSamsungAire";
            this.VerTodosComprarSamsungAire.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarSamsungAire.TabIndex = 168;
            this.VerTodosComprarSamsungAire.Text = "Comprar";
            this.VerTodosComprarSamsungAire.UseVisualStyleBackColor = true;
            this.VerTodosComprarSamsungAire.Click += new System.EventHandler(this.VerTodosComprarSamsungAire_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(238, 236);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 52);
            this.label4.TabIndex = 167;
            this.label4.Text = "220 V\r\n1 tonelada\r\n12,000 BTUS\r\n16 SEER\r\n";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(238, 211);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 15);
            this.label3.TabIndex = 166;
            this.label3.Text = "$ 8,999.00";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(239, 184);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 15);
            this.label2.TabIndex = 165;
            this.label2.Text = "Marca: SAMSUNG";
            // 
            // SamsungAR12MVFHEWK
            // 
            this.SamsungAR12MVFHEWK.AutoSize = true;
            this.SamsungAR12MVFHEWK.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.SamsungAR12MVFHEWK.Location = new System.Drawing.Point(237, 162);
            this.SamsungAR12MVFHEWK.Name = "SamsungAR12MVFHEWK";
            this.SamsungAR12MVFHEWK.Size = new System.Drawing.Size(554, 22);
            this.SamsungAR12MVFHEWK.TabIndex = 164;
            this.SamsungAR12MVFHEWK.Text = "Aire Acondicionado Samsung AR12MVFHEWK Inverter Frio";
            // 
            // VerTodosAgregarMabe3
            // 
            this.VerTodosAgregarMabe3.Location = new System.Drawing.Point(375, 1460);
            this.VerTodosAgregarMabe3.Name = "VerTodosAgregarMabe3";
            this.VerTodosAgregarMabe3.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarMabe3.TabIndex = 205;
            this.VerTodosAgregarMabe3.Text = "Agregar al carrito";
            this.VerTodosAgregarMabe3.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarMabe3
            // 
            this.VerTodosComprarMabe3.Location = new System.Drawing.Point(248, 1460);
            this.VerTodosComprarMabe3.Name = "VerTodosComprarMabe3";
            this.VerTodosComprarMabe3.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarMabe3.TabIndex = 204;
            this.VerTodosComprarMabe3.Text = "Comprar";
            this.VerTodosComprarMabe3.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(245, 1326);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(224, 117);
            this.label13.TabIndex = 203;
            this.label13.Text = "Tipo de clima, solo frío\r\nCapacidad de 12,000 BTU\r\n220 voltios\r\nTemperatura desde" +
    " 16-30 grados centígrados\r\nTubería de cobre\r\nFiltro de alta densidad\r\nIon de pla" +
    "ta\r\nBio filtro\r\nDisplay con luz LE";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label14.Location = new System.Drawing.Point(245, 1302);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(72, 15);
            this.label14.TabIndex = 202;
            this.label14.Text = "$ 12,290.00";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label15.Location = new System.Drawing.Point(245, 1278);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(82, 15);
            this.label15.TabIndex = 201;
            this.label15.Text = "Marca: MABE";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label16.Location = new System.Drawing.Point(244, 1256);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(433, 22);
            this.label16.TabIndex = 200;
            this.label16.Text = "Inversor Mini Split Mabe Solo Frío 12,000 BTU";
            // 
            // VerTodosAgregarMabe2
            // 
            this.VerTodosAgregarMabe2.Location = new System.Drawing.Point(375, 1205);
            this.VerTodosAgregarMabe2.Name = "VerTodosAgregarMabe2";
            this.VerTodosAgregarMabe2.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarMabe2.TabIndex = 199;
            this.VerTodosAgregarMabe2.Text = "Agregar al carrito";
            this.VerTodosAgregarMabe2.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarMabe2
            // 
            this.VerTodosComprarMabe2.Location = new System.Drawing.Point(248, 1205);
            this.VerTodosComprarMabe2.Name = "VerTodosComprarMabe2";
            this.VerTodosComprarMabe2.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarMabe2.TabIndex = 198;
            this.VerTodosComprarMabe2.Text = "Comprar";
            this.VerTodosComprarMabe2.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(245, 1145);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(291, 39);
            this.label17.TabIndex = 197;
            this.label17.Text = "Con tecnología Wi-Fi para encenderlo desde cualquier lugar\r\nTemperaturas de 16 a " +
    "30 ° C, 220 V / 60 Hz\r\nColor plata oscura";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label18.Location = new System.Drawing.Point(245, 1121);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(72, 15);
            this.label18.TabIndex = 196;
            this.label18.Text = "$ 10,990.00";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label19.Location = new System.Drawing.Point(245, 1097);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(82, 15);
            this.label19.TabIndex = 195;
            this.label19.Text = "Marca: MABE";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label20.Location = new System.Drawing.Point(244, 1075);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(443, 22);
            this.label20.TabIndex = 194;
            this.label20.Text = "Mini Split Mabe Inverter Solo Frío 18,000 BTU\'s";
            // 
            // VerTodoAgregarMabe1
            // 
            this.VerTodoAgregarMabe1.Location = new System.Drawing.Point(375, 1023);
            this.VerTodoAgregarMabe1.Name = "VerTodoAgregarMabe1";
            this.VerTodoAgregarMabe1.Size = new System.Drawing.Size(105, 23);
            this.VerTodoAgregarMabe1.TabIndex = 193;
            this.VerTodoAgregarMabe1.Text = "Agregar al carrito";
            this.VerTodoAgregarMabe1.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarMabe1
            // 
            this.VerTodosComprarMabe1.Location = new System.Drawing.Point(248, 1023);
            this.VerTodosComprarMabe1.Name = "VerTodosComprarMabe1";
            this.VerTodosComprarMabe1.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarMabe1.TabIndex = 192;
            this.VerTodosComprarMabe1.Text = "Comprar";
            this.VerTodosComprarMabe1.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(245, 963);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(306, 39);
            this.label21.TabIndex = 191;
            this.label21.Text = "Recubrimiento Blue Fin, protege las tuberías contra la corrosión\r\nFiltro Silver I" +
    "on que elimina bacterias\r\nCon Gas Ecológico R410A para ahorrar energía";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label22.Location = new System.Drawing.Point(245, 939);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(65, 15);
            this.label22.TabIndex = 190;
            this.label22.Text = "$ 6,590.00";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label23.Location = new System.Drawing.Point(245, 915);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(82, 15);
            this.label23.TabIndex = 189;
            this.label23.Text = "Marca: MABE";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label24.Location = new System.Drawing.Point(244, 893);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(369, 22);
            this.label24.TabIndex = 188;
            this.label24.Text = "Mini Split Mabe Frío y Calor 12000 BTU";
            // 
            // VerTodosComprarLg3
            // 
            this.VerTodosComprarLg3.Location = new System.Drawing.Point(248, 2232);
            this.VerTodosComprarLg3.Name = "VerTodosComprarLg3";
            this.VerTodosComprarLg3.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarLg3.TabIndex = 227;
            this.VerTodosComprarLg3.Text = "Comprar";
            this.VerTodosComprarLg3.UseVisualStyleBackColor = true;
            // 
            // VerTodosaAgregarLg
            // 
            this.VerTodosaAgregarLg.Location = new System.Drawing.Point(377, 2232);
            this.VerTodosaAgregarLg.Name = "VerTodosaAgregarLg";
            this.VerTodosaAgregarLg.Size = new System.Drawing.Size(105, 23);
            this.VerTodosaAgregarLg.TabIndex = 226;
            this.VerTodosaAgregarLg.Text = "Agregar al carrito";
            this.VerTodosaAgregarLg.UseVisualStyleBackColor = true;
            // 
            // VerTodosAgregarLg2
            // 
            this.VerTodosAgregarLg2.Location = new System.Drawing.Point(377, 2033);
            this.VerTodosAgregarLg2.Name = "VerTodosAgregarLg2";
            this.VerTodosAgregarLg2.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarLg2.TabIndex = 225;
            this.VerTodosAgregarLg2.Text = "Agregar al carrito";
            this.VerTodosAgregarLg2.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarLg2
            // 
            this.VerTodosComprarLg2.Location = new System.Drawing.Point(248, 2033);
            this.VerTodosComprarLg2.Name = "VerTodosComprarLg2";
            this.VerTodosComprarLg2.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarLg2.TabIndex = 224;
            this.VerTodosComprarLg2.Text = "Comprar";
            this.VerTodosComprarLg2.UseVisualStyleBackColor = true;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(246, 2146);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(321, 65);
            this.label25.TabIndex = 223;
            this.label25.Text = resources.GetString("label25.Text");
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label26.Location = new System.Drawing.Point(246, 2124);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(62, 15);
            this.label26.TabIndex = 222;
            this.label26.Text = "14,213.74";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label27.Location = new System.Drawing.Point(246, 2094);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(67, 15);
            this.label27.TabIndex = 221;
            this.label27.Text = "Marca:  LG";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label28.Location = new System.Drawing.Point(245, 2072);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(527, 22);
            this.label28.TabIndex = 220;
            this.label28.Text = "LG AIRE ACONDICIONADO MINISPLIT 12000 BTU\'S GRIS";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(246, 1936);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(253, 78);
            this.label29.TabIndex = 219;
            this.label29.Text = resources.GetString("label29.Text");
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label30.Location = new System.Drawing.Point(246, 1917);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(72, 15);
            this.label30.TabIndex = 218;
            this.label30.Text = "$ 10,290.00";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label31.Location = new System.Drawing.Point(246, 1888);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(67, 15);
            this.label31.TabIndex = 217;
            this.label31.Text = "Marca:  LG";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label32.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label32.Location = new System.Drawing.Point(245, 1866);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(433, 22);
            this.label32.TabIndex = 216;
            this.label32.Text = "Inversor Mini Split LG Frío y Calor 12,000 BTU";
            // 
            // VerTodosAgregarLg1
            // 
            this.VerTodosAgregarLg1.Location = new System.Drawing.Point(377, 1820);
            this.VerTodosAgregarLg1.Name = "VerTodosAgregarLg1";
            this.VerTodosAgregarLg1.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarLg1.TabIndex = 215;
            this.VerTodosAgregarLg1.Text = "Agregar al carrito";
            this.VerTodosAgregarLg1.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarLg1
            // 
            this.VerTodosComprarLg1.Location = new System.Drawing.Point(248, 1820);
            this.VerTodosComprarLg1.Name = "VerTodosComprarLg1";
            this.VerTodosComprarLg1.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarLg1.TabIndex = 214;
            this.VerTodosComprarLg1.Text = "Comprar";
            this.VerTodosComprarLg1.UseVisualStyleBackColor = true;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(246, 1766);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(57, 39);
            this.label33.TabIndex = 213;
            this.label33.Text = "1 tonelada\r\n18 SEER\r\nR410A";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label34.Location = new System.Drawing.Point(245, 1740);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(72, 15);
            this.label34.TabIndex = 212;
            this.label34.Text = "$ 12,999.00";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label35.Location = new System.Drawing.Point(246, 1713);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(64, 15);
            this.label35.TabIndex = 211;
            this.label35.Text = "Marca: LG";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label36.Location = new System.Drawing.Point(244, 1691);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(463, 22);
            this.label36.TabIndex = 210;
            this.label36.Text = "Aire Acondicionado LG VM121C8 Inverter S / Frio";
            // 
            // VerTodosComprarCarrier3
            // 
            this.VerTodosComprarCarrier3.Location = new System.Drawing.Point(247, 2987);
            this.VerTodosComprarCarrier3.Name = "VerTodosComprarCarrier3";
            this.VerTodosComprarCarrier3.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarCarrier3.TabIndex = 249;
            this.VerTodosComprarCarrier3.Text = "Comprar";
            this.VerTodosComprarCarrier3.UseVisualStyleBackColor = true;
            // 
            // VerTodosAgregarCarrier3
            // 
            this.VerTodosAgregarCarrier3.Location = new System.Drawing.Point(375, 2987);
            this.VerTodosAgregarCarrier3.Name = "VerTodosAgregarCarrier3";
            this.VerTodosAgregarCarrier3.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarCarrier3.TabIndex = 248;
            this.VerTodosAgregarCarrier3.Text = "Agregar al carrito";
            this.VerTodosAgregarCarrier3.UseVisualStyleBackColor = true;
            // 
            // VerTodosAgregarCarrier2
            // 
            this.VerTodosAgregarCarrier2.Location = new System.Drawing.Point(375, 2804);
            this.VerTodosAgregarCarrier2.Name = "VerTodosAgregarCarrier2";
            this.VerTodosAgregarCarrier2.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarCarrier2.TabIndex = 247;
            this.VerTodosAgregarCarrier2.Text = "Agregar al carrito";
            this.VerTodosAgregarCarrier2.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarCarrier2
            // 
            this.VerTodosComprarCarrier2.Location = new System.Drawing.Point(247, 2804);
            this.VerTodosComprarCarrier2.Name = "VerTodosComprarCarrier2";
            this.VerTodosComprarCarrier2.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarCarrier2.TabIndex = 246;
            this.VerTodosComprarCarrier2.Text = "Comprar";
            this.VerTodosComprarCarrier2.UseVisualStyleBackColor = true;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(244, 2932);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(306, 39);
            this.label37.TabIndex = 245;
            this.label37.Text = "Recubrimiento Blue Fin, protege las tuberías contra la corrosión\r\nFiltro Silver I" +
    "on que elimina bacterias\r\nCon Gas Ecológico R410A para ahorrar energía";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label38.Location = new System.Drawing.Point(244, 2905);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(65, 15);
            this.label38.TabIndex = 244;
            this.label38.Text = "$ 6,590.00";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label39.Location = new System.Drawing.Point(244, 2880);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(99, 15);
            this.label39.TabIndex = 243;
            this.label39.Text = "Marca: CARRIER";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label40.Location = new System.Drawing.Point(243, 2858);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(318, 22);
            this.label40.TabIndex = 242;
            this.label40.Text = "Mini Split  Frío y Calor 12000 BTU";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(244, 2724);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(384, 65);
            this.label41.TabIndex = 241;
            this.label41.Text = resources.GetString("label41.Text");
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label42.Location = new System.Drawing.Point(239, 2698);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(65, 15);
            this.label42.TabIndex = 240;
            this.label42.Text = "$ 4,990.00";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label43.Location = new System.Drawing.Point(238, 2673);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(102, 15);
            this.label43.TabIndex = 239;
            this.label43.Text = "Marca:  CARRIER";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label44.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label44.Location = new System.Drawing.Point(237, 2651);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(256, 22);
            this.label44.TabIndex = 238;
            this.label44.Text = "Aire Acondicionado 12000 ";
            // 
            // VerTodosAgregarCarrier1
            // 
            this.VerTodosAgregarCarrier1.Location = new System.Drawing.Point(376, 2604);
            this.VerTodosAgregarCarrier1.Name = "VerTodosAgregarCarrier1";
            this.VerTodosAgregarCarrier1.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarCarrier1.TabIndex = 237;
            this.VerTodosAgregarCarrier1.Text = "Agregar al carrito";
            this.VerTodosAgregarCarrier1.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarCarrier1
            // 
            this.VerTodosComprarCarrier1.Location = new System.Drawing.Point(248, 2604);
            this.VerTodosComprarCarrier1.Name = "VerTodosComprarCarrier1";
            this.VerTodosComprarCarrier1.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarCarrier1.TabIndex = 236;
            this.VerTodosComprarCarrier1.Text = "Comprar";
            this.VerTodosComprarCarrier1.UseVisualStyleBackColor = true;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(244, 2553);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(57, 39);
            this.label45.TabIndex = 235;
            this.label45.Text = "1 tonelada\r\n18 SEER\r\nR410A";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label46.Location = new System.Drawing.Point(245, 2524);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(65, 15);
            this.label46.TabIndex = 234;
            this.label46.Text = "$ 6,600.71";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label47.Location = new System.Drawing.Point(243, 2499);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(99, 15);
            this.label47.TabIndex = 233;
            this.label47.Text = "Marca: CARRIER";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label48.Location = new System.Drawing.Point(243, 2455);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(445, 44);
            this.label48.TabIndex = 232;
            this.label48.Text = "CARRIER AIRE ACONDICIONADO MINISPLIT LIV\r\n12000 BTU\'S BLANCO";
            // 
            // VerTodosAgregarWhirlpool3
            // 
            this.VerTodosAgregarWhirlpool3.Location = new System.Drawing.Point(375, 3869);
            this.VerTodosAgregarWhirlpool3.Name = "VerTodosAgregarWhirlpool3";
            this.VerTodosAgregarWhirlpool3.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarWhirlpool3.TabIndex = 271;
            this.VerTodosAgregarWhirlpool3.Text = "Agregar al carrito";
            this.VerTodosAgregarWhirlpool3.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarWhirlpool3
            // 
            this.VerTodosComprarWhirlpool3.Location = new System.Drawing.Point(247, 3869);
            this.VerTodosComprarWhirlpool3.Name = "VerTodosComprarWhirlpool3";
            this.VerTodosComprarWhirlpool3.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarWhirlpool3.TabIndex = 270;
            this.VerTodosComprarWhirlpool3.Text = "Comprar";
            this.VerTodosComprarWhirlpool3.UseVisualStyleBackColor = true;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(211, 3736);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(443, 117);
            this.label49.TabIndex = 269;
            this.label49.Text = resources.GetString("label49.Text");
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label50.Location = new System.Drawing.Point(211, 3709);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(65, 15);
            this.label50.TabIndex = 268;
            this.label50.Text = "$ 9,790.00";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label51.Location = new System.Drawing.Point(211, 3684);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(120, 15);
            this.label51.TabIndex = 267;
            this.label51.Text = "Marca: WHIRLPOOL";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label52.Location = new System.Drawing.Point(210, 3651);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(315, 22);
            this.label52.TabIndex = 266;
            this.label52.Text = "Lavadora Whirlpool 20 kg Blanca";
            // 
            // VerTodosAgregarWhirlpool2
            // 
            this.VerTodosAgregarWhirlpool2.Location = new System.Drawing.Point(375, 3597);
            this.VerTodosAgregarWhirlpool2.Name = "VerTodosAgregarWhirlpool2";
            this.VerTodosAgregarWhirlpool2.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarWhirlpool2.TabIndex = 265;
            this.VerTodosAgregarWhirlpool2.Text = "Agregar al carrito";
            this.VerTodosAgregarWhirlpool2.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarWhirlpool2
            // 
            this.VerTodosComprarWhirlpool2.Location = new System.Drawing.Point(247, 3597);
            this.VerTodosComprarWhirlpool2.Name = "VerTodosComprarWhirlpool2";
            this.VerTodosComprarWhirlpool2.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarWhirlpool2.TabIndex = 264;
            this.VerTodosComprarWhirlpool2.Text = "Comprar";
            this.VerTodosComprarWhirlpool2.UseVisualStyleBackColor = true;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(211, 3494);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(545, 91);
            this.label53.TabIndex = 263;
            this.label53.Text = resources.GetString("label53.Text");
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label54.Location = new System.Drawing.Point(211, 3479);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(65, 15);
            this.label54.TabIndex = 262;
            this.label54.Text = "$ 7,990.00";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label55.Location = new System.Drawing.Point(211, 3452);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(123, 15);
            this.label55.TabIndex = 261;
            this.label55.Text = "Marca: WHIRLPOOL ";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label56.Location = new System.Drawing.Point(210, 3421);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(373, 22);
            this.label56.TabIndex = 260;
            this.label56.Text = "Lavadora Whirlpool Xpert 18 Kg Blanca";
            // 
            // VerTodosAgregarWhirlpool1
            // 
            this.VerTodosAgregarWhirlpool1.Location = new System.Drawing.Point(375, 3363);
            this.VerTodosAgregarWhirlpool1.Name = "VerTodosAgregarWhirlpool1";
            this.VerTodosAgregarWhirlpool1.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarWhirlpool1.TabIndex = 259;
            this.VerTodosAgregarWhirlpool1.Text = "Agregar al carrito";
            this.VerTodosAgregarWhirlpool1.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarWhirlpool1
            // 
            this.VerTodosComprarWhirlpool1.Location = new System.Drawing.Point(247, 3363);
            this.VerTodosComprarWhirlpool1.Name = "VerTodosComprarWhirlpool1";
            this.VerTodosComprarWhirlpool1.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarWhirlpool1.TabIndex = 258;
            this.VerTodosComprarWhirlpool1.Text = "Comprar";
            this.VerTodosComprarWhirlpool1.UseVisualStyleBackColor = true;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(211, 3281);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(350, 39);
            this.label57.TabIndex = 257;
            this.label57.Text = "Ideal para 5 o más personas\r\n12 Ciclos para todo tipo de ropa y cuidado de ropa\r\n" +
    "Sistema de lavado Xpert Sistema que remueve las manchas más difíciles\r\n";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label58.Location = new System.Drawing.Point(211, 3234);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(70, 15);
            this.label58.TabIndex = 256;
            this.label58.Text = "$ 7,390.00}";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label59.Location = new System.Drawing.Point(211, 3204);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(120, 15);
            this.label59.TabIndex = 255;
            this.label59.Text = "Marca: WHIRLPOOL";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label60.Location = new System.Drawing.Point(210, 3167);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(318, 22);
            this.label60.TabIndex = 254;
            this.label60.Text = "Lavadora Whirlpool 18 Kg Blanca";
            // 
            // VerTodosComprarSamsung3
            // 
            this.VerTodosComprarSamsung3.Location = new System.Drawing.Point(200, 4662);
            this.VerTodosComprarSamsung3.Name = "VerTodosComprarSamsung3";
            this.VerTodosComprarSamsung3.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarSamsung3.TabIndex = 293;
            this.VerTodosComprarSamsung3.Text = "Comprar";
            this.VerTodosComprarSamsung3.UseVisualStyleBackColor = true;
            // 
            // VerTodosAgregarSamsung3
            // 
            this.VerTodosAgregarSamsung3.Location = new System.Drawing.Point(340, 4662);
            this.VerTodosAgregarSamsung3.Name = "VerTodosAgregarSamsung3";
            this.VerTodosAgregarSamsung3.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarSamsung3.TabIndex = 292;
            this.VerTodosAgregarSamsung3.Text = "Agregar al carrito";
            this.VerTodosAgregarSamsung3.UseVisualStyleBackColor = true;
            // 
            // VerTodosAgregarSamsung2
            // 
            this.VerTodosAgregarSamsung2.Location = new System.Drawing.Point(340, 4427);
            this.VerTodosAgregarSamsung2.Name = "VerTodosAgregarSamsung2";
            this.VerTodosAgregarSamsung2.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarSamsung2.TabIndex = 291;
            this.VerTodosAgregarSamsung2.Text = "Agregar al carrito";
            this.VerTodosAgregarSamsung2.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarSamsung2
            // 
            this.VerTodosComprarSamsung2.Location = new System.Drawing.Point(202, 4427);
            this.VerTodosComprarSamsung2.Name = "VerTodosComprarSamsung2";
            this.VerTodosComprarSamsung2.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarSamsung2.TabIndex = 290;
            this.VerTodosComprarSamsung2.Text = "Comprar";
            this.VerTodosComprarSamsung2.UseVisualStyleBackColor = true;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(200, 4591);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(206, 39);
            this.label61.TabIndex = 289;
            this.label61.Text = "Lavadora automatica 19 Kg\r\n Color Plata con negro y detalles en cromo\r\nPanel digi" +
    "tal";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label62.Location = new System.Drawing.Point(203, 4565);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(65, 15);
            this.label62.TabIndex = 288;
            this.label62.Text = "$ 9,965.00";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label63.Location = new System.Drawing.Point(203, 4536);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(107, 15);
            this.label63.TabIndex = 287;
            this.label63.Text = "Marca: SAMSUNG";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label64.Location = new System.Drawing.Point(200, 4483);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(380, 44);
            this.label64.TabIndex = 286;
            this.label64.Text = "LAVADORA 19KG AUTOMÁTICA NEGRO \r\nCON DETALLES CROMADOS";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(200, 4360);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(140, 39);
            this.label65.TabIndex = 285;
            this.label65.Text = "11 ciclos de lavado\r\n10 niveles de agua\r\nTina con acabado diamante";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label66.Location = new System.Drawing.Point(203, 4328);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(58, 15);
            this.label66.TabIndex = 284;
            this.label66.Text = " 9,999.00";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label67.Location = new System.Drawing.Point(200, 4297);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(107, 15);
            this.label67.TabIndex = 283;
            this.label67.Text = "Marca: SAMSUNG";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label68.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label68.Location = new System.Drawing.Point(199, 4263);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(318, 22);
            this.label68.TabIndex = 282;
            this.label68.Text = "Lavadora Samsung 19 Kg Blanca";
            // 
            // VerTodosAgregarSamsung1
            // 
            this.VerTodosAgregarSamsung1.Location = new System.Drawing.Point(340, 4199);
            this.VerTodosAgregarSamsung1.Name = "VerTodosAgregarSamsung1";
            this.VerTodosAgregarSamsung1.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarSamsung1.TabIndex = 281;
            this.VerTodosAgregarSamsung1.Text = "Agregar al carrito";
            this.VerTodosAgregarSamsung1.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarSamsung1
            // 
            this.VerTodosComprarSamsung1.Location = new System.Drawing.Point(200, 4199);
            this.VerTodosComprarSamsung1.Name = "VerTodosComprarSamsung1";
            this.VerTodosComprarSamsung1.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarSamsung1.TabIndex = 280;
            this.VerTodosComprarSamsung1.Text = "Comprar";
            this.VerTodosComprarSamsung1.UseVisualStyleBackColor = true;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(197, 4113);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(240, 52);
            this.label69.TabIndex = 279;
            this.label69.Text = "Recomendada para 4 personas\r\nManual de programación de lavado\r\nCarga superior\r\nSe" +
    " recomienda no interrumpir los ciclos de lavado";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label70.Location = new System.Drawing.Point(200, 4083);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(65, 15);
            this.label70.TabIndex = 278;
            this.label70.Text = "$ 8,899.00";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label71.Location = new System.Drawing.Point(200, 4056);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(107, 15);
            this.label71.TabIndex = 277;
            this.label71.Text = "Marca: SAMSUNG";
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label72.Location = new System.Drawing.Point(196, 4011);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(465, 22);
            this.label72.TabIndex = 276;
            this.label72.Text = "Lavadora Samsung Carga Superior 17 Kg Blanca";
            // 
            // VerTodosAgregarLavaLg3
            // 
            this.VerTodosAgregarLavaLg3.Location = new System.Drawing.Point(340, 5524);
            this.VerTodosAgregarLavaLg3.Name = "VerTodosAgregarLavaLg3";
            this.VerTodosAgregarLavaLg3.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarLavaLg3.TabIndex = 315;
            this.VerTodosAgregarLavaLg3.Text = "Agregar al carrito";
            this.VerTodosAgregarLavaLg3.UseVisualStyleBackColor = true;
            this.VerTodosAgregarLavaLg3.Click += new System.EventHandler(this.button35_Click);
            // 
            // VerTodosComprarLavaLg3
            // 
            this.VerTodosComprarLavaLg3.Location = new System.Drawing.Point(200, 5524);
            this.VerTodosComprarLavaLg3.Name = "VerTodosComprarLavaLg3";
            this.VerTodosComprarLavaLg3.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarLavaLg3.TabIndex = 314;
            this.VerTodosComprarLavaLg3.Text = "Comprar";
            this.VerTodosComprarLavaLg3.UseVisualStyleBackColor = true;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(223, 5427);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(212, 52);
            this.label73.TabIndex = 313;
            this.label73.Text = "Lavadora con 8 ciclos de lavado\r\nPuerta de vidrio templado\r\nAcabado de la canasta" +
    " de acero inoxidable\r\nProgramación Automática";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label74.Location = new System.Drawing.Point(223, 5389);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(77, 15);
            this.label74.TabIndex = 312;
            this.label74.Text = "$ 13,990.00}";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label75.Location = new System.Drawing.Point(223, 5364);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(64, 15);
            this.label75.TabIndex = 311;
            this.label75.Text = "Marca: LG";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label76.Location = new System.Drawing.Point(222, 5331);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(308, 22);
            this.label76.TabIndex = 310;
            this.label76.Text = "Lavadora LG 22 Kg Plata Deluxe";
            // 
            // VerTodosAgregarLavaLg2
            // 
            this.VerTodosAgregarLavaLg2.Location = new System.Drawing.Point(340, 5287);
            this.VerTodosAgregarLavaLg2.Name = "VerTodosAgregarLavaLg2";
            this.VerTodosAgregarLavaLg2.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarLavaLg2.TabIndex = 309;
            this.VerTodosAgregarLavaLg2.Text = "Agregar al carrito";
            this.VerTodosAgregarLavaLg2.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarLlavaLg2
            // 
            this.VerTodosComprarLlavaLg2.Location = new System.Drawing.Point(200, 5287);
            this.VerTodosComprarLlavaLg2.Name = "VerTodosComprarLlavaLg2";
            this.VerTodosComprarLlavaLg2.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarLlavaLg2.TabIndex = 308;
            this.VerTodosComprarLlavaLg2.Text = "Comprar";
            this.VerTodosComprarLlavaLg2.UseVisualStyleBackColor = true;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(223, 5196);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(422, 78);
            this.label77.TabIndex = 307;
            this.label77.Text = resources.GetString("label77.Text");
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label78.Location = new System.Drawing.Point(223, 5169);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(71, 15);
            this.label78.TabIndex = 306;
            this.label78.Text = "$ 11,590.00";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label79.Location = new System.Drawing.Point(223, 5145);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(64, 15);
            this.label79.TabIndex = 305;
            this.label79.Text = "Marca: LG";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label80.Location = new System.Drawing.Point(222, 5111);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(322, 22);
            this.label80.TabIndex = 304;
            this.label80.Text = "Lavadora LG Smart Inverter 18 kg";
            // 
            // VerTodosAgregarLavaLg1
            // 
            this.VerTodosAgregarLavaLg1.Location = new System.Drawing.Point(340, 5066);
            this.VerTodosAgregarLavaLg1.Name = "VerTodosAgregarLavaLg1";
            this.VerTodosAgregarLavaLg1.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarLavaLg1.TabIndex = 303;
            this.VerTodosAgregarLavaLg1.Text = "Agregar al carrito";
            this.VerTodosAgregarLavaLg1.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarLavaLg1
            // 
            this.VerTodosComprarLavaLg1.Location = new System.Drawing.Point(200, 5067);
            this.VerTodosComprarLavaLg1.Name = "VerTodosComprarLavaLg1";
            this.VerTodosComprarLavaLg1.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarLavaLg1.TabIndex = 302;
            this.VerTodosComprarLavaLg1.Text = "Comprar";
            this.VerTodosComprarLavaLg1.UseVisualStyleBackColor = true;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(223, 4960);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(355, 91);
            this.label81.TabIndex = 301;
            this.label81.Text = resources.GetString("label81.Text");
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label82.Location = new System.Drawing.Point(223, 4931);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(71, 15);
            this.label82.TabIndex = 300;
            this.label82.Text = "$ 11,590.00";
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label83.Location = new System.Drawing.Point(223, 4900);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(64, 15);
            this.label83.TabIndex = 299;
            this.label83.Text = "Marca: LG";
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label84.Location = new System.Drawing.Point(222, 4878);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(254, 22);
            this.label84.TabIndex = 298;
            this.label84.Text = "Lavadora LG 22 kg Blanca";
            // 
            // VerTodosComprarLavaGeneral3
            // 
            this.VerTodosComprarLavaGeneral3.Location = new System.Drawing.Point(208, 6376);
            this.VerTodosComprarLavaGeneral3.Name = "VerTodosComprarLavaGeneral3";
            this.VerTodosComprarLavaGeneral3.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarLavaGeneral3.TabIndex = 337;
            this.VerTodosComprarLavaGeneral3.Text = "Comprar";
            this.VerTodosComprarLavaGeneral3.UseVisualStyleBackColor = true;
            // 
            // VerTodosAgregarLavaGeneral3
            // 
            this.VerTodosAgregarLavaGeneral3.Location = new System.Drawing.Point(348, 6376);
            this.VerTodosAgregarLavaGeneral3.Name = "VerTodosAgregarLavaGeneral3";
            this.VerTodosAgregarLavaGeneral3.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarLavaGeneral3.TabIndex = 336;
            this.VerTodosAgregarLavaGeneral3.Text = "Agregar al carrito";
            this.VerTodosAgregarLavaGeneral3.UseVisualStyleBackColor = true;
            // 
            // VerTodosAgreagrLavaGeneral2
            // 
            this.VerTodosAgreagrLavaGeneral2.Location = new System.Drawing.Point(348, 6158);
            this.VerTodosAgreagrLavaGeneral2.Name = "VerTodosAgreagrLavaGeneral2";
            this.VerTodosAgreagrLavaGeneral2.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgreagrLavaGeneral2.TabIndex = 335;
            this.VerTodosAgreagrLavaGeneral2.Text = "Agregar al carrito";
            this.VerTodosAgreagrLavaGeneral2.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarLavaGeneral2
            // 
            this.VerTodosComprarLavaGeneral2.Location = new System.Drawing.Point(208, 6158);
            this.VerTodosComprarLavaGeneral2.Name = "VerTodosComprarLavaGeneral2";
            this.VerTodosComprarLavaGeneral2.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarLavaGeneral2.TabIndex = 334;
            this.VerTodosComprarLavaGeneral2.Text = "Comprar";
            this.VerTodosComprarLavaGeneral2.UseVisualStyleBackColor = true;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(208, 6294);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(253, 52);
            this.label85.TabIndex = 333;
            this.label85.Text = "Tecnología de lavado Air Bubbler 4D\r\nFiltro atrapapelusa\r\nCascada Dynamic Waterfa" +
    "ll\r\nCanasta de acero inoxidable con grabado Star Tubs";
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label86.Location = new System.Drawing.Point(211, 6255);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(62, 15);
            this.label86.TabIndex = 332;
            this.label86.Text = "$6,190.00";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label87.Location = new System.Drawing.Point(211, 6228);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(156, 15);
            this.label87.TabIndex = 331;
            this.label87.Text = "Marca: GENERIC ELECTRIC";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label88.Location = new System.Drawing.Point(208, 6197);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(305, 22);
            this.label88.TabIndex = 330;
            this.label88.Text = "Lavadora Daewoo 19 Kg Blanca";
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(205, 6089);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(224, 52);
            this.label89.TabIndex = 329;
            this.label89.Text = "Recomendable para 5 o más personas\r\nSistema de lavado por agitador\r\nLavado Expres" +
    "s en 20 minutos\r\nAhorra agua hasta 76% con aqua saver green";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label90.Location = new System.Drawing.Point(211, 6042);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(65, 15);
            this.label90.TabIndex = 328;
            this.label90.Text = "$ 7,490.00";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label91.Location = new System.Drawing.Point(208, 6011);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(156, 15);
            this.label91.TabIndex = 327;
            this.label91.Text = "Marca: GENERIC ELECTRIC";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label92.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label92.Location = new System.Drawing.Point(207, 5977);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(418, 22);
            this.label92.TabIndex = 326;
            this.label92.Text = "Lavadora GENERIC ELECTRIC 20 Kg Blanca";
            // 
            // VerTodosAgregarLavaGeneral1
            // 
            this.VerTodosAgregarLavaGeneral1.Location = new System.Drawing.Point(348, 5931);
            this.VerTodosAgregarLavaGeneral1.Name = "VerTodosAgregarLavaGeneral1";
            this.VerTodosAgregarLavaGeneral1.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarLavaGeneral1.TabIndex = 325;
            this.VerTodosAgregarLavaGeneral1.Text = "Agregar al carrito";
            this.VerTodosAgregarLavaGeneral1.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarLavaGeneral1
            // 
            this.VerTodosComprarLavaGeneral1.Location = new System.Drawing.Point(208, 5931);
            this.VerTodosComprarLavaGeneral1.Name = "VerTodosComprarLavaGeneral1";
            this.VerTodosComprarLavaGeneral1.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarLavaGeneral1.TabIndex = 324;
            this.VerTodosComprarLavaGeneral1.Text = "Comprar";
            this.VerTodosComprarLavaGeneral1.UseVisualStyleBackColor = true;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(205, 5827);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(337, 39);
            this.label93.TabIndex = 323;
            this.label93.Text = "Tapa de cristal templado con seguro\r\nPanel con 4 perillas\r\nDespachador de blanque" +
    "ador en cubierta y de suavizante en agitador";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label94.Location = new System.Drawing.Point(208, 5797);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(65, 15);
            this.label94.TabIndex = 322;
            this.label94.Text = "$ 8,490.00";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label95.Location = new System.Drawing.Point(208, 5770);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(156, 15);
            this.label95.TabIndex = 321;
            this.label95.Text = "Marca: GENERIC ELECTRIC";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label96.Location = new System.Drawing.Point(204, 5725);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(378, 22);
            this.label96.TabIndex = 320;
            this.label96.Text = "Lavadora Generic Electric 21 Kg Blanca";
            // 
            // VerTodosAgregarRefriWhirlpool3
            // 
            this.VerTodosAgregarRefriWhirlpool3.Location = new System.Drawing.Point(308, 7160);
            this.VerTodosAgregarRefriWhirlpool3.Name = "VerTodosAgregarRefriWhirlpool3";
            this.VerTodosAgregarRefriWhirlpool3.Size = new System.Drawing.Size(98, 23);
            this.VerTodosAgregarRefriWhirlpool3.TabIndex = 359;
            this.VerTodosAgregarRefriWhirlpool3.Text = "Agregar al carrito";
            this.VerTodosAgregarRefriWhirlpool3.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarRefriWhirlpool3
            // 
            this.VerTodosComprarRefriWhirlpool3.Location = new System.Drawing.Point(190, 7160);
            this.VerTodosComprarRefriWhirlpool3.Name = "VerTodosComprarRefriWhirlpool3";
            this.VerTodosComprarRefriWhirlpool3.Size = new System.Drawing.Size(98, 23);
            this.VerTodosComprarRefriWhirlpool3.TabIndex = 358;
            this.VerTodosComprarRefriWhirlpool3.Text = "Comprar";
            this.VerTodosComprarRefriWhirlpool3.UseVisualStyleBackColor = true;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(190, 7084);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(432, 52);
            this.label97.TabIndex = 357;
            this.label97.Text = resources.GetString("label97.Text");
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label98.Location = new System.Drawing.Point(190, 7056);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(72, 15);
            this.label98.TabIndex = 356;
            this.label98.Text = "$ 12,746.00";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label99.Location = new System.Drawing.Point(190, 7028);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(120, 15);
            this.label99.TabIndex = 355;
            this.label99.Text = "Marca: WHIRLPOOL";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label100.Location = new System.Drawing.Point(190, 6990);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(338, 22);
            this.label100.TabIndex = 354;
            this.label100.Text = "WHIRLPOOL REFRIGERADOR 18 P3";
            // 
            // VerTodosAgregarRefriWhirlpool2
            // 
            this.VerTodosAgregarRefriWhirlpool2.Location = new System.Drawing.Point(308, 6941);
            this.VerTodosAgregarRefriWhirlpool2.Name = "VerTodosAgregarRefriWhirlpool2";
            this.VerTodosAgregarRefriWhirlpool2.Size = new System.Drawing.Size(98, 23);
            this.VerTodosAgregarRefriWhirlpool2.TabIndex = 353;
            this.VerTodosAgregarRefriWhirlpool2.Text = "Agregar al carrito";
            this.VerTodosAgregarRefriWhirlpool2.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarRefriWhirlpool2
            // 
            this.VerTodosComprarRefriWhirlpool2.Location = new System.Drawing.Point(190, 6942);
            this.VerTodosComprarRefriWhirlpool2.Name = "VerTodosComprarRefriWhirlpool2";
            this.VerTodosComprarRefriWhirlpool2.Size = new System.Drawing.Size(98, 23);
            this.VerTodosComprarRefriWhirlpool2.TabIndex = 352;
            this.VerTodosComprarRefriWhirlpool2.Text = "Comprar";
            this.VerTodosComprarRefriWhirlpool2.UseVisualStyleBackColor = true;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(187, 6873);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(248, 39);
            this.label101.TabIndex = 351;
            this.label101.Text = "Recomendable para 1-2 personas\r\nMedidas fuera del paquete 168 x 56 x 67\r\nDespacha" +
    "dor de agua con capacidad de 4.2 Litros";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label102.Location = new System.Drawing.Point(190, 6848);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(65, 15);
            this.label102.TabIndex = 350;
            this.label102.Text = "$ 6,290.00";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label103.Location = new System.Drawing.Point(190, 6820);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(120, 15);
            this.label103.TabIndex = 349;
            this.label103.Text = "Marca: WHIRLPOOL";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label104.Location = new System.Drawing.Point(186, 6763);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(456, 44);
            this.label104.TabIndex = 348;
            this.label104.Text = "Refrigerador 9 Pies Whirlpool con Despachador \r\nAcero Inox";
            // 
            // VerTodosAgregarRefriWhirlpool1
            // 
            this.VerTodosAgregarRefriWhirlpool1.Location = new System.Drawing.Point(308, 6721);
            this.VerTodosAgregarRefriWhirlpool1.Name = "VerTodosAgregarRefriWhirlpool1";
            this.VerTodosAgregarRefriWhirlpool1.Size = new System.Drawing.Size(98, 23);
            this.VerTodosAgregarRefriWhirlpool1.TabIndex = 347;
            this.VerTodosAgregarRefriWhirlpool1.Text = "Agregar al carrito";
            this.VerTodosAgregarRefriWhirlpool1.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarRefriWhirlpool1
            // 
            this.VerTodosComprarRefriWhirlpool1.Location = new System.Drawing.Point(190, 6721);
            this.VerTodosComprarRefriWhirlpool1.Name = "VerTodosComprarRefriWhirlpool1";
            this.VerTodosComprarRefriWhirlpool1.Size = new System.Drawing.Size(98, 23);
            this.VerTodosComprarRefriWhirlpool1.TabIndex = 346;
            this.VerTodosComprarRefriWhirlpool1.Text = "Comprar";
            this.VerTodosComprarRefriWhirlpool1.UseVisualStyleBackColor = true;
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(187, 6651);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(328, 39);
            this.label105.TabIndex = 345;
            this.label105.Text = "Superficie Steel Pro Antifingerprint\r\nDispensador de agua con filtro incluido y c" +
    "apacidad de hasta 4 litros\r\nCuenta con puerta reversible y luz LED para una mayo" +
    "r visibilidad";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label106.Location = new System.Drawing.Point(190, 6616);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(65, 15);
            this.label106.TabIndex = 344;
            this.label106.Text = "$ 7,909.00";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label107.Location = new System.Drawing.Point(190, 6590);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(120, 15);
            this.label107.TabIndex = 343;
            this.label107.Text = "Marca: WHIRLPOOL";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label108.Location = new System.Drawing.Point(186, 6532);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(467, 44);
            this.label108.TabIndex = 342;
            this.label108.Text = "Refrigerador 14 Pies Whirlpool con Despachador \r\nAcero inox anti huellas";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label109.Location = new System.Drawing.Point(202, 7566);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(107, 15);
            this.label109.TabIndex = 382;
            this.label109.Text = "Marca: SAMSUNG";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label110.Location = new System.Drawing.Point(202, 7354);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(107, 15);
            this.label110.TabIndex = 381;
            this.label110.Text = "Marca: SAMSUNG";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Location = new System.Drawing.Point(203, 7838);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(328, 39);
            this.label111.TabIndex = 380;
            this.label111.Text = "Recomendable para 5-6 personas\r\nMedidas fuera del paquete: 178 x 91 x 86\r\nCon 2 c" +
    "ajones en refrigerador y uno más deslizable en el congelador";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label112.Location = new System.Drawing.Point(203, 7806);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(72, 15);
            this.label112.TabIndex = 379;
            this.label112.Text = "$ 13,790.00";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label113.Location = new System.Drawing.Point(202, 7778);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(107, 15);
            this.label113.TabIndex = 378;
            this.label113.Text = "Marca: SAMSUNG";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label114.Location = new System.Drawing.Point(202, 7752);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(350, 22);
            this.label114.TabIndex = 377;
            this.label114.Text = "Refrigerador 22 Pies Samsung Silver";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(203, 7625);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(348, 39);
            this.label115.TabIndex = 376;
            this.label115.Text = "Recomendable para 7 personas en adelante\r\nMedidas fuera del paquete 177,6 x 90,8 " +
    "x 91,9\r\nInversor digital ahorro de energía de hasta 40% vs modelo convencional";
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label116.Location = new System.Drawing.Point(210, 7597);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(72, 15);
            this.label116.TabIndex = 375;
            this.label116.Text = "$ 16,990.00";
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label117.Location = new System.Drawing.Point(202, 7511);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(427, 44);
            this.label117.TabIndex = 374;
            this.label117.Text = "Refrigerador 25 Pies Samsung Despachador \r\nde Agua Acero Inox";
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(203, 7413);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(255, 39);
            this.label118.TabIndex = 373;
            this.label118.Text = "Sistema de refrigeración Twing Cooling Plus\r\nInversor digital compresor\r\n70% más " +
    "humedad que un refrigerador convencional";
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(202, 7413);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(0, 13);
            this.label119.TabIndex = 372;
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label120.Location = new System.Drawing.Point(210, 7379);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(65, 15);
            this.label120.TabIndex = 371;
            this.label120.Text = "$ 7,990.00";
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label121.Location = new System.Drawing.Point(202, 7299);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(462, 44);
            this.label121.TabIndex = 370;
            this.label121.Text = "Refrigerador 14 Pies Samsung con Despachador\r\nAcero Inox";
            // 
            // VerTodosAgregarRefriSamsung3
            // 
            this.VerTodosAgregarRefriSamsung3.Location = new System.Drawing.Point(315, 7910);
            this.VerTodosAgregarRefriSamsung3.Name = "VerTodosAgregarRefriSamsung3";
            this.VerTodosAgregarRefriSamsung3.Size = new System.Drawing.Size(95, 23);
            this.VerTodosAgregarRefriSamsung3.TabIndex = 369;
            this.VerTodosAgregarRefriSamsung3.Text = "Agregar al carrito";
            this.VerTodosAgregarRefriSamsung3.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarRefriSamsung3
            // 
            this.VerTodosComprarRefriSamsung3.Location = new System.Drawing.Point(190, 7910);
            this.VerTodosComprarRefriSamsung3.Name = "VerTodosComprarRefriSamsung3";
            this.VerTodosComprarRefriSamsung3.Size = new System.Drawing.Size(107, 23);
            this.VerTodosComprarRefriSamsung3.TabIndex = 368;
            this.VerTodosComprarRefriSamsung3.Text = "Comprar";
            this.VerTodosComprarRefriSamsung3.UseVisualStyleBackColor = true;
            // 
            // VerTodosAgregarRefriSamsung2
            // 
            this.VerTodosAgregarRefriSamsung2.Location = new System.Drawing.Point(308, 7693);
            this.VerTodosAgregarRefriSamsung2.Name = "VerTodosAgregarRefriSamsung2";
            this.VerTodosAgregarRefriSamsung2.Size = new System.Drawing.Size(102, 23);
            this.VerTodosAgregarRefriSamsung2.TabIndex = 367;
            this.VerTodosAgregarRefriSamsung2.Text = "Agregar al carrito";
            this.VerTodosAgregarRefriSamsung2.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarRefriSamsung2
            // 
            this.VerTodosComprarRefriSamsung2.Location = new System.Drawing.Point(190, 7693);
            this.VerTodosComprarRefriSamsung2.Name = "VerTodosComprarRefriSamsung2";
            this.VerTodosComprarRefriSamsung2.Size = new System.Drawing.Size(107, 23);
            this.VerTodosComprarRefriSamsung2.TabIndex = 366;
            this.VerTodosComprarRefriSamsung2.Text = "Comprar";
            this.VerTodosComprarRefriSamsung2.UseVisualStyleBackColor = true;
            // 
            // VerTodosAgregarRefriSmsung1
            // 
            this.VerTodosAgregarRefriSmsung1.Location = new System.Drawing.Point(308, 7482);
            this.VerTodosAgregarRefriSmsung1.Name = "VerTodosAgregarRefriSmsung1";
            this.VerTodosAgregarRefriSmsung1.Size = new System.Drawing.Size(98, 23);
            this.VerTodosAgregarRefriSmsung1.TabIndex = 365;
            this.VerTodosAgregarRefriSmsung1.Text = "Agregar al carrito";
            this.VerTodosAgregarRefriSmsung1.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarRefriSamsung1
            // 
            this.VerTodosComprarRefriSamsung1.Location = new System.Drawing.Point(190, 7482);
            this.VerTodosComprarRefriSamsung1.Name = "VerTodosComprarRefriSamsung1";
            this.VerTodosComprarRefriSamsung1.Size = new System.Drawing.Size(107, 23);
            this.VerTodosComprarRefriSamsung1.TabIndex = 364;
            this.VerTodosComprarRefriSamsung1.Text = "Comprar";
            this.VerTodosComprarRefriSamsung1.UseVisualStyleBackColor = true;
            // 
            // VerTodosAgregarRefriMabe1
            // 
            this.VerTodosAgregarRefriMabe1.Location = new System.Drawing.Point(315, 8192);
            this.VerTodosAgregarRefriMabe1.Name = "VerTodosAgregarRefriMabe1";
            this.VerTodosAgregarRefriMabe1.Size = new System.Drawing.Size(95, 23);
            this.VerTodosAgregarRefriMabe1.TabIndex = 404;
            this.VerTodosAgregarRefriMabe1.Text = "Agregar al carrito";
            this.VerTodosAgregarRefriMabe1.UseVisualStyleBackColor = true;
            this.VerTodosAgregarRefriMabe1.Click += new System.EventHandler(this.button59_Click);
            // 
            // VerTodosComprarRefriMabe1
            // 
            this.VerTodosComprarRefriMabe1.Location = new System.Drawing.Point(190, 8194);
            this.VerTodosComprarRefriMabe1.Name = "VerTodosComprarRefriMabe1";
            this.VerTodosComprarRefriMabe1.Size = new System.Drawing.Size(107, 23);
            this.VerTodosComprarRefriMabe1.TabIndex = 403;
            this.VerTodosComprarRefriMabe1.Text = "Comprar";
            this.VerTodosComprarRefriMabe1.UseVisualStyleBackColor = true;
            // 
            // VerTodosAgregarRefriMabe2
            // 
            this.VerTodosAgregarRefriMabe2.Location = new System.Drawing.Point(309, 8382);
            this.VerTodosAgregarRefriMabe2.Name = "VerTodosAgregarRefriMabe2";
            this.VerTodosAgregarRefriMabe2.Size = new System.Drawing.Size(102, 23);
            this.VerTodosAgregarRefriMabe2.TabIndex = 402;
            this.VerTodosAgregarRefriMabe2.Text = "Agregar al carrito";
            this.VerTodosAgregarRefriMabe2.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarRefriMabe2
            // 
            this.VerTodosComprarRefriMabe2.Location = new System.Drawing.Point(195, 8384);
            this.VerTodosComprarRefriMabe2.Name = "VerTodosComprarRefriMabe2";
            this.VerTodosComprarRefriMabe2.Size = new System.Drawing.Size(96, 23);
            this.VerTodosComprarRefriMabe2.TabIndex = 401;
            this.VerTodosComprarRefriMabe2.Text = "Comprar";
            this.VerTodosComprarRefriMabe2.UseVisualStyleBackColor = true;
            // 
            // VerTodosAgregarRefriMabe3
            // 
            this.VerTodosAgregarRefriMabe3.Location = new System.Drawing.Point(309, 8598);
            this.VerTodosAgregarRefriMabe3.Name = "VerTodosAgregarRefriMabe3";
            this.VerTodosAgregarRefriMabe3.Size = new System.Drawing.Size(102, 23);
            this.VerTodosAgregarRefriMabe3.TabIndex = 400;
            this.VerTodosAgregarRefriMabe3.Text = "Agregar al carrito";
            this.VerTodosAgregarRefriMabe3.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarRefriMabe3
            // 
            this.VerTodosComprarRefriMabe3.Location = new System.Drawing.Point(189, 8598);
            this.VerTodosComprarRefriMabe3.Name = "VerTodosComprarRefriMabe3";
            this.VerTodosComprarRefriMabe3.Size = new System.Drawing.Size(102, 23);
            this.VerTodosComprarRefriMabe3.TabIndex = 399;
            this.VerTodosComprarRefriMabe3.Text = "Comprar";
            this.VerTodosComprarRefriMabe3.UseVisualStyleBackColor = true;
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Location = new System.Drawing.Point(192, 8137);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(568, 52);
            this.label122.TabIndex = 398;
            this.label122.Text = resources.GetString("label122.Text");
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label123.Location = new System.Drawing.Point(186, 8107);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(69, 15);
            this.label123.TabIndex = 397;
            this.label123.Text = "$10,299.00";
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label124.Location = new System.Drawing.Point(189, 8078);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(82, 15);
            this.label124.TabIndex = 396;
            this.label124.Text = "Marca: MABE";
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(189, 8318);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(485, 52);
            this.label125.TabIndex = 395;
            this.label125.Text = resources.GetString("label125.Text");
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label126.Location = new System.Drawing.Point(186, 8294);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(65, 15);
            this.label126.TabIndex = 394;
            this.label126.Text = "$ 8,999.00";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label127.Location = new System.Drawing.Point(186, 8270);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(82, 15);
            this.label127.TabIndex = 393;
            this.label127.Text = "Marca: MABE";
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label128.Location = new System.Drawing.Point(185, 8236);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(300, 22);
            this.label128.TabIndex = 392;
            this.label128.Text = "REFRIGERADOR MABE 14 PIES";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(186, 8536);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(470, 52);
            this.label129.TabIndex = 391;
            this.label129.Text = resources.GetString("label129.Text");
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label130.Location = new System.Drawing.Point(186, 8512);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(69, 15);
            this.label130.TabIndex = 390;
            this.label130.Text = "$12,495.00";
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label131.Location = new System.Drawing.Point(186, 8487);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(82, 15);
            this.label131.TabIndex = 389;
            this.label131.Text = "Marca: MABE";
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label132.Location = new System.Drawing.Point(185, 8431);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(416, 44);
            this.label132.TabIndex = 388;
            this.label132.Text = "Refrigerador Whirlpool 17 pies cúbicos gris \r\nacero WT1756A";
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label133.Location = new System.Drawing.Point(185, 8042);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(563, 22);
            this.label133.TabIndex = 387;
            this.label133.Text = "Mabe - Refrigerador Top Mount 14\" RME1436ZMFP0 - Negro";
            // 
            // VerTodosAgregarRefriLg3
            // 
            this.VerTodosAgregarRefriLg3.Location = new System.Drawing.Point(311, 9411);
            this.VerTodosAgregarRefriLg3.Name = "VerTodosAgregarRefriLg3";
            this.VerTodosAgregarRefriLg3.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarRefriLg3.TabIndex = 426;
            this.VerTodosAgregarRefriLg3.Text = "Agregar al carrito";
            this.VerTodosAgregarRefriLg3.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarRefriLg3
            // 
            this.VerTodosComprarRefriLg3.Location = new System.Drawing.Point(189, 9411);
            this.VerTodosComprarRefriLg3.Name = "VerTodosComprarRefriLg3";
            this.VerTodosComprarRefriLg3.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarRefriLg3.TabIndex = 425;
            this.VerTodosComprarRefriLg3.Text = "Comprar";
            this.VerTodosComprarRefriLg3.UseVisualStyleBackColor = true;
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(190, 9347);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(148, 39);
            this.label134.TabIndex = 424;
            this.label134.Text = "Puerta de enfriamiento\r\nCompresor inversor inteligente\r\nLuz LED";
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label135.Location = new System.Drawing.Point(190, 9294);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(72, 15);
            this.label135.TabIndex = 423;
            this.label135.Text = "$ 35,990.00";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label136.Location = new System.Drawing.Point(189, 9267);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(64, 15);
            this.label136.TabIndex = 422;
            this.label136.Text = "Marca: LG";
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label137.Location = new System.Drawing.Point(189, 9243);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(446, 22);
            this.label137.TabIndex = 421;
            this.label137.Text = "Refrigerador 15 Pies LG con Dispensador Plata";
            // 
            // VerTodosAgregarRefriLg2
            // 
            this.VerTodosAgregarRefriLg2.Location = new System.Drawing.Point(311, 9204);
            this.VerTodosAgregarRefriLg2.Name = "VerTodosAgregarRefriLg2";
            this.VerTodosAgregarRefriLg2.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarRefriLg2.TabIndex = 420;
            this.VerTodosAgregarRefriLg2.Text = "Agregar al carrito";
            this.VerTodosAgregarRefriLg2.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarRefriLg2
            // 
            this.VerTodosComprarRefriLg2.Location = new System.Drawing.Point(189, 9204);
            this.VerTodosComprarRefriLg2.Name = "VerTodosComprarRefriLg2";
            this.VerTodosComprarRefriLg2.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarRefriLg2.TabIndex = 419;
            this.VerTodosComprarRefriLg2.Text = "Comprar";
            this.VerTodosComprarRefriLg2.UseVisualStyleBackColor = true;
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(186, 9120);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(184, 39);
            this.label138.TabIndex = 418;
            this.label138.Text = "Función de diagnóstico inteligente\r\nDispensador y fábrica de hielos\r\nSistema de e" +
    "nfriamiento Multi Air Flow";
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label139.Location = new System.Drawing.Point(189, 9074);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(72, 15);
            this.label139.TabIndex = 417;
            this.label139.Text = "$ 35,990.00";
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label140.Location = new System.Drawing.Point(189, 9048);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(64, 15);
            this.label140.TabIndex = 416;
            this.label140.Text = "Marca: LG";
            // 
            // label141
            // 
            this.label141.AutoSize = true;
            this.label141.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label141.Location = new System.Drawing.Point(189, 9016);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(462, 22);
            this.label141.TabIndex = 415;
            this.label141.Text = "Refrigerador 22 Pies LG con Despachador Negro";
            // 
            // VerTodosAgregarRefriLg1
            // 
            this.VerTodosAgregarRefriLg1.Location = new System.Drawing.Point(311, 8976);
            this.VerTodosAgregarRefriLg1.Name = "VerTodosAgregarRefriLg1";
            this.VerTodosAgregarRefriLg1.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarRefriLg1.TabIndex = 414;
            this.VerTodosAgregarRefriLg1.Text = "Agregar al carrito";
            this.VerTodosAgregarRefriLg1.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarRefriLg1
            // 
            this.VerTodosComprarRefriLg1.Location = new System.Drawing.Point(189, 8976);
            this.VerTodosComprarRefriLg1.Name = "VerTodosComprarRefriLg1";
            this.VerTodosComprarRefriLg1.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarRefriLg1.TabIndex = 413;
            this.VerTodosComprarRefriLg1.Text = "Comprar";
            this.VerTodosComprarRefriLg1.UseVisualStyleBackColor = true;
            // 
            // label142
            // 
            this.label142.AutoSize = true;
            this.label142.Location = new System.Drawing.Point(186, 8897);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(369, 39);
            this.label142.TabIndex = 412;
            this.label142.Text = "Recomendable para 7 personas en adelante\r\nMedidas sin paquete 174 X 75.6 X 90.1\r\n" +
    "Inversor inteligente ahorro de energía de hasta 32% vs modelo convencional";
            // 
            // label143
            // 
            this.label143.AutoSize = true;
            this.label143.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label143.Location = new System.Drawing.Point(189, 8848);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(72, 15);
            this.label143.TabIndex = 411;
            this.label143.Text = "$ 15,990.00";
            // 
            // label144
            // 
            this.label144.AutoSize = true;
            this.label144.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label144.Location = new System.Drawing.Point(189, 8823);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(64, 15);
            this.label144.TabIndex = 410;
            this.label144.Text = "Marca: LG";
            // 
            // label145
            // 
            this.label145.AutoSize = true;
            this.label145.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label145.Location = new System.Drawing.Point(185, 8797);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(391, 22);
            this.label145.TabIndex = 409;
            this.label145.Text = "Refrigerador 22 Pies LG Acero Inoxidable";
            // 
            // VerTodosComrarMicroSamsung3
            // 
            this.VerTodosComrarMicroSamsung3.Location = new System.Drawing.Point(282, 10140);
            this.VerTodosComrarMicroSamsung3.Name = "VerTodosComrarMicroSamsung3";
            this.VerTodosComrarMicroSamsung3.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComrarMicroSamsung3.TabIndex = 448;
            this.VerTodosComrarMicroSamsung3.Text = "Comprar";
            this.VerTodosComrarMicroSamsung3.UseVisualStyleBackColor = true;
            // 
            // VerTodosAgregarMicroSamsung
            // 
            this.VerTodosAgregarMicroSamsung.Location = new System.Drawing.Point(420, 10140);
            this.VerTodosAgregarMicroSamsung.Name = "VerTodosAgregarMicroSamsung";
            this.VerTodosAgregarMicroSamsung.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarMicroSamsung.TabIndex = 447;
            this.VerTodosAgregarMicroSamsung.Text = "Agregar al carrito";
            this.VerTodosAgregarMicroSamsung.UseVisualStyleBackColor = true;
            // 
            // VerTodosAgregarMicroSamsung2
            // 
            this.VerTodosAgregarMicroSamsung2.Location = new System.Drawing.Point(420, 9918);
            this.VerTodosAgregarMicroSamsung2.Name = "VerTodosAgregarMicroSamsung2";
            this.VerTodosAgregarMicroSamsung2.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarMicroSamsung2.TabIndex = 446;
            this.VerTodosAgregarMicroSamsung2.Text = "Agregar al carrito";
            this.VerTodosAgregarMicroSamsung2.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarMicroSamsung2
            // 
            this.VerTodosComprarMicroSamsung2.Location = new System.Drawing.Point(291, 9918);
            this.VerTodosComprarMicroSamsung2.Name = "VerTodosComprarMicroSamsung2";
            this.VerTodosComprarMicroSamsung2.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarMicroSamsung2.TabIndex = 445;
            this.VerTodosComprarMicroSamsung2.Text = "Comprar";
            this.VerTodosComprarMicroSamsung2.UseVisualStyleBackColor = true;
            // 
            // label146
            // 
            this.label146.AutoSize = true;
            this.label146.Location = new System.Drawing.Point(278, 10083);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(246, 39);
            this.label146.TabIndex = 444;
            this.label146.Text = "5 niveles de potencia\r\n3 Etapas de cocción en un solo toque\r\n10 Botones programab" +
    "les y 20 memorias diferentes";
            // 
            // label147
            // 
            this.label147.AutoSize = true;
            this.label147.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label147.Location = new System.Drawing.Point(286, 10058);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(65, 15);
            this.label147.TabIndex = 443;
            this.label147.Text = "$ 5,390.00";
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label148.Location = new System.Drawing.Point(279, 10032);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(107, 15);
            this.label148.TabIndex = 442;
            this.label148.Text = "Marca: SAMSUNG";
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label149.Location = new System.Drawing.Point(277, 9979);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(547, 44);
            this.label149.TabIndex = 441;
            this.label149.Text = "Horno de Microondas Daewoo Industrial 1.0 Pies Cúbicos \r\nAcero Inox";
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Location = new System.Drawing.Point(289, 9834);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(143, 65);
            this.label150.TabIndex = 440;
            this.label150.Text = "- Fácil Limpieza\r\n- Tamaño: 33.7 x 65.3 x 50.4\r\n- Con plato giratorio\r\n- Botón Pa" +
    "lomitas\r\n- 6 opciones de cocción";
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label151.Location = new System.Drawing.Point(289, 9810);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(62, 15);
            this.label151.TabIndex = 439;
            this.label151.Text = "$ 2190.00";
            // 
            // label152
            // 
            this.label152.AutoSize = true;
            this.label152.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label152.Location = new System.Drawing.Point(289, 9783);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(107, 15);
            this.label152.TabIndex = 438;
            this.label152.Text = "Marca: SAMSUNG";
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label153.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label153.Location = new System.Drawing.Point(294, 9739);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(324, 44);
            this.label153.TabIndex = 437;
            this.label153.Text = "Horno de Microondas\r\n1.6 Pies Cúbicos Acero Inoxidable";
            // 
            // VerTodosAgregarMicroSamsung1
            // 
            this.VerTodosAgregarMicroSamsung1.Location = new System.Drawing.Point(420, 9674);
            this.VerTodosAgregarMicroSamsung1.Name = "VerTodosAgregarMicroSamsung1";
            this.VerTodosAgregarMicroSamsung1.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarMicroSamsung1.TabIndex = 436;
            this.VerTodosAgregarMicroSamsung1.Text = "Agregar al carrito";
            this.VerTodosAgregarMicroSamsung1.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarMicroSamsung1
            // 
            this.VerTodosComprarMicroSamsung1.Location = new System.Drawing.Point(281, 9674);
            this.VerTodosComprarMicroSamsung1.Name = "VerTodosComprarMicroSamsung1";
            this.VerTodosComprarMicroSamsung1.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarMicroSamsung1.TabIndex = 435;
            this.VerTodosComprarMicroSamsung1.Text = "Comprar";
            this.VerTodosComprarMicroSamsung1.UseVisualStyleBackColor = true;
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Location = new System.Drawing.Point(289, 9616);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(137, 39);
            this.label154.TabIndex = 434;
            this.label154.Text = "Fácil Limpieza\r\nTamaño: 30.1 x 55.8 x 43.6\r\nCon plato giratorio";
            // 
            // label155
            // 
            this.label155.AutoSize = true;
            this.label155.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label155.Location = new System.Drawing.Point(289, 9591);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(65, 15);
            this.label155.TabIndex = 433;
            this.label155.Text = "$ 2,590.00";
            // 
            // label156
            // 
            this.label156.AutoSize = true;
            this.label156.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label156.Location = new System.Drawing.Point(289, 9567);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(107, 15);
            this.label156.TabIndex = 432;
            this.label156.Text = "Marca: SAMSUNG";
            // 
            // label157
            // 
            this.label157.AutoSize = true;
            this.label157.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label157.Location = new System.Drawing.Point(288, 9545);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(514, 22);
            this.label157.TabIndex = 431;
            this.label157.Text = "Horno de Microondas Samsung 1.4 Pies Cúbicos Plata\r\n";
            // 
            // VerTodosComprarMicroWhirlpool
            // 
            this.VerTodosComprarMicroWhirlpool.Location = new System.Drawing.Point(273, 10840);
            this.VerTodosComprarMicroWhirlpool.Name = "VerTodosComprarMicroWhirlpool";
            this.VerTodosComprarMicroWhirlpool.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarMicroWhirlpool.TabIndex = 470;
            this.VerTodosComprarMicroWhirlpool.Text = "Comprar";
            this.VerTodosComprarMicroWhirlpool.UseVisualStyleBackColor = true;
            // 
            // VerTodosAgregarMicroWhirlpool3
            // 
            this.VerTodosAgregarMicroWhirlpool3.Location = new System.Drawing.Point(412, 10840);
            this.VerTodosAgregarMicroWhirlpool3.Name = "VerTodosAgregarMicroWhirlpool3";
            this.VerTodosAgregarMicroWhirlpool3.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarMicroWhirlpool3.TabIndex = 469;
            this.VerTodosAgregarMicroWhirlpool3.Text = "Agregar al carrito";
            this.VerTodosAgregarMicroWhirlpool3.UseVisualStyleBackColor = true;
            // 
            // VerTodosAgregarMicroWhirlpool2
            // 
            this.VerTodosAgregarMicroWhirlpool2.Location = new System.Drawing.Point(412, 10649);
            this.VerTodosAgregarMicroWhirlpool2.Name = "VerTodosAgregarMicroWhirlpool2";
            this.VerTodosAgregarMicroWhirlpool2.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarMicroWhirlpool2.TabIndex = 468;
            this.VerTodosAgregarMicroWhirlpool2.Text = "Agregar al carrito";
            this.VerTodosAgregarMicroWhirlpool2.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarMicroWhirlpool2
            // 
            this.VerTodosComprarMicroWhirlpool2.Location = new System.Drawing.Point(273, 10649);
            this.VerTodosComprarMicroWhirlpool2.Name = "VerTodosComprarMicroWhirlpool2";
            this.VerTodosComprarMicroWhirlpool2.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarMicroWhirlpool2.TabIndex = 467;
            this.VerTodosComprarMicroWhirlpool2.Text = "Comprar";
            this.VerTodosComprarMicroWhirlpool2.UseVisualStyleBackColor = true;
            // 
            // label158
            // 
            this.label158.AutoSize = true;
            this.label158.Location = new System.Drawing.Point(281, 10777);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(181, 39);
            this.label158.TabIndex = 466;
            this.label158.Text = "10 Niveles de potencia\r\nFunciones predeterminadas\r\nDescongelamiento por peso y ti" +
    "empo";
            // 
            // label159
            // 
            this.label159.AutoSize = true;
            this.label159.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label159.Location = new System.Drawing.Point(271, 10752);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(65, 15);
            this.label159.TabIndex = 465;
            this.label159.Text = "$ 2,290.00";
            // 
            // label160
            // 
            this.label160.AutoSize = true;
            this.label160.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label160.Location = new System.Drawing.Point(270, 10726);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(120, 15);
            this.label160.TabIndex = 464;
            this.label160.Text = "Marca: WHIRLPOOL";
            // 
            // label161
            // 
            this.label161.AutoSize = true;
            this.label161.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label161.Location = new System.Drawing.Point(270, 10704);
            this.label161.Name = "label161";
            this.label161.Size = new System.Drawing.Size(463, 22);
            this.label161.TabIndex = 463;
            this.label161.Text = "Horno de Microondas Whirlpool 1.1 Pies Cúbicos";
            // 
            // label162
            // 
            this.label162.AutoSize = true;
            this.label162.Location = new System.Drawing.Point(281, 10589);
            this.label162.Name = "label162";
            this.label162.Size = new System.Drawing.Size(205, 39);
            this.label162.TabIndex = 462;
            this.label162.Text = "- Funciones predeterminadas de alimentos\r\n- 3 opciones de descongelar por peso\r\n-" +
    " Diseño moderno";
            // 
            // label163
            // 
            this.label163.AutoSize = true;
            this.label163.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label163.Location = new System.Drawing.Point(281, 10562);
            this.label163.Name = "label163";
            this.label163.Size = new System.Drawing.Size(62, 15);
            this.label163.TabIndex = 461;
            this.label163.Text = "$ 2190.00";
            // 
            // label164
            // 
            this.label164.AutoSize = true;
            this.label164.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label164.Location = new System.Drawing.Point(281, 10527);
            this.label164.Name = "label164";
            this.label164.Size = new System.Drawing.Size(120, 15);
            this.label164.TabIndex = 460;
            this.label164.Text = "Marca: WHIRLPOOL";
            // 
            // label165
            // 
            this.label165.AutoSize = true;
            this.label165.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label165.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label165.Location = new System.Drawing.Point(286, 10505);
            this.label165.Name = "label165";
            this.label165.Size = new System.Drawing.Size(470, 22);
            this.label165.TabIndex = 459;
            this.label165.Text = "Horno de Microondas Whirlpool WM1511D Blanco";
            // 
            // VerTodosAgregarMicroWhirlpool
            // 
            this.VerTodosAgregarMicroWhirlpool.Location = new System.Drawing.Point(412, 10440);
            this.VerTodosAgregarMicroWhirlpool.Name = "VerTodosAgregarMicroWhirlpool";
            this.VerTodosAgregarMicroWhirlpool.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarMicroWhirlpool.TabIndex = 458;
            this.VerTodosAgregarMicroWhirlpool.Text = "Agregar al carrito";
            this.VerTodosAgregarMicroWhirlpool.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarMicroWhirlpool1
            // 
            this.VerTodosComprarMicroWhirlpool1.Location = new System.Drawing.Point(273, 10440);
            this.VerTodosComprarMicroWhirlpool1.Name = "VerTodosComprarMicroWhirlpool1";
            this.VerTodosComprarMicroWhirlpool1.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarMicroWhirlpool1.TabIndex = 457;
            this.VerTodosComprarMicroWhirlpool1.Text = "Comprar";
            this.VerTodosComprarMicroWhirlpool1.UseVisualStyleBackColor = true;
            // 
            // label166
            // 
            this.label166.AutoSize = true;
            this.label166.Location = new System.Drawing.Point(281, 10382);
            this.label166.Name = "label166";
            this.label166.Size = new System.Drawing.Size(155, 39);
            this.label166.TabIndex = 456;
            this.label166.Text = "9 opciones para cocinar\r\nDescongela alimentos por peso\r\nBloqueo de controles";
            // 
            // label167
            // 
            this.label167.AutoSize = true;
            this.label167.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label167.Location = new System.Drawing.Point(281, 10357);
            this.label167.Name = "label167";
            this.label167.Size = new System.Drawing.Size(65, 15);
            this.label167.TabIndex = 455;
            this.label167.Text = "$ 2,035.00";
            // 
            // label168
            // 
            this.label168.AutoSize = true;
            this.label168.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label168.Location = new System.Drawing.Point(281, 10333);
            this.label168.Name = "label168";
            this.label168.Size = new System.Drawing.Size(120, 15);
            this.label168.TabIndex = 454;
            this.label168.Text = "Marca: WHIRLPOOL";
            // 
            // label169
            // 
            this.label169.AutoSize = true;
            this.label169.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label169.Location = new System.Drawing.Point(280, 10311);
            this.label169.Name = "label169";
            this.label169.Size = new System.Drawing.Size(521, 22);
            this.label169.TabIndex = 453;
            this.label169.Text = "Horno de Microondas Whirlpool 1.1 Pies Cúbicos Silver";
            // 
            // VerTodosComprarMicroMabe3
            // 
            this.VerTodosComprarMicroMabe3.Location = new System.Drawing.Point(265, 11570);
            this.VerTodosComprarMicroMabe3.Name = "VerTodosComprarMicroMabe3";
            this.VerTodosComprarMicroMabe3.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarMicroMabe3.TabIndex = 492;
            this.VerTodosComprarMicroMabe3.Text = "Comprar";
            this.VerTodosComprarMicroMabe3.UseVisualStyleBackColor = true;
            // 
            // VerTodosAgregarMicroMabe3
            // 
            this.VerTodosAgregarMicroMabe3.Location = new System.Drawing.Point(403, 11570);
            this.VerTodosAgregarMicroMabe3.Name = "VerTodosAgregarMicroMabe3";
            this.VerTodosAgregarMicroMabe3.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarMicroMabe3.TabIndex = 491;
            this.VerTodosAgregarMicroMabe3.Text = "Agregar al carrito";
            this.VerTodosAgregarMicroMabe3.UseVisualStyleBackColor = true;
            // 
            // VerTodosAgreagrMicroMabe2
            // 
            this.VerTodosAgreagrMicroMabe2.Location = new System.Drawing.Point(403, 11339);
            this.VerTodosAgreagrMicroMabe2.Name = "VerTodosAgreagrMicroMabe2";
            this.VerTodosAgreagrMicroMabe2.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgreagrMicroMabe2.TabIndex = 490;
            this.VerTodosAgreagrMicroMabe2.Text = "Agregar al carrito";
            this.VerTodosAgreagrMicroMabe2.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarMicroMabe2
            // 
            this.VerTodosComprarMicroMabe2.Location = new System.Drawing.Point(265, 11339);
            this.VerTodosComprarMicroMabe2.Name = "VerTodosComprarMicroMabe2";
            this.VerTodosComprarMicroMabe2.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarMicroMabe2.TabIndex = 489;
            this.VerTodosComprarMicroMabe2.Text = "Comprar";
            this.VerTodosComprarMicroMabe2.UseVisualStyleBackColor = true;
            // 
            // label170
            // 
            this.label170.AutoSize = true;
            this.label170.Location = new System.Drawing.Point(272, 11471);
            this.label170.Name = "label170";
            this.label170.Size = new System.Drawing.Size(386, 91);
            this.label170.TabIndex = 488;
            this.label170.Text = resources.GetString("label170.Text");
            // 
            // label171
            // 
            this.label171.AutoSize = true;
            this.label171.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label171.Location = new System.Drawing.Point(262, 11446);
            this.label171.Name = "label171";
            this.label171.Size = new System.Drawing.Size(58, 15);
            this.label171.TabIndex = 487;
            this.label171.Text = " 1,743.00";
            // 
            // label172
            // 
            this.label172.AutoSize = true;
            this.label172.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label172.Location = new System.Drawing.Point(261, 11420);
            this.label172.Name = "label172";
            this.label172.Size = new System.Drawing.Size(82, 15);
            this.label172.TabIndex = 486;
            this.label172.Text = "Marca: MABE";
            // 
            // label173
            // 
            this.label173.AutoSize = true;
            this.label173.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label173.Location = new System.Drawing.Point(261, 11398);
            this.label173.Name = "label173";
            this.label173.Size = new System.Drawing.Size(476, 22);
            this.label173.TabIndex = 485;
            this.label173.Text = "Horno Microondas Mabe 0.7 Pies Mabe HMM70SW";
            // 
            // label174
            // 
            this.label174.AutoSize = true;
            this.label174.Location = new System.Drawing.Point(272, 11281);
            this.label174.Name = "label174";
            this.label174.Size = new System.Drawing.Size(171, 39);
            this.label174.TabIndex = 484;
            this.label174.Text = "10 niveles de potencia\r\n6 selecciones rápidas de cocinado\r\nSeguro para niños";
            // 
            // label175
            // 
            this.label175.AutoSize = true;
            this.label175.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label175.Location = new System.Drawing.Point(272, 11266);
            this.label175.Name = "label175";
            this.label175.Size = new System.Drawing.Size(65, 15);
            this.label175.TabIndex = 483;
            this.label175.Text = "$ 2,900.00";
            // 
            // label176
            // 
            this.label176.AutoSize = true;
            this.label176.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label176.Location = new System.Drawing.Point(272, 11241);
            this.label176.Name = "label176";
            this.label176.Size = new System.Drawing.Size(82, 15);
            this.label176.TabIndex = 482;
            this.label176.Text = "Marca: MABE";
            // 
            // label177
            // 
            this.label177.AutoSize = true;
            this.label177.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label177.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label177.Location = new System.Drawing.Point(271, 11197);
            this.label177.Name = "label177";
            this.label177.Size = new System.Drawing.Size(309, 44);
            this.label177.TabIndex = 481;
            this.label177.Text = "HORNO DE MICROONDAS 1.4 P3\r\nCUFT ESPEJO PLATA Mabe ";
            // 
            // VerTodosAgregarMicroMabe1
            // 
            this.VerTodosAgregarMicroMabe1.Location = new System.Drawing.Point(403, 11134);
            this.VerTodosAgregarMicroMabe1.Name = "VerTodosAgregarMicroMabe1";
            this.VerTodosAgregarMicroMabe1.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarMicroMabe1.TabIndex = 480;
            this.VerTodosAgregarMicroMabe1.Text = "Agregar al carrito";
            this.VerTodosAgregarMicroMabe1.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarMicroMabe1
            // 
            this.VerTodosComprarMicroMabe1.Location = new System.Drawing.Point(264, 11134);
            this.VerTodosComprarMicroMabe1.Name = "VerTodosComprarMicroMabe1";
            this.VerTodosComprarMicroMabe1.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarMicroMabe1.TabIndex = 479;
            this.VerTodosComprarMicroMabe1.Text = "Comprar";
            this.VerTodosComprarMicroMabe1.UseVisualStyleBackColor = true;
            // 
            // label178
            // 
            this.label178.AutoSize = true;
            this.label178.Location = new System.Drawing.Point(272, 11076);
            this.label178.Name = "label178";
            this.label178.Size = new System.Drawing.Size(128, 39);
            this.label178.TabIndex = 478;
            this.label178.Text = "Fácil Limpieza\r\nTamaño: 26.2 x 45.2 x 33\r\nCon plato giratorio";
            // 
            // label179
            // 
            this.label179.AutoSize = true;
            this.label179.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label179.Location = new System.Drawing.Point(272, 11051);
            this.label179.Name = "label179";
            this.label179.Size = new System.Drawing.Size(55, 15);
            this.label179.TabIndex = 477;
            this.label179.Text = "$ 990.00";
            // 
            // label180
            // 
            this.label180.AutoSize = true;
            this.label180.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label180.Location = new System.Drawing.Point(272, 11027);
            this.label180.Name = "label180";
            this.label180.Size = new System.Drawing.Size(82, 15);
            this.label180.TabIndex = 476;
            this.label180.Text = "Marca: MABE";
            // 
            // label181
            // 
            this.label181.AutoSize = true;
            this.label181.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label181.Location = new System.Drawing.Point(271, 11005);
            this.label181.Name = "label181";
            this.label181.Size = new System.Drawing.Size(496, 22);
            this.label181.TabIndex = 475;
            this.label181.Text = "Horno de Microondas Mabe 0.7 Pies Cúbicos Blanco";
            // 
            // VerTodosComprarMicroDaewoo3
            // 
            this.VerTodosComprarMicroDaewoo3.Location = new System.Drawing.Point(278, 12283);
            this.VerTodosComprarMicroDaewoo3.Name = "VerTodosComprarMicroDaewoo3";
            this.VerTodosComprarMicroDaewoo3.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarMicroDaewoo3.TabIndex = 514;
            this.VerTodosComprarMicroDaewoo3.Text = "Comprar";
            this.VerTodosComprarMicroDaewoo3.UseVisualStyleBackColor = true;
            // 
            // VerTodosAgregarMicroDaewoo3
            // 
            this.VerTodosAgregarMicroDaewoo3.Location = new System.Drawing.Point(417, 12283);
            this.VerTodosAgregarMicroDaewoo3.Name = "VerTodosAgregarMicroDaewoo3";
            this.VerTodosAgregarMicroDaewoo3.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarMicroDaewoo3.TabIndex = 513;
            this.VerTodosAgregarMicroDaewoo3.Text = "Agregar al carrito";
            this.VerTodosAgregarMicroDaewoo3.UseVisualStyleBackColor = true;
            // 
            // VerTodosAgregarMicroDaewoo2
            // 
            this.VerTodosAgregarMicroDaewoo2.Location = new System.Drawing.Point(417, 12109);
            this.VerTodosAgregarMicroDaewoo2.Name = "VerTodosAgregarMicroDaewoo2";
            this.VerTodosAgregarMicroDaewoo2.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarMicroDaewoo2.TabIndex = 512;
            this.VerTodosAgregarMicroDaewoo2.Text = "Agregar al carrito";
            this.VerTodosAgregarMicroDaewoo2.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarMicroDaewoo2
            // 
            this.VerTodosComprarMicroDaewoo2.Location = new System.Drawing.Point(279, 12109);
            this.VerTodosComprarMicroDaewoo2.Name = "VerTodosComprarMicroDaewoo2";
            this.VerTodosComprarMicroDaewoo2.Size = new System.Drawing.Size(105, 23);
            this.VerTodosComprarMicroDaewoo2.TabIndex = 511;
            this.VerTodosComprarMicroDaewoo2.Text = "Comprar";
            this.VerTodosComprarMicroDaewoo2.UseVisualStyleBackColor = true;
            // 
            // label182
            // 
            this.label182.AutoSize = true;
            this.label182.Location = new System.Drawing.Point(276, 12229);
            this.label182.Name = "label182";
            this.label182.Size = new System.Drawing.Size(269, 39);
            this.label182.TabIndex = 510;
            this.label182.Text = "- Función de descongelamiento por tiempo o por peso\r\n- 4 opciones de cocción rápi" +
    "do\r\n- Bloqueo de panel para evitar accidentes con los niños";
            // 
            // label183
            // 
            this.label183.AutoSize = true;
            this.label183.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label183.Location = new System.Drawing.Point(276, 12198);
            this.label183.Name = "label183";
            this.label183.Size = new System.Drawing.Size(65, 15);
            this.label183.TabIndex = 509;
            this.label183.Text = "$ 2,290.00";
            // 
            // label184
            // 
            this.label184.AutoSize = true;
            this.label184.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label184.Location = new System.Drawing.Point(276, 12169);
            this.label184.Name = "label184";
            this.label184.Size = new System.Drawing.Size(102, 15);
            this.label184.TabIndex = 508;
            this.label184.Text = "Marca: DAEWOO";
            // 
            // label185
            // 
            this.label185.AutoSize = true;
            this.label185.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label185.Location = new System.Drawing.Point(275, 12147);
            this.label185.Name = "label185";
            this.label185.Size = new System.Drawing.Size(509, 22);
            this.label185.TabIndex = 507;
            this.label185.Text = "Horno de Microondas Daewoo 1.1 Pies Cúbicos Acero";
            // 
            // label186
            // 
            this.label186.AutoSize = true;
            this.label186.Location = new System.Drawing.Point(270, 11997);
            this.label186.Name = "label186";
            this.label186.Size = new System.Drawing.Size(211, 91);
            this.label186.TabIndex = 506;
            this.label186.Text = resources.GetString("label186.Text");
            // 
            // label187
            // 
            this.label187.AutoSize = true;
            this.label187.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label187.Location = new System.Drawing.Point(270, 11970);
            this.label187.Name = "label187";
            this.label187.Size = new System.Drawing.Size(65, 15);
            this.label187.TabIndex = 505;
            this.label187.Text = "$ 1,199.00";
            // 
            // label188
            // 
            this.label188.AutoSize = true;
            this.label188.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label188.Location = new System.Drawing.Point(270, 11944);
            this.label188.Name = "label188";
            this.label188.Size = new System.Drawing.Size(102, 15);
            this.label188.TabIndex = 504;
            this.label188.Text = "Marca: DAEWOO";
            // 
            // label189
            // 
            this.label189.AutoSize = true;
            this.label189.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label189.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label189.Location = new System.Drawing.Point(269, 11922);
            this.label189.Name = "label189";
            this.label189.Size = new System.Drawing.Size(469, 22);
            this.label189.TabIndex = 503;
            this.label189.Text = "Horno De Microondas 0.7 Pies Daewoo KOR-660S";
            // 
            // VerTodosAgregarMicroDaewoo
            // 
            this.VerTodosAgregarMicroDaewoo.Location = new System.Drawing.Point(417, 11887);
            this.VerTodosAgregarMicroDaewoo.Name = "VerTodosAgregarMicroDaewoo";
            this.VerTodosAgregarMicroDaewoo.Size = new System.Drawing.Size(105, 23);
            this.VerTodosAgregarMicroDaewoo.TabIndex = 502;
            this.VerTodosAgregarMicroDaewoo.Text = "Agregar al carrito";
            this.VerTodosAgregarMicroDaewoo.UseVisualStyleBackColor = true;
            // 
            // VerTodosComprarMicoDaewoo1
            // 
            this.VerTodosComprarMicoDaewoo1.Location = new System.Drawing.Point(273, 11887);
            this.VerTodosComprarMicoDaewoo1.Name = "VerTodosComprarMicoDaewoo1";
            this.VerTodosComprarMicoDaewoo1.Size = new System.Drawing.Size(110, 23);
            this.VerTodosComprarMicoDaewoo1.TabIndex = 501;
            this.VerTodosComprarMicoDaewoo1.Text = "Comprar";
            this.VerTodosComprarMicoDaewoo1.UseVisualStyleBackColor = true;
            // 
            // label190
            // 
            this.label190.AutoSize = true;
            this.label190.Location = new System.Drawing.Point(270, 11790);
            this.label190.Name = "label190";
            this.label190.Size = new System.Drawing.Size(199, 78);
            this.label190.TabIndex = 500;
            this.label190.Text = "Sistema de cóncavo de reflexión\r\nOperación fácil\r\nOpciones de Descongelado por al" +
    "imento\r\n5 Menús de auto cocción\r\n10 Niveles de potencia\r\nBotón de inicio rápido";
            // 
            // label191
            // 
            this.label191.AutoSize = true;
            this.label191.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label191.Location = new System.Drawing.Point(270, 11765);
            this.label191.Name = "label191";
            this.label191.Size = new System.Drawing.Size(0, 15);
            this.label191.TabIndex = 499;
            // 
            // label192
            // 
            this.label192.AutoSize = true;
            this.label192.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label192.Location = new System.Drawing.Point(270, 11741);
            this.label192.Name = "label192";
            this.label192.Size = new System.Drawing.Size(102, 15);
            this.label192.TabIndex = 498;
            this.label192.Text = "Marca: DAEWOO";
            // 
            // label193
            // 
            this.label193.AutoSize = true;
            this.label193.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label193.Location = new System.Drawing.Point(269, 11719);
            this.label193.Name = "label193";
            this.label193.Size = new System.Drawing.Size(353, 22);
            this.label193.TabIndex = 497;
            this.label193.Text = "Horno Daewoo DAEWOO KOR-1N0AS";
            // 
            // BtnCarrito
            // 
            this.BtnCarrito.Image = global::Comercializadora.Properties.Resources.carro_de_la_compra__3_;
            this.BtnCarrito.Location = new System.Drawing.Point(849, 23);
            this.BtnCarrito.Name = "BtnCarrito";
            this.BtnCarrito.Size = new System.Drawing.Size(43, 45);
            this.BtnCarrito.TabIndex = 515;
            this.BtnCarrito.UseVisualStyleBackColor = false;
            this.BtnCarrito.Click += new System.EventHandler(this.BtnCarrito_Click);
            // 
            // pictureBox61
            // 
            this.pictureBox61.Image = global::Comercializadora.Properties.Resources.MicroDaewoo3;
            this.pictureBox61.Location = new System.Drawing.Point(7, 12147);
            this.pictureBox61.Name = "pictureBox61";
            this.pictureBox61.Size = new System.Drawing.Size(250, 145);
            this.pictureBox61.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox61.TabIndex = 496;
            this.pictureBox61.TabStop = false;
            // 
            // pictureBox62
            // 
            this.pictureBox62.Image = global::Comercializadora.Properties.Resources.MicroDaewoo2;
            this.pictureBox62.Location = new System.Drawing.Point(8, 11931);
            this.pictureBox62.Name = "pictureBox62";
            this.pictureBox62.Size = new System.Drawing.Size(249, 145);
            this.pictureBox62.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox62.TabIndex = 495;
            this.pictureBox62.TabStop = false;
            // 
            // pictureBox63
            // 
            this.pictureBox63.Image = global::Comercializadora.Properties.Resources.MicroDaewoo1;
            this.pictureBox63.Location = new System.Drawing.Point(11, 11732);
            this.pictureBox63.Name = "pictureBox63";
            this.pictureBox63.Size = new System.Drawing.Size(246, 145);
            this.pictureBox63.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox63.TabIndex = 494;
            this.pictureBox63.TabStop = false;
            // 
            // pictureBox64
            // 
            this.pictureBox64.Image = global::Comercializadora.Properties.Resources.Daewoo;
            this.pictureBox64.Location = new System.Drawing.Point(316, 11618);
            this.pictureBox64.Name = "pictureBox64";
            this.pictureBox64.Size = new System.Drawing.Size(236, 66);
            this.pictureBox64.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox64.TabIndex = 493;
            this.pictureBox64.TabStop = false;
            // 
            // pictureBox57
            // 
            this.pictureBox57.Image = global::Comercializadora.Properties.Resources.MicroMabe3;
            this.pictureBox57.Location = new System.Drawing.Point(17, 11398);
            this.pictureBox57.Name = "pictureBox57";
            this.pictureBox57.Size = new System.Drawing.Size(229, 139);
            this.pictureBox57.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox57.TabIndex = 474;
            this.pictureBox57.TabStop = false;
            // 
            // pictureBox58
            // 
            this.pictureBox58.Image = global::Comercializadora.Properties.Resources.MicroMabe2;
            this.pictureBox58.Location = new System.Drawing.Point(17, 11212);
            this.pictureBox58.Name = "pictureBox58";
            this.pictureBox58.Size = new System.Drawing.Size(220, 129);
            this.pictureBox58.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox58.TabIndex = 473;
            this.pictureBox58.TabStop = false;
            // 
            // pictureBox59
            // 
            this.pictureBox59.Image = global::Comercializadora.Properties.Resources.MicroMabe1;
            this.pictureBox59.Location = new System.Drawing.Point(17, 11016);
            this.pictureBox59.Name = "pictureBox59";
            this.pictureBox59.Size = new System.Drawing.Size(220, 130);
            this.pictureBox59.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox59.TabIndex = 472;
            this.pictureBox59.TabStop = false;
            // 
            // pictureBox60
            // 
            this.pictureBox60.Image = global::Comercializadora.Properties.Resources.Mabe;
            this.pictureBox60.Location = new System.Drawing.Point(291, 10897);
            this.pictureBox60.Name = "pictureBox60";
            this.pictureBox60.Size = new System.Drawing.Size(226, 87);
            this.pictureBox60.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox60.TabIndex = 471;
            this.pictureBox60.TabStop = false;
            // 
            // pictureBox53
            // 
            this.pictureBox53.Image = global::Comercializadora.Properties.Resources.MicroWhirlpool3;
            this.pictureBox53.Location = new System.Drawing.Point(25, 10704);
            this.pictureBox53.Name = "pictureBox53";
            this.pictureBox53.Size = new System.Drawing.Size(216, 131);
            this.pictureBox53.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox53.TabIndex = 452;
            this.pictureBox53.TabStop = false;
            // 
            // pictureBox54
            // 
            this.pictureBox54.Image = global::Comercializadora.Properties.Resources.MicroWhirlpool2;
            this.pictureBox54.Location = new System.Drawing.Point(25, 10505);
            this.pictureBox54.Name = "pictureBox54";
            this.pictureBox54.Size = new System.Drawing.Size(216, 123);
            this.pictureBox54.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox54.TabIndex = 451;
            this.pictureBox54.TabStop = false;
            // 
            // pictureBox55
            // 
            this.pictureBox55.Image = global::Comercializadora.Properties.Resources.MicroWhirlpool1;
            this.pictureBox55.Location = new System.Drawing.Point(25, 10311);
            this.pictureBox55.Name = "pictureBox55";
            this.pictureBox55.Size = new System.Drawing.Size(216, 132);
            this.pictureBox55.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox55.TabIndex = 450;
            this.pictureBox55.TabStop = false;
            // 
            // pictureBox56
            // 
            this.pictureBox56.Image = global::Comercializadora.Properties.Resources.whirlpool;
            this.pictureBox56.Location = new System.Drawing.Point(281, 10187);
            this.pictureBox56.Name = "pictureBox56";
            this.pictureBox56.Size = new System.Drawing.Size(237, 104);
            this.pictureBox56.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox56.TabIndex = 449;
            this.pictureBox56.TabStop = false;
            // 
            // pictureBox49
            // 
            this.pictureBox49.Image = global::Comercializadora.Properties.Resources.MicroSamsung2;
            this.pictureBox49.Location = new System.Drawing.Point(16, 9761);
            this.pictureBox49.Name = "pictureBox49";
            this.pictureBox49.Size = new System.Drawing.Size(227, 129);
            this.pictureBox49.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox49.TabIndex = 430;
            this.pictureBox49.TabStop = false;
            // 
            // pictureBox50
            // 
            this.pictureBox50.Image = global::Comercializadora.Properties.Resources.MicroSamsung3;
            this.pictureBox50.Location = new System.Drawing.Point(16, 9998);
            this.pictureBox50.Name = "pictureBox50";
            this.pictureBox50.Size = new System.Drawing.Size(227, 135);
            this.pictureBox50.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox50.TabIndex = 429;
            this.pictureBox50.TabStop = false;
            // 
            // pictureBox51
            // 
            this.pictureBox51.Image = global::Comercializadora.Properties.Resources.MicroSamsung1;
            this.pictureBox51.Location = new System.Drawing.Point(13, 9545);
            this.pictureBox51.Name = "pictureBox51";
            this.pictureBox51.Size = new System.Drawing.Size(230, 129);
            this.pictureBox51.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox51.TabIndex = 428;
            this.pictureBox51.TabStop = false;
            // 
            // pictureBox52
            // 
            this.pictureBox52.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox52.Image = global::Comercializadora.Properties.Resources.samsung1;
            this.pictureBox52.Location = new System.Drawing.Point(298, 9467);
            this.pictureBox52.Name = "pictureBox52";
            this.pictureBox52.Size = new System.Drawing.Size(231, 50);
            this.pictureBox52.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox52.TabIndex = 427;
            this.pictureBox52.TabStop = false;
            // 
            // pictureBox45
            // 
            this.pictureBox45.Image = global::Comercializadora.Properties.Resources.Lg;
            this.pictureBox45.Location = new System.Drawing.Point(281, 8641);
            this.pictureBox45.Name = "pictureBox45";
            this.pictureBox45.Size = new System.Drawing.Size(293, 136);
            this.pictureBox45.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox45.TabIndex = 408;
            this.pictureBox45.TabStop = false;
            // 
            // pictureBox46
            // 
            this.pictureBox46.Image = global::Comercializadora.Properties.Resources.RefriLg6;
            this.pictureBox46.Location = new System.Drawing.Point(13, 9243);
            this.pictureBox46.Name = "pictureBox46";
            this.pictureBox46.Size = new System.Drawing.Size(117, 213);
            this.pictureBox46.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox46.TabIndex = 407;
            this.pictureBox46.TabStop = false;
            // 
            // pictureBox47
            // 
            this.pictureBox47.Image = global::Comercializadora.Properties.Resources.RefriLg4;
            this.pictureBox47.Location = new System.Drawing.Point(13, 9016);
            this.pictureBox47.Name = "pictureBox47";
            this.pictureBox47.Size = new System.Drawing.Size(117, 212);
            this.pictureBox47.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox47.TabIndex = 406;
            this.pictureBox47.TabStop = false;
            // 
            // pictureBox48
            // 
            this.pictureBox48.Image = global::Comercializadora.Properties.Resources.RefriLG1;
            this.pictureBox48.Location = new System.Drawing.Point(12, 8797);
            this.pictureBox48.Name = "pictureBox48";
            this.pictureBox48.Size = new System.Drawing.Size(118, 202);
            this.pictureBox48.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox48.TabIndex = 405;
            this.pictureBox48.TabStop = false;
            // 
            // pictureBox41
            // 
            this.pictureBox41.Image = global::Comercializadora.Properties.Resources.refrimabe3;
            this.pictureBox41.Location = new System.Drawing.Point(11, 8431);
            this.pictureBox41.Name = "pictureBox41";
            this.pictureBox41.Size = new System.Drawing.Size(112, 190);
            this.pictureBox41.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox41.TabIndex = 386;
            this.pictureBox41.TabStop = false;
            // 
            // pictureBox42
            // 
            this.pictureBox42.Image = global::Comercializadora.Properties.Resources.Refrimabe2;
            this.pictureBox42.Location = new System.Drawing.Point(11, 8236);
            this.pictureBox42.Name = "pictureBox42";
            this.pictureBox42.Size = new System.Drawing.Size(112, 172);
            this.pictureBox42.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox42.TabIndex = 385;
            this.pictureBox42.TabStop = false;
            // 
            // pictureBox43
            // 
            this.pictureBox43.Image = global::Comercializadora.Properties.Resources.RefriMabe;
            this.pictureBox43.Location = new System.Drawing.Point(11, 8042);
            this.pictureBox43.Name = "pictureBox43";
            this.pictureBox43.Size = new System.Drawing.Size(112, 175);
            this.pictureBox43.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox43.TabIndex = 384;
            this.pictureBox43.TabStop = false;
            // 
            // pictureBox44
            // 
            this.pictureBox44.Image = global::Comercializadora.Properties.Resources.Mabe;
            this.pictureBox44.Location = new System.Drawing.Point(275, 7952);
            this.pictureBox44.Name = "pictureBox44";
            this.pictureBox44.Size = new System.Drawing.Size(231, 66);
            this.pictureBox44.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox44.TabIndex = 383;
            this.pictureBox44.TabStop = false;
            // 
            // pictureBox37
            // 
            this.pictureBox37.Image = global::Comercializadora.Properties.Resources.Refrigeradorsamsung3;
            this.pictureBox37.Location = new System.Drawing.Point(12, 7723);
            this.pictureBox37.Name = "pictureBox37";
            this.pictureBox37.Size = new System.Drawing.Size(125, 210);
            this.pictureBox37.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox37.TabIndex = 363;
            this.pictureBox37.TabStop = false;
            // 
            // pictureBox38
            // 
            this.pictureBox38.Image = global::Comercializadora.Properties.Resources.Refrigerador_Samsung2;
            this.pictureBox38.Location = new System.Drawing.Point(12, 7511);
            this.pictureBox38.Name = "pictureBox38";
            this.pictureBox38.Size = new System.Drawing.Size(125, 206);
            this.pictureBox38.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox38.TabIndex = 362;
            this.pictureBox38.TabStop = false;
            // 
            // pictureBox39
            // 
            this.pictureBox39.Image = global::Comercializadora.Properties.Resources.Refrigerador_Samsung;
            this.pictureBox39.Location = new System.Drawing.Point(19, 7299);
            this.pictureBox39.Name = "pictureBox39";
            this.pictureBox39.Size = new System.Drawing.Size(118, 206);
            this.pictureBox39.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox39.TabIndex = 361;
            this.pictureBox39.TabStop = false;
            // 
            // pictureBox40
            // 
            this.pictureBox40.Image = global::Comercializadora.Properties.Resources.samsung;
            this.pictureBox40.Location = new System.Drawing.Point(289, 7212);
            this.pictureBox40.Name = "pictureBox40";
            this.pictureBox40.Size = new System.Drawing.Size(223, 71);
            this.pictureBox40.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox40.TabIndex = 360;
            this.pictureBox40.TabStop = false;
            // 
            // pictureBox33
            // 
            this.pictureBox33.Image = global::Comercializadora.Properties.Resources.Refriwhirlpool3;
            this.pictureBox33.Location = new System.Drawing.Point(12, 6990);
            this.pictureBox33.Name = "pictureBox33";
            this.pictureBox33.Size = new System.Drawing.Size(117, 194);
            this.pictureBox33.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox33.TabIndex = 341;
            this.pictureBox33.TabStop = false;
            // 
            // pictureBox34
            // 
            this.pictureBox34.Image = global::Comercializadora.Properties.Resources.RefriWhirlpool2;
            this.pictureBox34.Location = new System.Drawing.Point(12, 6763);
            this.pictureBox34.Name = "pictureBox34";
            this.pictureBox34.Size = new System.Drawing.Size(117, 203);
            this.pictureBox34.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox34.TabIndex = 340;
            this.pictureBox34.TabStop = false;
            // 
            // pictureBox35
            // 
            this.pictureBox35.Image = global::Comercializadora.Properties.Resources.RefriWhilpoll1;
            this.pictureBox35.Location = new System.Drawing.Point(12, 6532);
            this.pictureBox35.Name = "pictureBox35";
            this.pictureBox35.Size = new System.Drawing.Size(117, 212);
            this.pictureBox35.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox35.TabIndex = 339;
            this.pictureBox35.TabStop = false;
            // 
            // pictureBox36
            // 
            this.pictureBox36.Image = global::Comercializadora.Properties.Resources.whirlpool;
            this.pictureBox36.Location = new System.Drawing.Point(282, 6421);
            this.pictureBox36.Name = "pictureBox36";
            this.pictureBox36.Size = new System.Drawing.Size(236, 104);
            this.pictureBox36.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox36.TabIndex = 338;
            this.pictureBox36.TabStop = false;
            // 
            // pictureBox29
            // 
            this.pictureBox29.Image = global::Comercializadora.Properties.Resources.LavaGeneric3;
            this.pictureBox29.Location = new System.Drawing.Point(12, 6197);
            this.pictureBox29.Name = "pictureBox29";
            this.pictureBox29.Size = new System.Drawing.Size(152, 207);
            this.pictureBox29.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox29.TabIndex = 319;
            this.pictureBox29.TabStop = false;
            // 
            // pictureBox30
            // 
            this.pictureBox30.Image = global::Comercializadora.Properties.Resources.LavaGeneric2_1;
            this.pictureBox30.Location = new System.Drawing.Point(12, 5977);
            this.pictureBox30.Name = "pictureBox30";
            this.pictureBox30.Size = new System.Drawing.Size(151, 214);
            this.pictureBox30.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox30.TabIndex = 318;
            this.pictureBox30.TabStop = false;
            // 
            // pictureBox31
            // 
            this.pictureBox31.Image = global::Comercializadora.Properties.Resources.LavaGeneric11;
            this.pictureBox31.Location = new System.Drawing.Point(12, 5725);
            this.pictureBox31.Name = "pictureBox31";
            this.pictureBox31.Size = new System.Drawing.Size(152, 229);
            this.pictureBox31.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox31.TabIndex = 317;
            this.pictureBox31.TabStop = false;
            // 
            // pictureBox32
            // 
            this.pictureBox32.Image = global::Comercializadora.Properties.Resources.Generic_Electronic;
            this.pictureBox32.Location = new System.Drawing.Point(173, 5593);
            this.pictureBox32.Name = "pictureBox32";
            this.pictureBox32.Size = new System.Drawing.Size(440, 106);
            this.pictureBox32.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox32.TabIndex = 316;
            this.pictureBox32.TabStop = false;
            // 
            // pictureBox25
            // 
            this.pictureBox25.Image = global::Comercializadora.Properties.Resources.LavaLg3;
            this.pictureBox25.Location = new System.Drawing.Point(12, 5331);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(158, 216);
            this.pictureBox25.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox25.TabIndex = 297;
            this.pictureBox25.TabStop = false;
            // 
            // pictureBox26
            // 
            this.pictureBox26.Image = global::Comercializadora.Properties.Resources.LavaLg2;
            this.pictureBox26.Location = new System.Drawing.Point(12, 5114);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(158, 196);
            this.pictureBox26.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox26.TabIndex = 296;
            this.pictureBox26.TabStop = false;
            // 
            // pictureBox27
            // 
            this.pictureBox27.Image = global::Comercializadora.Properties.Resources.LavaLg1;
            this.pictureBox27.Location = new System.Drawing.Point(12, 4857);
            this.pictureBox27.Name = "pictureBox27";
            this.pictureBox27.Size = new System.Drawing.Size(157, 233);
            this.pictureBox27.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox27.TabIndex = 295;
            this.pictureBox27.TabStop = false;
            // 
            // pictureBox28
            // 
            this.pictureBox28.Image = global::Comercializadora.Properties.Resources.Lg;
            this.pictureBox28.Location = new System.Drawing.Point(245, 4725);
            this.pictureBox28.Name = "pictureBox28";
            this.pictureBox28.Size = new System.Drawing.Size(286, 134);
            this.pictureBox28.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox28.TabIndex = 294;
            this.pictureBox28.TabStop = false;
            // 
            // pictureBox21
            // 
            this.pictureBox21.Image = global::Comercializadora.Properties.Resources.LavaSamsung2;
            this.pictureBox21.Location = new System.Drawing.Point(12, 4239);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(154, 211);
            this.pictureBox21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox21.TabIndex = 275;
            this.pictureBox21.TabStop = false;
            // 
            // pictureBox22
            // 
            this.pictureBox22.Image = global::Comercializadora.Properties.Resources.LavaSamsung3_1;
            this.pictureBox22.Location = new System.Drawing.Point(12, 4468);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(154, 211);
            this.pictureBox22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox22.TabIndex = 274;
            this.pictureBox22.TabStop = false;
            // 
            // pictureBox23
            // 
            this.pictureBox23.Image = global::Comercializadora.Properties.Resources.LavaSamsung1;
            this.pictureBox23.Location = new System.Drawing.Point(12, 4011);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(154, 211);
            this.pictureBox23.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox23.TabIndex = 273;
            this.pictureBox23.TabStop = false;
            // 
            // pictureBox24
            // 
            this.pictureBox24.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox24.Image = global::Comercializadora.Properties.Resources.samsung1;
            this.pictureBox24.Location = new System.Drawing.Point(275, 3924);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(231, 50);
            this.pictureBox24.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox24.TabIndex = 272;
            this.pictureBox24.TabStop = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.Image = global::Comercializadora.Properties.Resources.Lavawhirlpool3;
            this.pictureBox17.Location = new System.Drawing.Point(12, 3651);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(163, 225);
            this.pictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox17.TabIndex = 253;
            this.pictureBox17.TabStop = false;
            // 
            // pictureBox18
            // 
            this.pictureBox18.Image = global::Comercializadora.Properties.Resources.Lavawhirlpool2;
            this.pictureBox18.Location = new System.Drawing.Point(12, 3418);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(163, 214);
            this.pictureBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox18.TabIndex = 252;
            this.pictureBox18.TabStop = false;
            // 
            // pictureBox19
            // 
            this.pictureBox19.Image = global::Comercializadora.Properties.Resources.Lavawhirlpool1;
            this.pictureBox19.Location = new System.Drawing.Point(12, 3167);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(163, 219);
            this.pictureBox19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox19.TabIndex = 251;
            this.pictureBox19.TabStop = false;
            // 
            // pictureBox20
            // 
            this.pictureBox20.Image = global::Comercializadora.Properties.Resources.whirlpool;
            this.pictureBox20.Location = new System.Drawing.Point(269, 3039);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(237, 99);
            this.pictureBox20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox20.TabIndex = 250;
            this.pictureBox20.TabStop = false;
            // 
            // pictureBox13
            // 
            this.pictureBox13.Image = global::Comercializadora.Properties.Resources.AireCarrier2;
            this.pictureBox13.Location = new System.Drawing.Point(12, 2691);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(214, 94);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox13.TabIndex = 231;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.Image = global::Comercializadora.Properties.Resources.AireCarrier3;
            this.pictureBox14.Location = new System.Drawing.Point(12, 2885);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(214, 94);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox14.TabIndex = 230;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.Image = global::Comercializadora.Properties.Resources.AireCarrier;
            this.pictureBox15.Location = new System.Drawing.Point(12, 2485);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(214, 94);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox15.TabIndex = 229;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox16
            // 
            this.pictureBox16.Image = global::Comercializadora.Properties.Resources.Carrier;
            this.pictureBox16.Location = new System.Drawing.Point(259, 2281);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(448, 142);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox16.TabIndex = 228;
            this.pictureBox16.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::Comercializadora.Properties.Resources.AireLg2;
            this.pictureBox9.Location = new System.Drawing.Point(12, 1891);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(219, 114);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox9.TabIndex = 209;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = global::Comercializadora.Properties.Resources.AireLg3;
            this.pictureBox10.Location = new System.Drawing.Point(12, 2100);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(219, 114);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox10.TabIndex = 208;
            this.pictureBox10.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = global::Comercializadora.Properties.Resources.AireLg1;
            this.pictureBox11.Location = new System.Drawing.Point(12, 1704);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(219, 114);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox11.TabIndex = 207;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox12.Image = global::Comercializadora.Properties.Resources.Lg;
            this.pictureBox12.Location = new System.Drawing.Point(264, 1532);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(284, 132);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox12.TabIndex = 206;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::Comercializadora.Properties.Resources.AireMabe3;
            this.pictureBox5.Location = new System.Drawing.Point(12, 1302);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(225, 115);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox5.TabIndex = 187;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::Comercializadora.Properties.Resources.AireMabe2;
            this.pictureBox6.Location = new System.Drawing.Point(12, 1098);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(225, 115);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox6.TabIndex = 186;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::Comercializadora.Properties.Resources.AireMabe1;
            this.pictureBox7.Location = new System.Drawing.Point(12, 915);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(225, 115);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox7.TabIndex = 185;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox8.Image = global::Comercializadora.Properties.Resources.Mabe;
            this.pictureBox8.Location = new System.Drawing.Point(270, 765);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(216, 95);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox8.TabIndex = 184;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Comercializadora.Properties.Resources.AireSamsung3;
            this.pictureBox4.Location = new System.Drawing.Point(22, 583);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(208, 100);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox4.TabIndex = 183;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Comercializadora.Properties.Resources.AireSamsung2;
            this.pictureBox3.Location = new System.Drawing.Point(15, 378);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(218, 87);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 182;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Comercializadora.Properties.Resources.AireSamsung;
            this.pictureBox2.Location = new System.Drawing.Point(12, 184);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(220, 94);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 163;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources.samsung;
            this.pictureBox1.Location = new System.Drawing.Point(263, 23);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(325, 116);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 162;
            this.pictureBox1.TabStop = false;
            // 
            // BtRegresarMabe
            // 
            this.BtRegresarMabe.Image = global::Comercializadora.Properties.Resources.regresar3;
            this.BtRegresarMabe.Location = new System.Drawing.Point(-1, 0);
            this.BtRegresarMabe.Name = "BtRegresarMabe";
            this.BtRegresarMabe.Size = new System.Drawing.Size(107, 82);
            this.BtRegresarMabe.TabIndex = 141;
            this.BtRegresarMabe.UseVisualStyleBackColor = true;
            this.BtRegresarMabe.Click += new System.EventHandler(this.BtRegresarMabe_Click);
            // 
            // VerTodosLosProductos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(930, 745);
            this.Controls.Add(this.BtnCarrito);
            this.Controls.Add(this.VerTodosComprarMicroDaewoo3);
            this.Controls.Add(this.VerTodosAgregarMicroDaewoo3);
            this.Controls.Add(this.VerTodosAgregarMicroDaewoo2);
            this.Controls.Add(this.VerTodosComprarMicroDaewoo2);
            this.Controls.Add(this.label182);
            this.Controls.Add(this.label183);
            this.Controls.Add(this.label184);
            this.Controls.Add(this.label185);
            this.Controls.Add(this.label186);
            this.Controls.Add(this.label187);
            this.Controls.Add(this.label188);
            this.Controls.Add(this.label189);
            this.Controls.Add(this.VerTodosAgregarMicroDaewoo);
            this.Controls.Add(this.VerTodosComprarMicoDaewoo1);
            this.Controls.Add(this.label190);
            this.Controls.Add(this.label191);
            this.Controls.Add(this.label192);
            this.Controls.Add(this.label193);
            this.Controls.Add(this.pictureBox61);
            this.Controls.Add(this.pictureBox62);
            this.Controls.Add(this.pictureBox63);
            this.Controls.Add(this.pictureBox64);
            this.Controls.Add(this.VerTodosComprarMicroMabe3);
            this.Controls.Add(this.VerTodosAgregarMicroMabe3);
            this.Controls.Add(this.VerTodosAgreagrMicroMabe2);
            this.Controls.Add(this.VerTodosComprarMicroMabe2);
            this.Controls.Add(this.label170);
            this.Controls.Add(this.label171);
            this.Controls.Add(this.label172);
            this.Controls.Add(this.label173);
            this.Controls.Add(this.label174);
            this.Controls.Add(this.label175);
            this.Controls.Add(this.label176);
            this.Controls.Add(this.label177);
            this.Controls.Add(this.VerTodosAgregarMicroMabe1);
            this.Controls.Add(this.VerTodosComprarMicroMabe1);
            this.Controls.Add(this.label178);
            this.Controls.Add(this.label179);
            this.Controls.Add(this.label180);
            this.Controls.Add(this.label181);
            this.Controls.Add(this.pictureBox57);
            this.Controls.Add(this.pictureBox58);
            this.Controls.Add(this.pictureBox59);
            this.Controls.Add(this.pictureBox60);
            this.Controls.Add(this.VerTodosComprarMicroWhirlpool);
            this.Controls.Add(this.VerTodosAgregarMicroWhirlpool3);
            this.Controls.Add(this.VerTodosAgregarMicroWhirlpool2);
            this.Controls.Add(this.VerTodosComprarMicroWhirlpool2);
            this.Controls.Add(this.label158);
            this.Controls.Add(this.label159);
            this.Controls.Add(this.label160);
            this.Controls.Add(this.label161);
            this.Controls.Add(this.label162);
            this.Controls.Add(this.label163);
            this.Controls.Add(this.label164);
            this.Controls.Add(this.label165);
            this.Controls.Add(this.VerTodosAgregarMicroWhirlpool);
            this.Controls.Add(this.VerTodosComprarMicroWhirlpool1);
            this.Controls.Add(this.label166);
            this.Controls.Add(this.label167);
            this.Controls.Add(this.label168);
            this.Controls.Add(this.label169);
            this.Controls.Add(this.pictureBox53);
            this.Controls.Add(this.pictureBox54);
            this.Controls.Add(this.pictureBox55);
            this.Controls.Add(this.pictureBox56);
            this.Controls.Add(this.VerTodosComrarMicroSamsung3);
            this.Controls.Add(this.VerTodosAgregarMicroSamsung);
            this.Controls.Add(this.VerTodosAgregarMicroSamsung2);
            this.Controls.Add(this.VerTodosComprarMicroSamsung2);
            this.Controls.Add(this.label146);
            this.Controls.Add(this.label147);
            this.Controls.Add(this.label148);
            this.Controls.Add(this.label149);
            this.Controls.Add(this.label150);
            this.Controls.Add(this.label151);
            this.Controls.Add(this.label152);
            this.Controls.Add(this.label153);
            this.Controls.Add(this.VerTodosAgregarMicroSamsung1);
            this.Controls.Add(this.VerTodosComprarMicroSamsung1);
            this.Controls.Add(this.label154);
            this.Controls.Add(this.label155);
            this.Controls.Add(this.label156);
            this.Controls.Add(this.label157);
            this.Controls.Add(this.pictureBox49);
            this.Controls.Add(this.pictureBox50);
            this.Controls.Add(this.pictureBox51);
            this.Controls.Add(this.pictureBox52);
            this.Controls.Add(this.VerTodosAgregarRefriLg3);
            this.Controls.Add(this.VerTodosComprarRefriLg3);
            this.Controls.Add(this.label134);
            this.Controls.Add(this.label135);
            this.Controls.Add(this.label136);
            this.Controls.Add(this.label137);
            this.Controls.Add(this.VerTodosAgregarRefriLg2);
            this.Controls.Add(this.VerTodosComprarRefriLg2);
            this.Controls.Add(this.label138);
            this.Controls.Add(this.label139);
            this.Controls.Add(this.label140);
            this.Controls.Add(this.label141);
            this.Controls.Add(this.VerTodosAgregarRefriLg1);
            this.Controls.Add(this.VerTodosComprarRefriLg1);
            this.Controls.Add(this.label142);
            this.Controls.Add(this.label143);
            this.Controls.Add(this.label144);
            this.Controls.Add(this.label145);
            this.Controls.Add(this.pictureBox45);
            this.Controls.Add(this.pictureBox46);
            this.Controls.Add(this.pictureBox47);
            this.Controls.Add(this.pictureBox48);
            this.Controls.Add(this.VerTodosAgregarRefriMabe1);
            this.Controls.Add(this.VerTodosComprarRefriMabe1);
            this.Controls.Add(this.VerTodosAgregarRefriMabe2);
            this.Controls.Add(this.VerTodosComprarRefriMabe2);
            this.Controls.Add(this.VerTodosAgregarRefriMabe3);
            this.Controls.Add(this.VerTodosComprarRefriMabe3);
            this.Controls.Add(this.label122);
            this.Controls.Add(this.label123);
            this.Controls.Add(this.label124);
            this.Controls.Add(this.label125);
            this.Controls.Add(this.label126);
            this.Controls.Add(this.label127);
            this.Controls.Add(this.label128);
            this.Controls.Add(this.label129);
            this.Controls.Add(this.label130);
            this.Controls.Add(this.label131);
            this.Controls.Add(this.label132);
            this.Controls.Add(this.label133);
            this.Controls.Add(this.pictureBox41);
            this.Controls.Add(this.pictureBox42);
            this.Controls.Add(this.pictureBox43);
            this.Controls.Add(this.pictureBox44);
            this.Controls.Add(this.label109);
            this.Controls.Add(this.label110);
            this.Controls.Add(this.label111);
            this.Controls.Add(this.label112);
            this.Controls.Add(this.label113);
            this.Controls.Add(this.label114);
            this.Controls.Add(this.label115);
            this.Controls.Add(this.label116);
            this.Controls.Add(this.label117);
            this.Controls.Add(this.label118);
            this.Controls.Add(this.label119);
            this.Controls.Add(this.label120);
            this.Controls.Add(this.label121);
            this.Controls.Add(this.VerTodosAgregarRefriSamsung3);
            this.Controls.Add(this.VerTodosComprarRefriSamsung3);
            this.Controls.Add(this.VerTodosAgregarRefriSamsung2);
            this.Controls.Add(this.VerTodosComprarRefriSamsung2);
            this.Controls.Add(this.VerTodosAgregarRefriSmsung1);
            this.Controls.Add(this.VerTodosComprarRefriSamsung1);
            this.Controls.Add(this.pictureBox37);
            this.Controls.Add(this.pictureBox38);
            this.Controls.Add(this.pictureBox39);
            this.Controls.Add(this.pictureBox40);
            this.Controls.Add(this.VerTodosAgregarRefriWhirlpool3);
            this.Controls.Add(this.VerTodosComprarRefriWhirlpool3);
            this.Controls.Add(this.label97);
            this.Controls.Add(this.label98);
            this.Controls.Add(this.label99);
            this.Controls.Add(this.label100);
            this.Controls.Add(this.VerTodosAgregarRefriWhirlpool2);
            this.Controls.Add(this.VerTodosComprarRefriWhirlpool2);
            this.Controls.Add(this.label101);
            this.Controls.Add(this.label102);
            this.Controls.Add(this.label103);
            this.Controls.Add(this.label104);
            this.Controls.Add(this.VerTodosAgregarRefriWhirlpool1);
            this.Controls.Add(this.VerTodosComprarRefriWhirlpool1);
            this.Controls.Add(this.label105);
            this.Controls.Add(this.label106);
            this.Controls.Add(this.label107);
            this.Controls.Add(this.label108);
            this.Controls.Add(this.pictureBox33);
            this.Controls.Add(this.pictureBox34);
            this.Controls.Add(this.pictureBox35);
            this.Controls.Add(this.pictureBox36);
            this.Controls.Add(this.VerTodosComprarLavaGeneral3);
            this.Controls.Add(this.VerTodosAgregarLavaGeneral3);
            this.Controls.Add(this.VerTodosAgreagrLavaGeneral2);
            this.Controls.Add(this.VerTodosComprarLavaGeneral2);
            this.Controls.Add(this.label85);
            this.Controls.Add(this.label86);
            this.Controls.Add(this.label87);
            this.Controls.Add(this.label88);
            this.Controls.Add(this.label89);
            this.Controls.Add(this.label90);
            this.Controls.Add(this.label91);
            this.Controls.Add(this.label92);
            this.Controls.Add(this.VerTodosAgregarLavaGeneral1);
            this.Controls.Add(this.VerTodosComprarLavaGeneral1);
            this.Controls.Add(this.label93);
            this.Controls.Add(this.label94);
            this.Controls.Add(this.label95);
            this.Controls.Add(this.label96);
            this.Controls.Add(this.pictureBox29);
            this.Controls.Add(this.pictureBox30);
            this.Controls.Add(this.pictureBox31);
            this.Controls.Add(this.pictureBox32);
            this.Controls.Add(this.VerTodosAgregarLavaLg3);
            this.Controls.Add(this.VerTodosComprarLavaLg3);
            this.Controls.Add(this.label73);
            this.Controls.Add(this.label74);
            this.Controls.Add(this.label75);
            this.Controls.Add(this.label76);
            this.Controls.Add(this.VerTodosAgregarLavaLg2);
            this.Controls.Add(this.VerTodosComprarLlavaLg2);
            this.Controls.Add(this.label77);
            this.Controls.Add(this.label78);
            this.Controls.Add(this.label79);
            this.Controls.Add(this.label80);
            this.Controls.Add(this.VerTodosAgregarLavaLg1);
            this.Controls.Add(this.VerTodosComprarLavaLg1);
            this.Controls.Add(this.label81);
            this.Controls.Add(this.label82);
            this.Controls.Add(this.label83);
            this.Controls.Add(this.label84);
            this.Controls.Add(this.pictureBox25);
            this.Controls.Add(this.pictureBox26);
            this.Controls.Add(this.pictureBox27);
            this.Controls.Add(this.pictureBox28);
            this.Controls.Add(this.VerTodosComprarSamsung3);
            this.Controls.Add(this.VerTodosAgregarSamsung3);
            this.Controls.Add(this.VerTodosAgregarSamsung2);
            this.Controls.Add(this.VerTodosComprarSamsung2);
            this.Controls.Add(this.label61);
            this.Controls.Add(this.label62);
            this.Controls.Add(this.label63);
            this.Controls.Add(this.label64);
            this.Controls.Add(this.label65);
            this.Controls.Add(this.label66);
            this.Controls.Add(this.label67);
            this.Controls.Add(this.label68);
            this.Controls.Add(this.VerTodosAgregarSamsung1);
            this.Controls.Add(this.VerTodosComprarSamsung1);
            this.Controls.Add(this.label69);
            this.Controls.Add(this.label70);
            this.Controls.Add(this.label71);
            this.Controls.Add(this.label72);
            this.Controls.Add(this.pictureBox21);
            this.Controls.Add(this.pictureBox22);
            this.Controls.Add(this.pictureBox23);
            this.Controls.Add(this.pictureBox24);
            this.Controls.Add(this.VerTodosAgregarWhirlpool3);
            this.Controls.Add(this.VerTodosComprarWhirlpool3);
            this.Controls.Add(this.label49);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.label51);
            this.Controls.Add(this.label52);
            this.Controls.Add(this.VerTodosAgregarWhirlpool2);
            this.Controls.Add(this.VerTodosComprarWhirlpool2);
            this.Controls.Add(this.label53);
            this.Controls.Add(this.label54);
            this.Controls.Add(this.label55);
            this.Controls.Add(this.label56);
            this.Controls.Add(this.VerTodosAgregarWhirlpool1);
            this.Controls.Add(this.VerTodosComprarWhirlpool1);
            this.Controls.Add(this.label57);
            this.Controls.Add(this.label58);
            this.Controls.Add(this.label59);
            this.Controls.Add(this.label60);
            this.Controls.Add(this.pictureBox17);
            this.Controls.Add(this.pictureBox18);
            this.Controls.Add(this.pictureBox19);
            this.Controls.Add(this.pictureBox20);
            this.Controls.Add(this.VerTodosComprarCarrier3);
            this.Controls.Add(this.VerTodosAgregarCarrier3);
            this.Controls.Add(this.VerTodosAgregarCarrier2);
            this.Controls.Add(this.VerTodosComprarCarrier2);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.label38);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.label40);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.label42);
            this.Controls.Add(this.label43);
            this.Controls.Add(this.label44);
            this.Controls.Add(this.VerTodosAgregarCarrier1);
            this.Controls.Add(this.VerTodosComprarCarrier1);
            this.Controls.Add(this.label45);
            this.Controls.Add(this.label46);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.label48);
            this.Controls.Add(this.pictureBox13);
            this.Controls.Add(this.pictureBox14);
            this.Controls.Add(this.pictureBox15);
            this.Controls.Add(this.pictureBox16);
            this.Controls.Add(this.VerTodosComprarLg3);
            this.Controls.Add(this.VerTodosaAgregarLg);
            this.Controls.Add(this.VerTodosAgregarLg2);
            this.Controls.Add(this.VerTodosComprarLg2);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.VerTodosAgregarLg1);
            this.Controls.Add(this.VerTodosComprarLg1);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label34);
            this.Controls.Add(this.label35);
            this.Controls.Add(this.label36);
            this.Controls.Add(this.pictureBox9);
            this.Controls.Add(this.pictureBox10);
            this.Controls.Add(this.pictureBox11);
            this.Controls.Add(this.pictureBox12);
            this.Controls.Add(this.VerTodosAgregarMabe3);
            this.Controls.Add(this.VerTodosComprarMabe3);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.VerTodosAgregarMabe2);
            this.Controls.Add(this.VerTodosComprarMabe2);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.VerTodoAgregarMabe1);
            this.Controls.Add(this.VerTodosComprarMabe1);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox8);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.VerTodosComprarSamsungAire3);
            this.Controls.Add(this.VerTodosAgregarSamsungAire3);
            this.Controls.Add(this.VerTodosAgregarSamsungAire2);
            this.Controls.Add(this.VerTodosComprarSamsungAire2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.MiniSplitSamsung);
            this.Controls.Add(this.VerTodosAgregarSamsungAire);
            this.Controls.Add(this.VerTodosComprarSamsungAire);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.SamsungAR12MVFHEWK);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.BtRegresarMabe);
            this.Name = "VerTodosLosProductos";
            this.Text = "VerTodosLosProductos";
            this.Load += new System.EventHandler(this.VerTodosLosProductos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox63)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button BtRegresarMabe;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button VerTodosComprarSamsungAire3;
        private System.Windows.Forms.Button VerTodosAgregarSamsungAire3;
        private System.Windows.Forms.Button VerTodosAgregarSamsungAire2;
        private System.Windows.Forms.Button VerTodosComprarSamsungAire2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label MiniSplitSamsung;
        private System.Windows.Forms.Button VerTodosAgregarSamsungAire;
        private System.Windows.Forms.Button VerTodosComprarSamsungAire;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label SamsungAR12MVFHEWK;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button VerTodosAgregarMabe3;
        private System.Windows.Forms.Button VerTodosComprarMabe3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button VerTodosAgregarMabe2;
        private System.Windows.Forms.Button VerTodosComprarMabe2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button VerTodoAgregarMabe1;
        private System.Windows.Forms.Button VerTodosComprarMabe1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Button VerTodosComprarLg3;
        private System.Windows.Forms.Button VerTodosaAgregarLg;
        private System.Windows.Forms.Button VerTodosAgregarLg2;
        private System.Windows.Forms.Button VerTodosComprarLg2;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Button VerTodosAgregarLg1;
        private System.Windows.Forms.Button VerTodosComprarLg1;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.Button VerTodosComprarCarrier3;
        private System.Windows.Forms.Button VerTodosAgregarCarrier3;
        private System.Windows.Forms.Button VerTodosAgregarCarrier2;
        private System.Windows.Forms.Button VerTodosComprarCarrier2;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Button VerTodosAgregarCarrier1;
        private System.Windows.Forms.Button VerTodosComprarCarrier1;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.Button VerTodosAgregarWhirlpool3;
        private System.Windows.Forms.Button VerTodosComprarWhirlpool3;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Button VerTodosAgregarWhirlpool2;
        private System.Windows.Forms.Button VerTodosComprarWhirlpool2;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Button VerTodosAgregarWhirlpool1;
        private System.Windows.Forms.Button VerTodosComprarWhirlpool1;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.Button VerTodosComprarSamsung3;
        private System.Windows.Forms.Button VerTodosAgregarSamsung3;
        private System.Windows.Forms.Button VerTodosAgregarSamsung2;
        private System.Windows.Forms.Button VerTodosComprarSamsung2;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Button VerTodosAgregarSamsung1;
        private System.Windows.Forms.Button VerTodosComprarSamsung1;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.PictureBox pictureBox24;
        private System.Windows.Forms.Button VerTodosAgregarLavaLg3;
        private System.Windows.Forms.Button VerTodosComprarLavaLg3;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Button VerTodosAgregarLavaLg2;
        private System.Windows.Forms.Button VerTodosComprarLlavaLg2;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Button VerTodosAgregarLavaLg1;
        private System.Windows.Forms.Button VerTodosComprarLavaLg1;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.PictureBox pictureBox25;
        private System.Windows.Forms.PictureBox pictureBox26;
        private System.Windows.Forms.PictureBox pictureBox27;
        private System.Windows.Forms.PictureBox pictureBox28;
        private System.Windows.Forms.Button VerTodosComprarLavaGeneral3;
        private System.Windows.Forms.Button VerTodosAgregarLavaGeneral3;
        private System.Windows.Forms.Button VerTodosAgreagrLavaGeneral2;
        private System.Windows.Forms.Button VerTodosComprarLavaGeneral2;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Button VerTodosAgregarLavaGeneral1;
        private System.Windows.Forms.Button VerTodosComprarLavaGeneral1;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.PictureBox pictureBox29;
        private System.Windows.Forms.PictureBox pictureBox30;
        private System.Windows.Forms.PictureBox pictureBox31;
        private System.Windows.Forms.PictureBox pictureBox32;
        private System.Windows.Forms.Button VerTodosAgregarRefriWhirlpool3;
        private System.Windows.Forms.Button VerTodosComprarRefriWhirlpool3;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Button VerTodosAgregarRefriWhirlpool2;
        private System.Windows.Forms.Button VerTodosComprarRefriWhirlpool2;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Button VerTodosAgregarRefriWhirlpool1;
        private System.Windows.Forms.Button VerTodosComprarRefriWhirlpool1;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.PictureBox pictureBox33;
        private System.Windows.Forms.PictureBox pictureBox34;
        private System.Windows.Forms.PictureBox pictureBox35;
        private System.Windows.Forms.PictureBox pictureBox36;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Button VerTodosAgregarRefriSamsung3;
        private System.Windows.Forms.Button VerTodosComprarRefriSamsung3;
        private System.Windows.Forms.Button VerTodosAgregarRefriSamsung2;
        private System.Windows.Forms.Button VerTodosComprarRefriSamsung2;
        private System.Windows.Forms.Button VerTodosAgregarRefriSmsung1;
        private System.Windows.Forms.Button VerTodosComprarRefriSamsung1;
        private System.Windows.Forms.PictureBox pictureBox37;
        private System.Windows.Forms.PictureBox pictureBox38;
        private System.Windows.Forms.PictureBox pictureBox39;
        private System.Windows.Forms.PictureBox pictureBox40;
        private System.Windows.Forms.Button VerTodosAgregarRefriMabe1;
        private System.Windows.Forms.Button VerTodosComprarRefriMabe1;
        private System.Windows.Forms.Button VerTodosAgregarRefriMabe2;
        private System.Windows.Forms.Button VerTodosComprarRefriMabe2;
        private System.Windows.Forms.Button VerTodosAgregarRefriMabe3;
        private System.Windows.Forms.Button VerTodosComprarRefriMabe3;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.PictureBox pictureBox41;
        private System.Windows.Forms.PictureBox pictureBox42;
        private System.Windows.Forms.PictureBox pictureBox43;
        private System.Windows.Forms.PictureBox pictureBox44;
        private System.Windows.Forms.Button VerTodosAgregarRefriLg3;
        private System.Windows.Forms.Button VerTodosComprarRefriLg3;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Button VerTodosAgregarRefriLg2;
        private System.Windows.Forms.Button VerTodosComprarRefriLg2;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.Button VerTodosAgregarRefriLg1;
        private System.Windows.Forms.Button VerTodosComprarRefriLg1;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.PictureBox pictureBox45;
        private System.Windows.Forms.PictureBox pictureBox46;
        private System.Windows.Forms.PictureBox pictureBox47;
        private System.Windows.Forms.PictureBox pictureBox48;
        private System.Windows.Forms.Button VerTodosComrarMicroSamsung3;
        private System.Windows.Forms.Button VerTodosAgregarMicroSamsung;
        private System.Windows.Forms.Button VerTodosAgregarMicroSamsung2;
        private System.Windows.Forms.Button VerTodosComprarMicroSamsung2;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.Label label149;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.Button VerTodosAgregarMicroSamsung1;
        private System.Windows.Forms.Button VerTodosComprarMicroSamsung1;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.PictureBox pictureBox49;
        private System.Windows.Forms.PictureBox pictureBox50;
        private System.Windows.Forms.PictureBox pictureBox51;
        private System.Windows.Forms.PictureBox pictureBox52;
        private System.Windows.Forms.Button VerTodosComprarMicroWhirlpool;
        private System.Windows.Forms.Button VerTodosAgregarMicroWhirlpool3;
        private System.Windows.Forms.Button VerTodosAgregarMicroWhirlpool2;
        private System.Windows.Forms.Button VerTodosComprarMicroWhirlpool2;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.Label label159;
        private System.Windows.Forms.Label label160;
        private System.Windows.Forms.Label label161;
        private System.Windows.Forms.Label label162;
        private System.Windows.Forms.Label label163;
        private System.Windows.Forms.Label label164;
        private System.Windows.Forms.Label label165;
        private System.Windows.Forms.Button VerTodosAgregarMicroWhirlpool;
        private System.Windows.Forms.Button VerTodosComprarMicroWhirlpool1;
        private System.Windows.Forms.Label label166;
        private System.Windows.Forms.Label label167;
        private System.Windows.Forms.Label label168;
        private System.Windows.Forms.Label label169;
        private System.Windows.Forms.PictureBox pictureBox53;
        private System.Windows.Forms.PictureBox pictureBox54;
        private System.Windows.Forms.PictureBox pictureBox55;
        private System.Windows.Forms.PictureBox pictureBox56;
        private System.Windows.Forms.Button VerTodosComprarMicroMabe3;
        private System.Windows.Forms.Button VerTodosAgregarMicroMabe3;
        private System.Windows.Forms.Button VerTodosAgreagrMicroMabe2;
        private System.Windows.Forms.Button VerTodosComprarMicroMabe2;
        private System.Windows.Forms.Label label170;
        private System.Windows.Forms.Label label171;
        private System.Windows.Forms.Label label172;
        private System.Windows.Forms.Label label173;
        private System.Windows.Forms.Label label174;
        private System.Windows.Forms.Label label175;
        private System.Windows.Forms.Label label176;
        private System.Windows.Forms.Label label177;
        private System.Windows.Forms.Button VerTodosAgregarMicroMabe1;
        private System.Windows.Forms.Button VerTodosComprarMicroMabe1;
        private System.Windows.Forms.Label label178;
        private System.Windows.Forms.Label label179;
        private System.Windows.Forms.Label label180;
        private System.Windows.Forms.Label label181;
        private System.Windows.Forms.PictureBox pictureBox57;
        private System.Windows.Forms.PictureBox pictureBox58;
        private System.Windows.Forms.PictureBox pictureBox59;
        private System.Windows.Forms.PictureBox pictureBox60;
        private System.Windows.Forms.Button VerTodosComprarMicroDaewoo3;
        private System.Windows.Forms.Button VerTodosAgregarMicroDaewoo3;
        private System.Windows.Forms.Button VerTodosAgregarMicroDaewoo2;
        private System.Windows.Forms.Button VerTodosComprarMicroDaewoo2;
        private System.Windows.Forms.Label label182;
        private System.Windows.Forms.Label label183;
        private System.Windows.Forms.Label label184;
        private System.Windows.Forms.Label label185;
        private System.Windows.Forms.Label label186;
        private System.Windows.Forms.Label label187;
        private System.Windows.Forms.Label label188;
        private System.Windows.Forms.Label label189;
        private System.Windows.Forms.Button VerTodosAgregarMicroDaewoo;
        private System.Windows.Forms.Button VerTodosComprarMicoDaewoo1;
        private System.Windows.Forms.Label label190;
        private System.Windows.Forms.Label label191;
        private System.Windows.Forms.Label label192;
        private System.Windows.Forms.Label label193;
        private System.Windows.Forms.PictureBox pictureBox61;
        private System.Windows.Forms.PictureBox pictureBox62;
        private System.Windows.Forms.PictureBox pictureBox63;
        private System.Windows.Forms.PictureBox pictureBox64;
        private System.Windows.Forms.Button BtnCarrito;
    }
}