﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comercializadora
{
    public partial class PianosPag3 : Form
    {
        public PianosPag3()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnPag2Piano_Click(object sender, EventArgs e)
        {
            this.Close();
            PianosPag2 pp2 = new PianosPag2();
            pp2.Show();
        }

        private void btnPag1Piano_Click(object sender, EventArgs e)
        {
            this.Close();
            PianosPag1 pp1 = new PianosPag1();
            pp1.Show();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
