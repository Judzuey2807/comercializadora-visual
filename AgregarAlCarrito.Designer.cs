﻿namespace Comercializadora
{
    partial class AgregarAlCarrito
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ListAgregar = new System.Windows.Forms.ListBox();
            this.CarritoComprar = new System.Windows.Forms.Button();
            this.CarritoBorrar = new System.Windows.Forms.Button();
            this.BtRegresarMabe = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ListAgregar
            // 
            this.ListAgregar.FormattingEnabled = true;
            this.ListAgregar.Location = new System.Drawing.Point(106, 1);
            this.ListAgregar.Name = "ListAgregar";
            this.ListAgregar.Size = new System.Drawing.Size(479, 303);
            this.ListAgregar.TabIndex = 0;
            this.ListAgregar.SelectedIndexChanged += new System.EventHandler(this.ListAgregar_SelectedIndexChanged);
            // 
            // CarritoComprar
            // 
            this.CarritoComprar.Location = new System.Drawing.Point(6, 199);
            this.CarritoComprar.Name = "CarritoComprar";
            this.CarritoComprar.Size = new System.Drawing.Size(94, 23);
            this.CarritoComprar.TabIndex = 1;
            this.CarritoComprar.Text = "Comprar";
            this.CarritoComprar.UseVisualStyleBackColor = true;
            // 
            // CarritoBorrar
            // 
            this.CarritoBorrar.Location = new System.Drawing.Point(6, 136);
            this.CarritoBorrar.Name = "CarritoBorrar";
            this.CarritoBorrar.Size = new System.Drawing.Size(94, 23);
            this.CarritoBorrar.TabIndex = 2;
            this.CarritoBorrar.Text = "Borrar";
            this.CarritoBorrar.UseVisualStyleBackColor = true;
            this.CarritoBorrar.Click += new System.EventHandler(this.CarritoBorrar_Click);
            // 
            // BtRegresarMabe
            // 
            this.BtRegresarMabe.Image = global::Comercializadora.Properties.Resources.regresar3;
            this.BtRegresarMabe.Location = new System.Drawing.Point(1, 1);
            this.BtRegresarMabe.Name = "BtRegresarMabe";
            this.BtRegresarMabe.Size = new System.Drawing.Size(107, 82);
            this.BtRegresarMabe.TabIndex = 142;
            this.BtRegresarMabe.UseVisualStyleBackColor = true;
            this.BtRegresarMabe.Click += new System.EventHandler(this.BtRegresarMabe_Click);
            // 
            // AgregarAlCarrito
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(586, 305);
            this.Controls.Add(this.BtRegresarMabe);
            this.Controls.Add(this.CarritoBorrar);
            this.Controls.Add(this.CarritoComprar);
            this.Controls.Add(this.ListAgregar);
            this.Name = "AgregarAlCarrito";
            this.Text = "AgregarAlCarrito";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button CarritoComprar;
        private System.Windows.Forms.Button CarritoBorrar;
        public System.Windows.Forms.ListBox ListAgregar;
        private System.Windows.Forms.Button BtRegresarMabe;
    }
}