﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comercializadora
{
    public partial class AudioPag2 : Form
    {
        public AudioPag2()
        {
            InitializeComponent();
        }

        private void btnAPag1_Click(object sender, EventArgs e)
        {
            this.Close();
            AudioPag1 ap1 = new AudioPag1();
            ap1.ShowDialog();

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblAPag2_Click(object sender, EventArgs e)
        {
            this.Close();
            AudioPag2 ap2 = new AudioPag2();
            ap2.ShowDialog();
        }
    }
}
