﻿namespace Comercializadora
{
    partial class AireLg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AireLg));
            this.ComprarAireLg3 = new System.Windows.Forms.Button();
            this.AgregarAireLg3 = new System.Windows.Forms.Button();
            this.AgregarAireLg2 = new System.Windows.Forms.Button();
            this.ComprarAireLg2 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.AgregarAireLg1 = new System.Windows.Forms.Button();
            this.ComprarAireLg1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BtRegresarMabe = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // ComprarAireLg3
            // 
            this.ComprarAireLg3.Location = new System.Drawing.Point(274, 717);
            this.ComprarAireLg3.Name = "ComprarAireLg3";
            this.ComprarAireLg3.Size = new System.Drawing.Size(105, 23);
            this.ComprarAireLg3.TabIndex = 113;
            this.ComprarAireLg3.Text = "Comprar";
            this.ComprarAireLg3.UseVisualStyleBackColor = true;
            // 
            // AgregarAireLg3
            // 
            this.AgregarAireLg3.Location = new System.Drawing.Point(403, 717);
            this.AgregarAireLg3.Name = "AgregarAireLg3";
            this.AgregarAireLg3.Size = new System.Drawing.Size(105, 23);
            this.AgregarAireLg3.TabIndex = 112;
            this.AgregarAireLg3.Text = "Agregar al carrito";
            this.AgregarAireLg3.UseVisualStyleBackColor = true;
            // 
            // AgregarAireLg2
            // 
            this.AgregarAireLg2.Location = new System.Drawing.Point(403, 518);
            this.AgregarAireLg2.Name = "AgregarAireLg2";
            this.AgregarAireLg2.Size = new System.Drawing.Size(105, 23);
            this.AgregarAireLg2.TabIndex = 111;
            this.AgregarAireLg2.Text = "Agregar al carrito";
            this.AgregarAireLg2.UseVisualStyleBackColor = true;
            // 
            // ComprarAireLg2
            // 
            this.ComprarAireLg2.Location = new System.Drawing.Point(274, 518);
            this.ComprarAireLg2.Name = "ComprarAireLg2";
            this.ComprarAireLg2.Size = new System.Drawing.Size(105, 23);
            this.ComprarAireLg2.TabIndex = 110;
            this.ComprarAireLg2.Text = "Comprar";
            this.ComprarAireLg2.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(272, 631);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(321, 65);
            this.label12.TabIndex = 109;
            this.label12.Text = resources.GetString("label12.Text");
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(272, 609);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(62, 15);
            this.label11.TabIndex = 108;
            this.label11.Text = "14,213.74";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(272, 579);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 15);
            this.label10.TabIndex = 107;
            this.label10.Text = "Marca:  LG";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(271, 557);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(527, 22);
            this.label9.TabIndex = 106;
            this.label9.Text = "LG AIRE ACONDICIONADO MINISPLIT 12000 BTU\'S GRIS";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(272, 421);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(253, 78);
            this.label8.TabIndex = 105;
            this.label8.Text = resources.GetString("label8.Text");
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(272, 402);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 15);
            this.label7.TabIndex = 104;
            this.label7.Text = "$ 10,290.00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(272, 373);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 15);
            this.label6.TabIndex = 103;
            this.label6.Text = "Marca:  LG";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label5.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(271, 351);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(433, 22);
            this.label5.TabIndex = 102;
            this.label5.Text = "Inversor Mini Split LG Frío y Calor 12,000 BTU";
            // 
            // AgregarAireLg1
            // 
            this.AgregarAireLg1.Location = new System.Drawing.Point(403, 305);
            this.AgregarAireLg1.Name = "AgregarAireLg1";
            this.AgregarAireLg1.Size = new System.Drawing.Size(105, 23);
            this.AgregarAireLg1.TabIndex = 101;
            this.AgregarAireLg1.Text = "Agregar al carrito";
            this.AgregarAireLg1.UseVisualStyleBackColor = true;
            // 
            // ComprarAireLg1
            // 
            this.ComprarAireLg1.Location = new System.Drawing.Point(274, 305);
            this.ComprarAireLg1.Name = "ComprarAireLg1";
            this.ComprarAireLg1.Size = new System.Drawing.Size(105, 23);
            this.ComprarAireLg1.TabIndex = 100;
            this.ComprarAireLg1.Text = "Comprar";
            this.ComprarAireLg1.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(272, 251);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 39);
            this.label4.TabIndex = 99;
            this.label4.Text = "1 tonelada\r\n18 SEER\r\nR410A";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(271, 225);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 15);
            this.label3.TabIndex = 98;
            this.label3.Text = "$ 12,999.00";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(272, 198);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 15);
            this.label2.TabIndex = 97;
            this.label2.Text = "Marca: LG";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(270, 176);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(463, 22);
            this.label1.TabIndex = 96;
            this.label1.Text = "Aire Acondicionado LG VM121C8 Inverter S / Frio";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Comercializadora.Properties.Resources.AireLg2;
            this.pictureBox4.Location = new System.Drawing.Point(36, 373);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(219, 114);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Comercializadora.Properties.Resources.AireLg3;
            this.pictureBox3.Location = new System.Drawing.Point(36, 582);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(219, 114);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Comercializadora.Properties.Resources.AireLg1;
            this.pictureBox2.Location = new System.Drawing.Point(36, 186);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(219, 114);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources.Lg;
            this.pictureBox1.Location = new System.Drawing.Point(261, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(284, 132);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // BtRegresarMabe
            // 
            this.BtRegresarMabe.Image = global::Comercializadora.Properties.Resources.regresar3;
            this.BtRegresarMabe.Location = new System.Drawing.Point(0, 1);
            this.BtRegresarMabe.Name = "BtRegresarMabe";
            this.BtRegresarMabe.Size = new System.Drawing.Size(107, 82);
            this.BtRegresarMabe.TabIndex = 139;
            this.BtRegresarMabe.UseVisualStyleBackColor = true;
            this.BtRegresarMabe.Click += new System.EventHandler(this.BtRegresarMabe_Click);
            // 
            // AireLg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(819, 774);
            this.Controls.Add(this.BtRegresarMabe);
            this.Controls.Add(this.ComprarAireLg3);
            this.Controls.Add(this.AgregarAireLg3);
            this.Controls.Add(this.AgregarAireLg2);
            this.Controls.Add(this.ComprarAireLg2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.AgregarAireLg1);
            this.Controls.Add(this.ComprarAireLg1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "AireLg";
            this.Text = "AireLg";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Button ComprarAireLg3;
        private System.Windows.Forms.Button AgregarAireLg3;
        private System.Windows.Forms.Button AgregarAireLg2;
        private System.Windows.Forms.Button ComprarAireLg2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button AgregarAireLg1;
        private System.Windows.Forms.Button ComprarAireLg1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtRegresarMabe;
    }
}