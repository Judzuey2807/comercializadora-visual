﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comercializadora
{
    public partial class Musica : Form
    {
        public Musica()
        {
            InitializeComponent();
        }

        private void ImagenBoxGuitarra_Click(object sender, EventArgs e)
        {
            GuitarrasYBajosPag1 gb = new GuitarrasYBajosPag1();
            gb.Show();
        }

        private void pictureBoxTeclado_Click(object sender, EventArgs e)
        {
            PianosPag1 pn = new PianosPag1();
            pn.Show();
        }

        private void pictureBoxBateria_Click(object sender, EventArgs e)
        {
            PercusionesPag1 per = new PercusionesPag1();
            per.Show();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            AudioPag1 ad = new AudioPag1();
            ad.Show();
        }

        private void btnRegresar_Click(object sender, EventArgs e)
        {
            Categorias c = new Categorias();
            c.ShowDialog();
            this.Close();
        }
    }
}
