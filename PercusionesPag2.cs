﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comercializadora
{
    public partial class PercusionesPag2 : Form
    {
        public PercusionesPag2()
        {
            InitializeComponent();
        }

        private void btnBPag1_Click(object sender, EventArgs e)
        {
            this.Close();
            PercusionesPag1 pr1 = new PercusionesPag1();
            pr1.Show();
        }

        private void btnBPag3_Click(object sender, EventArgs e)
        {
            this.Close();
            PercusionesPag3 pr3 = new PercusionesPag3();
            pr3.Show();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
