﻿namespace Comercializadora
{
    partial class MicroSamsung
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ComprarMicroSamsung3 = new System.Windows.Forms.Button();
            this.AgregarMicroSamsung3 = new System.Windows.Forms.Button();
            this.AgregarMicroSamsung2 = new System.Windows.Forms.Button();
            this.ComprarMicroSamsung2 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.AgregarMicroSamsung1 = new System.Windows.Forms.Button();
            this.ComprarMicroSamsung1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.BtRegresarMabe = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // ComprarMicroSamsung3
            // 
            this.ComprarMicroSamsung3.Location = new System.Drawing.Point(275, 729);
            this.ComprarMicroSamsung3.Name = "ComprarMicroSamsung3";
            this.ComprarMicroSamsung3.Size = new System.Drawing.Size(105, 23);
            this.ComprarMicroSamsung3.TabIndex = 59;
            this.ComprarMicroSamsung3.Text = "Comprar";
            this.ComprarMicroSamsung3.UseVisualStyleBackColor = true;
            // 
            // AgregarMicroSamsung3
            // 
            this.AgregarMicroSamsung3.Location = new System.Drawing.Point(413, 729);
            this.AgregarMicroSamsung3.Name = "AgregarMicroSamsung3";
            this.AgregarMicroSamsung3.Size = new System.Drawing.Size(105, 23);
            this.AgregarMicroSamsung3.TabIndex = 58;
            this.AgregarMicroSamsung3.Text = "Agregar al carrito";
            this.AgregarMicroSamsung3.UseVisualStyleBackColor = true;
            // 
            // AgregarMicroSamsung2
            // 
            this.AgregarMicroSamsung2.Location = new System.Drawing.Point(413, 507);
            this.AgregarMicroSamsung2.Name = "AgregarMicroSamsung2";
            this.AgregarMicroSamsung2.Size = new System.Drawing.Size(105, 23);
            this.AgregarMicroSamsung2.TabIndex = 57;
            this.AgregarMicroSamsung2.Text = "Agregar al carrito";
            this.AgregarMicroSamsung2.UseVisualStyleBackColor = true;
            // 
            // ComprarMicroSamsung2
            // 
            this.ComprarMicroSamsung2.Location = new System.Drawing.Point(284, 507);
            this.ComprarMicroSamsung2.Name = "ComprarMicroSamsung2";
            this.ComprarMicroSamsung2.Size = new System.Drawing.Size(105, 23);
            this.ComprarMicroSamsung2.TabIndex = 56;
            this.ComprarMicroSamsung2.Text = "Comprar";
            this.ComprarMicroSamsung2.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(271, 672);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(246, 39);
            this.label12.TabIndex = 55;
            this.label12.Text = "5 niveles de potencia\r\n3 Etapas de cocción en un solo toque\r\n10 Botones programab" +
    "les y 20 memorias diferentes";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(279, 647);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 15);
            this.label11.TabIndex = 54;
            this.label11.Text = "$ 5,390.00";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(272, 621);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(107, 15);
            this.label10.TabIndex = 53;
            this.label10.Text = "Marca: SAMSUNG";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(270, 568);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(547, 44);
            this.label9.TabIndex = 52;
            this.label9.Text = "Horno de Microondas Daewoo Industrial 1.0 Pies Cúbicos \r\nAcero Inox";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(282, 423);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(143, 65);
            this.label8.TabIndex = 51;
            this.label8.Text = "- Fácil Limpieza\r\n- Tamaño: 33.7 x 65.3 x 50.4\r\n- Con plato giratorio\r\n- Botón Pa" +
    "lomitas\r\n- 6 opciones de cocción";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(282, 399);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 15);
            this.label7.TabIndex = 50;
            this.label7.Text = "$ 2190.00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(282, 372);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(107, 15);
            this.label6.TabIndex = 49;
            this.label6.Text = "Marca: SAMSUNG";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label5.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(287, 328);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(324, 44);
            this.label5.TabIndex = 48;
            this.label5.Text = "Horno de Microondas\r\n1.6 Pies Cúbicos Acero Inoxidable";
            // 
            // AgregarMicroSamsung1
            // 
            this.AgregarMicroSamsung1.Location = new System.Drawing.Point(413, 263);
            this.AgregarMicroSamsung1.Name = "AgregarMicroSamsung1";
            this.AgregarMicroSamsung1.Size = new System.Drawing.Size(105, 23);
            this.AgregarMicroSamsung1.TabIndex = 47;
            this.AgregarMicroSamsung1.Text = "Agregar al carrito";
            this.AgregarMicroSamsung1.UseVisualStyleBackColor = true;
            // 
            // ComprarMicroSamsung1
            // 
            this.ComprarMicroSamsung1.Location = new System.Drawing.Point(274, 263);
            this.ComprarMicroSamsung1.Name = "ComprarMicroSamsung1";
            this.ComprarMicroSamsung1.Size = new System.Drawing.Size(105, 23);
            this.ComprarMicroSamsung1.TabIndex = 46;
            this.ComprarMicroSamsung1.Text = "Comprar";
            this.ComprarMicroSamsung1.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(282, 205);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(137, 39);
            this.label4.TabIndex = 45;
            this.label4.Text = "Fácil Limpieza\r\nTamaño: 30.1 x 55.8 x 43.6\r\nCon plato giratorio";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(282, 180);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 15);
            this.label3.TabIndex = 44;
            this.label3.Text = "$ 2,590.00";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(282, 156);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 15);
            this.label2.TabIndex = 43;
            this.label2.Text = "Marca: SAMSUNG";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(281, 134);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(514, 22);
            this.label1.TabIndex = 42;
            this.label1.Text = "Horno de Microondas Samsung 1.4 Pies Cúbicos Plata\r\n";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Comercializadora.Properties.Resources.MicroSamsung2;
            this.pictureBox4.Location = new System.Drawing.Point(9, 350);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(227, 129);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox4.TabIndex = 4;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Comercializadora.Properties.Resources.MicroSamsung3;
            this.pictureBox3.Location = new System.Drawing.Point(9, 587);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(227, 135);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Comercializadora.Properties.Resources.MicroSamsung1;
            this.pictureBox2.Location = new System.Drawing.Point(6, 134);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(230, 129);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources.samsung1;
            this.pictureBox1.Location = new System.Drawing.Point(291, 56);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(231, 50);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // BtRegresarMabe
            // 
            this.BtRegresarMabe.Image = global::Comercializadora.Properties.Resources.regresar3;
            this.BtRegresarMabe.Location = new System.Drawing.Point(0, 2);
            this.BtRegresarMabe.Name = "BtRegresarMabe";
            this.BtRegresarMabe.Size = new System.Drawing.Size(107, 82);
            this.BtRegresarMabe.TabIndex = 141;
            this.BtRegresarMabe.UseVisualStyleBackColor = true;
            this.BtRegresarMabe.Click += new System.EventHandler(this.BtRegresarMabe_Click);
            // 
            // MicroSamsung
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(807, 757);
            this.Controls.Add(this.BtRegresarMabe);
            this.Controls.Add(this.ComprarMicroSamsung3);
            this.Controls.Add(this.AgregarMicroSamsung3);
            this.Controls.Add(this.AgregarMicroSamsung2);
            this.Controls.Add(this.ComprarMicroSamsung2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.AgregarMicroSamsung1);
            this.Controls.Add(this.ComprarMicroSamsung1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "MicroSamsung";
            this.Text = "MicroSamsung";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Button ComprarMicroSamsung3;
        private System.Windows.Forms.Button AgregarMicroSamsung3;
        private System.Windows.Forms.Button AgregarMicroSamsung2;
        private System.Windows.Forms.Button ComprarMicroSamsung2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button AgregarMicroSamsung1;
        private System.Windows.Forms.Button ComprarMicroSamsung1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtRegresarMabe;
    }
}