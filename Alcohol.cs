﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comercializadora
{
    public partial class Alcohol : Form
    {
        public Alcohol()
        {
            InitializeComponent();
        }

        private void pictureBoxBateria_Click(object sender, EventArgs e)
        {
            Licores l = new Licores();
            l.ShowDialog();
            this.Close();
        }

        private void pictureBoxTeclado_Click(object sender, EventArgs e)
        {
            this.Close();
            Vinos v = new Vinos();
            v.ShowDialog();
        }

        private void btnRegresar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
