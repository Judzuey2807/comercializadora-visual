﻿namespace Comercializadora
{
    partial class AudioPag2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDescripcionAth = new System.Windows.Forms.Label();
            this.lblPrecioAth = new System.Windows.Forms.Label();
            this.comboBoxAth = new System.Windows.Forms.ComboBox();
            this.btnAth = new System.Windows.Forms.Button();
            this.lblDescripcionMezc = new System.Windows.Forms.Label();
            this.lblPrecioMezc = new System.Windows.Forms.Label();
            this.comboBoxMezc = new System.Windows.Forms.ComboBox();
            this.btnMezc = new System.Windows.Forms.Button();
            this.lblDescripcionMesa = new System.Windows.Forms.Label();
            this.lblPrecioMesa = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.btnAPag1 = new System.Windows.Forms.Button();
            this.lblAPag2 = new System.Windows.Forms.Label();
            this.pictureBoxMesa = new System.Windows.Forms.PictureBox();
            this.pictureBoxMezc = new System.Windows.Forms.PictureBox();
            this.pictureBoxAth = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMesa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMezc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDescripcionAth
            // 
            this.lblDescripcionAth.AutoSize = true;
            this.lblDescripcionAth.Location = new System.Drawing.Point(168, 79);
            this.lblDescripcionAth.Name = "lblDescripcionAth";
            this.lblDescripcionAth.Size = new System.Drawing.Size(317, 13);
            this.lblDescripcionAth.TabIndex = 1;
            this.lblDescripcionAth.Text = "Audífonos dinámicos para estudio AUDIO-TECHNICA ATH-M20X";
            // 
            // lblPrecioAth
            // 
            this.lblPrecioAth.AutoSize = true;
            this.lblPrecioAth.Location = new System.Drawing.Point(168, 104);
            this.lblPrecioAth.Name = "lblPrecioAth";
            this.lblPrecioAth.Size = new System.Drawing.Size(40, 13);
            this.lblPrecioAth.TabIndex = 2;
            this.lblPrecioAth.Text = "$1,010";
            // 
            // comboBoxAth
            // 
            this.comboBoxAth.FormattingEnabled = true;
            this.comboBoxAth.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxAth.Location = new System.Drawing.Point(611, 94);
            this.comboBoxAth.Name = "comboBoxAth";
            this.comboBoxAth.Size = new System.Drawing.Size(44, 21);
            this.comboBoxAth.TabIndex = 3;
            // 
            // btnAth
            // 
            this.btnAth.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAth.Location = new System.Drawing.Point(673, 92);
            this.btnAth.Name = "btnAth";
            this.btnAth.Size = new System.Drawing.Size(75, 23);
            this.btnAth.TabIndex = 4;
            this.btnAth.Text = "Agregar";
            this.btnAth.UseVisualStyleBackColor = true;
            // 
            // lblDescripcionMezc
            // 
            this.lblDescripcionMezc.AutoSize = true;
            this.lblDescripcionMezc.Location = new System.Drawing.Point(168, 188);
            this.lblDescripcionMezc.Name = "lblDescripcionMezc";
            this.lblDescripcionMezc.Size = new System.Drawing.Size(267, 13);
            this.lblDescripcionMezc.TabIndex = 6;
            this.lblDescripcionMezc.Text = "Sistema DJ todo en uno Rekorbox PIONEER XDJ-RX2";
            // 
            // lblPrecioMezc
            // 
            this.lblPrecioMezc.AutoSize = true;
            this.lblPrecioMezc.Location = new System.Drawing.Point(168, 214);
            this.lblPrecioMezc.Name = "lblPrecioMezc";
            this.lblPrecioMezc.Size = new System.Drawing.Size(46, 13);
            this.lblPrecioMezc.TabIndex = 7;
            this.lblPrecioMezc.Text = "$32,428";
            // 
            // comboBoxMezc
            // 
            this.comboBoxMezc.FormattingEnabled = true;
            this.comboBoxMezc.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxMezc.Location = new System.Drawing.Point(611, 200);
            this.comboBoxMezc.Name = "comboBoxMezc";
            this.comboBoxMezc.Size = new System.Drawing.Size(43, 21);
            this.comboBoxMezc.TabIndex = 8;
            // 
            // btnMezc
            // 
            this.btnMezc.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMezc.Location = new System.Drawing.Point(673, 198);
            this.btnMezc.Name = "btnMezc";
            this.btnMezc.Size = new System.Drawing.Size(75, 23);
            this.btnMezc.TabIndex = 9;
            this.btnMezc.Text = "Agregar";
            this.btnMezc.UseVisualStyleBackColor = true;
            // 
            // lblDescripcionMesa
            // 
            this.lblDescripcionMesa.AutoSize = true;
            this.lblDescripcionMesa.Location = new System.Drawing.Point(168, 297);
            this.lblDescripcionMesa.Name = "lblDescripcionMesa";
            this.lblDescripcionMesa.Size = new System.Drawing.Size(311, 13);
            this.lblDescripcionMesa.TabIndex = 11;
            this.lblDescripcionMesa.Text = "Tornamesa Profesional para DJ usb-Análogo AUDIO TECHNICA";
            // 
            // lblPrecioMesa
            // 
            this.lblPrecioMesa.AutoSize = true;
            this.lblPrecioMesa.Location = new System.Drawing.Point(168, 329);
            this.lblPrecioMesa.Name = "lblPrecioMesa";
            this.lblPrecioMesa.Size = new System.Drawing.Size(46, 13);
            this.lblPrecioMesa.TabIndex = 12;
            this.lblPrecioMesa.Text = "$11,089";
            // 
            // button1
            // 
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Location = new System.Drawing.Point(673, 308);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 13;
            this.button1.Text = "Agregar";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBox1.Location = new System.Drawing.Point(611, 310);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(44, 21);
            this.comboBox1.TabIndex = 14;
            // 
            // btnAPag1
            // 
            this.btnAPag1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAPag1.Location = new System.Drawing.Point(359, 412);
            this.btnAPag1.Name = "btnAPag1";
            this.btnAPag1.Size = new System.Drawing.Size(27, 23);
            this.btnAPag1.TabIndex = 15;
            this.btnAPag1.Text = "1";
            this.btnAPag1.UseVisualStyleBackColor = true;
            this.btnAPag1.Click += new System.EventHandler(this.btnAPag1_Click);
            // 
            // lblAPag2
            // 
            this.lblAPag2.AutoSize = true;
            this.lblAPag2.Location = new System.Drawing.Point(392, 417);
            this.lblAPag2.Name = "lblAPag2";
            this.lblAPag2.Size = new System.Drawing.Size(13, 13);
            this.lblAPag2.TabIndex = 16;
            this.lblAPag2.Text = "2";
            this.lblAPag2.Click += new System.EventHandler(this.lblAPag2_Click);
            // 
            // pictureBoxMesa
            // 
            this.pictureBoxMesa.Image = global::Comercializadora.Properties.Resources.at_lp1240_usb_xp_1_sq_2x;
            this.pictureBoxMesa.Location = new System.Drawing.Point(30, 259);
            this.pictureBoxMesa.Name = "pictureBoxMesa";
            this.pictureBoxMesa.Size = new System.Drawing.Size(102, 101);
            this.pictureBoxMesa.TabIndex = 10;
            this.pictureBoxMesa.TabStop = false;
            // 
            // pictureBoxMezc
            // 
            this.pictureBoxMezc.Image = global::Comercializadora.Properties.Resources.pioneer_xdjrx2_01;
            this.pictureBoxMezc.Location = new System.Drawing.Point(30, 152);
            this.pictureBoxMezc.Name = "pictureBoxMezc";
            this.pictureBoxMezc.Size = new System.Drawing.Size(102, 101);
            this.pictureBoxMezc.TabIndex = 5;
            this.pictureBoxMezc.TabStop = false;
            // 
            // pictureBoxAth
            // 
            this.pictureBoxAth.Image = global::Comercializadora.Properties.Resources.audiotechnica_ath_m20x_01;
            this.pictureBoxAth.Location = new System.Drawing.Point(30, 44);
            this.pictureBoxAth.Name = "pictureBoxAth";
            this.pictureBoxAth.Size = new System.Drawing.Size(102, 102);
            this.pictureBoxAth.TabIndex = 0;
            this.pictureBoxAth.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources._2569f;
            this.pictureBox1.Location = new System.Drawing.Point(721, 408);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(27, 27);
            this.pictureBox1.TabIndex = 17;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // AudioPag2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 465);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblAPag2);
            this.Controls.Add(this.btnAPag1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lblPrecioMesa);
            this.Controls.Add(this.lblDescripcionMesa);
            this.Controls.Add(this.pictureBoxMesa);
            this.Controls.Add(this.btnMezc);
            this.Controls.Add(this.comboBoxMezc);
            this.Controls.Add(this.lblPrecioMezc);
            this.Controls.Add(this.lblDescripcionMezc);
            this.Controls.Add(this.pictureBoxMezc);
            this.Controls.Add(this.btnAth);
            this.Controls.Add(this.comboBoxAth);
            this.Controls.Add(this.lblPrecioAth);
            this.Controls.Add(this.lblDescripcionAth);
            this.Controls.Add(this.pictureBoxAth);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AudioPag2";
            this.Text = "AudioPag2";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMesa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxMezc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxAth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxAth;
        private System.Windows.Forms.Label lblDescripcionAth;
        private System.Windows.Forms.Label lblPrecioAth;
        private System.Windows.Forms.ComboBox comboBoxAth;
        private System.Windows.Forms.Button btnAth;
        private System.Windows.Forms.PictureBox pictureBoxMezc;
        private System.Windows.Forms.Label lblDescripcionMezc;
        private System.Windows.Forms.Label lblPrecioMezc;
        private System.Windows.Forms.ComboBox comboBoxMezc;
        private System.Windows.Forms.Button btnMezc;
        private System.Windows.Forms.PictureBox pictureBoxMesa;
        private System.Windows.Forms.Label lblDescripcionMesa;
        private System.Windows.Forms.Label lblPrecioMesa;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button btnAPag1;
        private System.Windows.Forms.Label lblAPag2;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}