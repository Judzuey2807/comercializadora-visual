﻿namespace Comercializadora
{
    partial class MicroDaewoo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MicroDaewoo));
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ComprarMicoDaewoo3 = new System.Windows.Forms.Button();
            this.AgreagarMicoDaewoo3 = new System.Windows.Forms.Button();
            this.AgregarMicoDaewoo2 = new System.Windows.Forms.Button();
            this.ComprarMicoDaewoo2 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.AgregarMicoDaewoo1 = new System.Windows.Forms.Button();
            this.ComprarMicoDaewoo1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.BtRegresarMabe = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Comercializadora.Properties.Resources.MicroDaewoo3;
            this.pictureBox4.Location = new System.Drawing.Point(12, 523);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(250, 145);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Comercializadora.Properties.Resources.MicroDaewoo2;
            this.pictureBox3.Location = new System.Drawing.Point(13, 307);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(249, 145);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Comercializadora.Properties.Resources.MicroDaewoo1;
            this.pictureBox2.Location = new System.Drawing.Point(16, 108);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(246, 145);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources.Daewoo;
            this.pictureBox1.Location = new System.Drawing.Point(278, 25);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(212, 45);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // ComprarMicoDaewoo3
            // 
            this.ComprarMicoDaewoo3.Location = new System.Drawing.Point(283, 659);
            this.ComprarMicoDaewoo3.Name = "ComprarMicoDaewoo3";
            this.ComprarMicoDaewoo3.Size = new System.Drawing.Size(105, 23);
            this.ComprarMicoDaewoo3.TabIndex = 95;
            this.ComprarMicoDaewoo3.Text = "Comprar";
            this.ComprarMicoDaewoo3.UseVisualStyleBackColor = true;
            // 
            // AgreagarMicoDaewoo3
            // 
            this.AgreagarMicoDaewoo3.Location = new System.Drawing.Point(422, 659);
            this.AgreagarMicoDaewoo3.Name = "AgreagarMicoDaewoo3";
            this.AgreagarMicoDaewoo3.Size = new System.Drawing.Size(105, 23);
            this.AgreagarMicoDaewoo3.TabIndex = 94;
            this.AgreagarMicoDaewoo3.Text = "Agregar al carrito";
            this.AgreagarMicoDaewoo3.UseVisualStyleBackColor = true;
            // 
            // AgregarMicoDaewoo2
            // 
            this.AgregarMicoDaewoo2.Location = new System.Drawing.Point(422, 485);
            this.AgregarMicoDaewoo2.Name = "AgregarMicoDaewoo2";
            this.AgregarMicoDaewoo2.Size = new System.Drawing.Size(105, 23);
            this.AgregarMicoDaewoo2.TabIndex = 93;
            this.AgregarMicoDaewoo2.Text = "Agregar al carrito";
            this.AgregarMicoDaewoo2.UseVisualStyleBackColor = true;
            // 
            // ComprarMicoDaewoo2
            // 
            this.ComprarMicoDaewoo2.Location = new System.Drawing.Point(284, 485);
            this.ComprarMicoDaewoo2.Name = "ComprarMicoDaewoo2";
            this.ComprarMicoDaewoo2.Size = new System.Drawing.Size(105, 23);
            this.ComprarMicoDaewoo2.TabIndex = 92;
            this.ComprarMicoDaewoo2.Text = "Comprar";
            this.ComprarMicoDaewoo2.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(281, 605);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(269, 39);
            this.label12.TabIndex = 91;
            this.label12.Text = "- Función de descongelamiento por tiempo o por peso\r\n- 4 opciones de cocción rápi" +
    "do\r\n- Bloqueo de panel para evitar accidentes con los niños";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(281, 574);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 15);
            this.label11.TabIndex = 90;
            this.label11.Text = "$ 2,290.00";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(281, 545);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(102, 15);
            this.label10.TabIndex = 89;
            this.label10.Text = "Marca: DAEWOO";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(280, 523);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(509, 22);
            this.label9.TabIndex = 88;
            this.label9.Text = "Horno de Microondas Daewoo 1.1 Pies Cúbicos Acero";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(275, 373);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(211, 91);
            this.label8.TabIndex = 87;
            this.label8.Text = resources.GetString("label8.Text");
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(275, 346);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 15);
            this.label7.TabIndex = 86;
            this.label7.Text = "$ 1,199.00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(275, 320);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(102, 15);
            this.label6.TabIndex = 85;
            this.label6.Text = "Marca: DAEWOO";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label5.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(274, 298);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(469, 22);
            this.label5.TabIndex = 84;
            this.label5.Text = "Horno De Microondas 0.7 Pies Daewoo KOR-660S";
            // 
            // AgregarMicoDaewoo1
            // 
            this.AgregarMicoDaewoo1.Location = new System.Drawing.Point(422, 263);
            this.AgregarMicoDaewoo1.Name = "AgregarMicoDaewoo1";
            this.AgregarMicoDaewoo1.Size = new System.Drawing.Size(105, 23);
            this.AgregarMicoDaewoo1.TabIndex = 83;
            this.AgregarMicoDaewoo1.Text = "Agregar al carrito";
            this.AgregarMicoDaewoo1.UseVisualStyleBackColor = true;
            // 
            // ComprarMicoDaewoo1
            // 
            this.ComprarMicoDaewoo1.Location = new System.Drawing.Point(278, 263);
            this.ComprarMicoDaewoo1.Name = "ComprarMicoDaewoo1";
            this.ComprarMicoDaewoo1.Size = new System.Drawing.Size(110, 23);
            this.ComprarMicoDaewoo1.TabIndex = 82;
            this.ComprarMicoDaewoo1.Text = "Comprar";
            this.ComprarMicoDaewoo1.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(275, 166);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(199, 78);
            this.label4.TabIndex = 81;
            this.label4.Text = "Sistema de cóncavo de reflexión\r\nOperación fácil\r\nOpciones de Descongelado por al" +
    "imento\r\n5 Menús de auto cocción\r\n10 Niveles de potencia\r\nBotón de inicio rápido";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(275, 141);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 15);
            this.label3.TabIndex = 80;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(275, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 15);
            this.label2.TabIndex = 79;
            this.label2.Text = "Marca: DAEWOO";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(274, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(353, 22);
            this.label1.TabIndex = 78;
            this.label1.Text = "Horno Daewoo DAEWOO KOR-1N0AS";
            // 
            // BtRegresarMabe
            // 
            this.BtRegresarMabe.Image = global::Comercializadora.Properties.Resources.regresar3;
            this.BtRegresarMabe.Location = new System.Drawing.Point(0, 0);
            this.BtRegresarMabe.Name = "BtRegresarMabe";
            this.BtRegresarMabe.Size = new System.Drawing.Size(107, 82);
            this.BtRegresarMabe.TabIndex = 141;
            this.BtRegresarMabe.UseVisualStyleBackColor = true;
            this.BtRegresarMabe.Click += new System.EventHandler(this.BtRegresarMabe_Click);
            // 
            // MicroDaewoo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(799, 689);
            this.Controls.Add(this.BtRegresarMabe);
            this.Controls.Add(this.ComprarMicoDaewoo3);
            this.Controls.Add(this.AgreagarMicoDaewoo3);
            this.Controls.Add(this.AgregarMicoDaewoo2);
            this.Controls.Add(this.ComprarMicoDaewoo2);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.AgregarMicoDaewoo1);
            this.Controls.Add(this.ComprarMicoDaewoo1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "MicroDaewoo";
            this.Text = "MicroDaewoo";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Button ComprarMicoDaewoo3;
        private System.Windows.Forms.Button AgreagarMicoDaewoo3;
        private System.Windows.Forms.Button AgregarMicoDaewoo2;
        private System.Windows.Forms.Button ComprarMicoDaewoo2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button AgregarMicoDaewoo1;
        private System.Windows.Forms.Button ComprarMicoDaewoo1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtRegresarMabe;
    }
}