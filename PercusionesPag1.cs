﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comercializadora
{
    public partial class PercusionesPag1 : Form
    {
        public PercusionesPag1()
        {
            InitializeComponent();
        }

        private void PercusionesPag1_Load(object sender, EventArgs e)
        {

        }

        private void btnBPag2_Click(object sender, EventArgs e)
        {
            PercusionesPag2 p2 = new PercusionesPag2();
            p2.ShowDialog();
            this.Close();
        }

        private void btnBPag3_Click(object sender, EventArgs e)
        {
            PercusionesPag3 p3 = new PercusionesPag3();
            p3.ShowDialog();
            this.Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblBPag1_Click(object sender, EventArgs e)
        {
            PercusionesPag1 p1 = new PercusionesPag1();
            p1.ShowDialog();
            this.Close();
        }
    }
}
