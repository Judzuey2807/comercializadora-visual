﻿namespace Comercializadora
{
    partial class PercusionesPag1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDescripcionBLudwig = new System.Windows.Forms.Label();
            this.lblPrecioLudwig = new System.Windows.Forms.Label();
            this.comboBoxBLudwig = new System.Windows.Forms.ComboBox();
            this.btnBLudwig = new System.Windows.Forms.Button();
            this.btnBMapex = new System.Windows.Forms.Button();
            this.comboBoxBMapex = new System.Windows.Forms.ComboBox();
            this.lblDescripcionBMapex = new System.Windows.Forms.Label();
            this.lblPrecioBMapex = new System.Windows.Forms.Label();
            this.comboBoxBAlesis = new System.Windows.Forms.ComboBox();
            this.btnBAlesis = new System.Windows.Forms.Button();
            this.lblDescripcionBAlesis = new System.Windows.Forms.Label();
            this.lblPrecioBAlesis = new System.Windows.Forms.Label();
            this.lblBPag1 = new System.Windows.Forms.Label();
            this.btnBPag2 = new System.Windows.Forms.Button();
            this.btnBPag3 = new System.Windows.Forms.Button();
            this.pictureBoxBAlesis = new System.Windows.Forms.PictureBox();
            this.pictureBoxBMapex = new System.Windows.Forms.PictureBox();
            this.pictureBoxBLudwig = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBAlesis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBMapex)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBLudwig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDescripcionBLudwig
            // 
            this.lblDescripcionBLudwig.AutoSize = true;
            this.lblDescripcionBLudwig.Location = new System.Drawing.Point(168, 79);
            this.lblDescripcionBLudwig.Name = "lblDescripcionBLudwig";
            this.lblDescripcionBLudwig.Size = new System.Drawing.Size(236, 13);
            this.lblDescripcionBLudwig.TabIndex = 1;
            this.lblDescripcionBLudwig.Text = "Bateriá 5 piezas color plata Ludwig LC17515DIR";
            // 
            // lblPrecioLudwig
            // 
            this.lblPrecioLudwig.AutoSize = true;
            this.lblPrecioLudwig.Location = new System.Drawing.Point(168, 104);
            this.lblPrecioLudwig.Name = "lblPrecioLudwig";
            this.lblPrecioLudwig.Size = new System.Drawing.Size(40, 13);
            this.lblPrecioLudwig.TabIndex = 2;
            this.lblPrecioLudwig.Text = "$9,465";
            // 
            // comboBoxBLudwig
            // 
            this.comboBoxBLudwig.FormattingEnabled = true;
            this.comboBoxBLudwig.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxBLudwig.Location = new System.Drawing.Point(611, 94);
            this.comboBoxBLudwig.Name = "comboBoxBLudwig";
            this.comboBoxBLudwig.Size = new System.Drawing.Size(44, 21);
            this.comboBoxBLudwig.TabIndex = 3;
            // 
            // btnBLudwig
            // 
            this.btnBLudwig.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBLudwig.Location = new System.Drawing.Point(673, 92);
            this.btnBLudwig.Name = "btnBLudwig";
            this.btnBLudwig.Size = new System.Drawing.Size(75, 23);
            this.btnBLudwig.TabIndex = 4;
            this.btnBLudwig.Text = "Agregar";
            this.btnBLudwig.UseVisualStyleBackColor = true;
            // 
            // btnBMapex
            // 
            this.btnBMapex.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBMapex.Location = new System.Drawing.Point(673, 198);
            this.btnBMapex.Name = "btnBMapex";
            this.btnBMapex.Size = new System.Drawing.Size(75, 23);
            this.btnBMapex.TabIndex = 6;
            this.btnBMapex.Text = "Agregar";
            this.btnBMapex.UseVisualStyleBackColor = true;
            // 
            // comboBoxBMapex
            // 
            this.comboBoxBMapex.FormattingEnabled = true;
            this.comboBoxBMapex.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxBMapex.Location = new System.Drawing.Point(611, 200);
            this.comboBoxBMapex.Name = "comboBoxBMapex";
            this.comboBoxBMapex.Size = new System.Drawing.Size(44, 21);
            this.comboBoxBMapex.TabIndex = 7;
            // 
            // lblDescripcionBMapex
            // 
            this.lblDescripcionBMapex.AutoSize = true;
            this.lblDescripcionBMapex.Location = new System.Drawing.Point(168, 188);
            this.lblDescripcionBMapex.Name = "lblDescripcionBMapex";
            this.lblDescripcionBMapex.Size = new System.Drawing.Size(298, 13);
            this.lblDescripcionBMapex.TabIndex = 8;
            this.lblDescripcionBMapex.Text = "Batería de 5 piezas Mapex Armony AR529S-BTK Black dawn";
            // 
            // lblPrecioBMapex
            // 
            this.lblPrecioBMapex.AutoSize = true;
            this.lblPrecioBMapex.Location = new System.Drawing.Point(168, 214);
            this.lblPrecioBMapex.Name = "lblPrecioBMapex";
            this.lblPrecioBMapex.Size = new System.Drawing.Size(46, 13);
            this.lblPrecioBMapex.TabIndex = 9;
            this.lblPrecioBMapex.Text = "$27,355";
            // 
            // comboBoxBAlesis
            // 
            this.comboBoxBAlesis.FormattingEnabled = true;
            this.comboBoxBAlesis.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxBAlesis.Location = new System.Drawing.Point(611, 310);
            this.comboBoxBAlesis.Name = "comboBoxBAlesis";
            this.comboBoxBAlesis.Size = new System.Drawing.Size(44, 21);
            this.comboBoxBAlesis.TabIndex = 10;
            // 
            // btnBAlesis
            // 
            this.btnBAlesis.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBAlesis.Location = new System.Drawing.Point(673, 308);
            this.btnBAlesis.Name = "btnBAlesis";
            this.btnBAlesis.Size = new System.Drawing.Size(75, 23);
            this.btnBAlesis.TabIndex = 11;
            this.btnBAlesis.Text = "Agregar";
            this.btnBAlesis.UseVisualStyleBackColor = true;
            // 
            // lblDescripcionBAlesis
            // 
            this.lblDescripcionBAlesis.AutoSize = true;
            this.lblDescripcionBAlesis.Location = new System.Drawing.Point(168, 297);
            this.lblDescripcionBAlesis.Name = "lblDescripcionBAlesis";
            this.lblDescripcionBAlesis.Size = new System.Drawing.Size(251, 13);
            this.lblDescripcionBAlesis.TabIndex = 13;
            this.lblDescripcionBAlesis.Text = "Batería eléctrica 8 piezas Alesis Command Mesh Kit";
            // 
            // lblPrecioBAlesis
            // 
            this.lblPrecioBAlesis.AutoSize = true;
            this.lblPrecioBAlesis.Location = new System.Drawing.Point(168, 329);
            this.lblPrecioBAlesis.Name = "lblPrecioBAlesis";
            this.lblPrecioBAlesis.Size = new System.Drawing.Size(46, 13);
            this.lblPrecioBAlesis.TabIndex = 14;
            this.lblPrecioBAlesis.Text = "$19,260";
            // 
            // lblBPag1
            // 
            this.lblBPag1.AutoSize = true;
            this.lblBPag1.Location = new System.Drawing.Point(370, 417);
            this.lblBPag1.Name = "lblBPag1";
            this.lblBPag1.Size = new System.Drawing.Size(13, 13);
            this.lblBPag1.TabIndex = 15;
            this.lblBPag1.Text = "1";
            this.lblBPag1.Click += new System.EventHandler(this.lblBPag1_Click);
            // 
            // btnBPag2
            // 
            this.btnBPag2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBPag2.Location = new System.Drawing.Point(392, 412);
            this.btnBPag2.Name = "btnBPag2";
            this.btnBPag2.Size = new System.Drawing.Size(27, 23);
            this.btnBPag2.TabIndex = 16;
            this.btnBPag2.Text = "2";
            this.btnBPag2.UseVisualStyleBackColor = true;
            this.btnBPag2.Click += new System.EventHandler(this.btnBPag2_Click);
            // 
            // btnBPag3
            // 
            this.btnBPag3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBPag3.Location = new System.Drawing.Point(425, 412);
            this.btnBPag3.Name = "btnBPag3";
            this.btnBPag3.Size = new System.Drawing.Size(27, 23);
            this.btnBPag3.TabIndex = 17;
            this.btnBPag3.Text = "3";
            this.btnBPag3.UseVisualStyleBackColor = true;
            this.btnBPag3.Click += new System.EventHandler(this.btnBPag3_Click);
            // 
            // pictureBoxBAlesis
            // 
            this.pictureBoxBAlesis.Image = global::Comercializadora.Properties.Resources.alesis_command_mesh_kit_back;
            this.pictureBoxBAlesis.Location = new System.Drawing.Point(30, 259);
            this.pictureBoxBAlesis.Name = "pictureBoxBAlesis";
            this.pictureBoxBAlesis.Size = new System.Drawing.Size(102, 100);
            this.pictureBoxBAlesis.TabIndex = 12;
            this.pictureBoxBAlesis.TabStop = false;
            // 
            // pictureBoxBMapex
            // 
            this.pictureBoxBMapex.Image = global::Comercializadora.Properties.Resources.bateria_de_5_piezas_rock_shell_pack_mapex_ar529s_hb_btk_1;
            this.pictureBoxBMapex.Location = new System.Drawing.Point(30, 151);
            this.pictureBoxBMapex.Name = "pictureBoxBMapex";
            this.pictureBoxBMapex.Size = new System.Drawing.Size(102, 102);
            this.pictureBoxBMapex.TabIndex = 5;
            this.pictureBoxBMapex.TabStop = false;
            // 
            // pictureBoxBLudwig
            // 
            this.pictureBoxBLudwig.Image = global::Comercializadora.Properties.Resources.lc17515dir;
            this.pictureBoxBLudwig.Location = new System.Drawing.Point(30, 44);
            this.pictureBoxBLudwig.Name = "pictureBoxBLudwig";
            this.pictureBoxBLudwig.Size = new System.Drawing.Size(102, 101);
            this.pictureBoxBLudwig.TabIndex = 0;
            this.pictureBoxBLudwig.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources._2569f;
            this.pictureBox1.Location = new System.Drawing.Point(721, 403);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(27, 27);
            this.pictureBox1.TabIndex = 18;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // PercusionesPag1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 465);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnBPag3);
            this.Controls.Add(this.btnBPag2);
            this.Controls.Add(this.lblBPag1);
            this.Controls.Add(this.lblPrecioBAlesis);
            this.Controls.Add(this.lblDescripcionBAlesis);
            this.Controls.Add(this.pictureBoxBAlesis);
            this.Controls.Add(this.btnBAlesis);
            this.Controls.Add(this.comboBoxBAlesis);
            this.Controls.Add(this.lblPrecioBMapex);
            this.Controls.Add(this.lblDescripcionBMapex);
            this.Controls.Add(this.comboBoxBMapex);
            this.Controls.Add(this.btnBMapex);
            this.Controls.Add(this.pictureBoxBMapex);
            this.Controls.Add(this.btnBLudwig);
            this.Controls.Add(this.comboBoxBLudwig);
            this.Controls.Add(this.lblPrecioLudwig);
            this.Controls.Add(this.lblDescripcionBLudwig);
            this.Controls.Add(this.pictureBoxBLudwig);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "PercusionesPag1";
            this.Text = "PercusionesPag1";
            this.Load += new System.EventHandler(this.PercusionesPag1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBAlesis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBMapex)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBLudwig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxBLudwig;
        private System.Windows.Forms.Label lblDescripcionBLudwig;
        private System.Windows.Forms.Label lblPrecioLudwig;
        private System.Windows.Forms.ComboBox comboBoxBLudwig;
        private System.Windows.Forms.Button btnBLudwig;
        private System.Windows.Forms.PictureBox pictureBoxBMapex;
        private System.Windows.Forms.Button btnBMapex;
        private System.Windows.Forms.ComboBox comboBoxBMapex;
        private System.Windows.Forms.Label lblDescripcionBMapex;
        private System.Windows.Forms.Label lblPrecioBMapex;
        private System.Windows.Forms.ComboBox comboBoxBAlesis;
        private System.Windows.Forms.Button btnBAlesis;
        private System.Windows.Forms.PictureBox pictureBoxBAlesis;
        private System.Windows.Forms.Label lblDescripcionBAlesis;
        private System.Windows.Forms.Label lblPrecioBAlesis;
        private System.Windows.Forms.Label lblBPag1;
        private System.Windows.Forms.Button btnBPag2;
        private System.Windows.Forms.Button btnBPag3;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}