﻿namespace Comercializadora
{
    partial class GuitarrasYBajosPag2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblDescripcionFenderAcu = new System.Windows.Forms.Label();
            this.lblPrecioFenderAcu = new System.Windows.Forms.Label();
            this.comboBoxFenderAcu = new System.Windows.Forms.ComboBox();
            this.btnAgregarFenderAcu = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblDescripcionBFender = new System.Windows.Forms.Label();
            this.lblPrecioBFender = new System.Windows.Forms.Label();
            this.lblDescripcionBIbanez = new System.Windows.Forms.Label();
            this.lblPrecioBIbanez = new System.Windows.Forms.Label();
            this.comboBoxBFender = new System.Windows.Forms.ComboBox();
            this.comboBoxBIbanez = new System.Windows.Forms.ComboBox();
            this.btnAgregarBFEnder = new System.Windows.Forms.Button();
            this.btnAgregarBIbanez = new System.Windows.Forms.Button();
            this.btnPag1 = new System.Windows.Forms.Button();
            this.lblPag2 = new System.Windows.Forms.Label();
            this.btnPag3 = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // lblDescripcionFenderAcu
            // 
            this.lblDescripcionFenderAcu.AutoSize = true;
            this.lblDescripcionFenderAcu.Location = new System.Drawing.Point(168, 79);
            this.lblDescripcionFenderAcu.Name = "lblDescripcionFenderAcu";
            this.lblDescripcionFenderAcu.Size = new System.Drawing.Size(254, 13);
            this.lblDescripcionFenderAcu.TabIndex = 1;
            this.lblDescripcionFenderAcu.Text = "Guitarra Acústica Fender CD-60S Left-Hand, Natural";
            // 
            // lblPrecioFenderAcu
            // 
            this.lblPrecioFenderAcu.AutoSize = true;
            this.lblPrecioFenderAcu.Location = new System.Drawing.Point(168, 104);
            this.lblPrecioFenderAcu.Name = "lblPrecioFenderAcu";
            this.lblPrecioFenderAcu.Size = new System.Drawing.Size(40, 13);
            this.lblPrecioFenderAcu.TabIndex = 2;
            this.lblPrecioFenderAcu.Text = "$3,216";
            // 
            // comboBoxFenderAcu
            // 
            this.comboBoxFenderAcu.FormattingEnabled = true;
            this.comboBoxFenderAcu.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxFenderAcu.Location = new System.Drawing.Point(611, 94);
            this.comboBoxFenderAcu.Name = "comboBoxFenderAcu";
            this.comboBoxFenderAcu.Size = new System.Drawing.Size(44, 21);
            this.comboBoxFenderAcu.TabIndex = 3;
            // 
            // btnAgregarFenderAcu
            // 
            this.btnAgregarFenderAcu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAgregarFenderAcu.Location = new System.Drawing.Point(673, 92);
            this.btnAgregarFenderAcu.Name = "btnAgregarFenderAcu";
            this.btnAgregarFenderAcu.Size = new System.Drawing.Size(75, 23);
            this.btnAgregarFenderAcu.TabIndex = 4;
            this.btnAgregarFenderAcu.Text = "Agregar";
            this.btnAgregarFenderAcu.UseVisualStyleBackColor = true;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Comercializadora.Properties.Resources.bajoIbanez;
            this.pictureBox3.Location = new System.Drawing.Point(30, 262);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(104, 104);
            this.pictureBox3.TabIndex = 6;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Comercializadora.Properties.Resources.bajofender;
            this.pictureBox2.Location = new System.Drawing.Point(30, 153);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(103, 103);
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources.AcuFender1;
            this.pictureBox1.Location = new System.Drawing.Point(30, 44);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(103, 103);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // lblDescripcionBFender
            // 
            this.lblDescripcionBFender.AutoSize = true;
            this.lblDescripcionBFender.Location = new System.Drawing.Point(168, 188);
            this.lblDescripcionBFender.Name = "lblDescripcionBFender";
            this.lblDescripcionBFender.Size = new System.Drawing.Size(411, 13);
            this.lblDescripcionBFender.TabIndex = 7;
            this.lblDescripcionBFender.Text = "Bajo Eléctrico Fender Vintera \'70s Jazz Bass, Pau Ferro Fingerboard, 3-Color Sunb" +
    "urst";
            this.lblDescripcionBFender.Click += new System.EventHandler(this.lblDescripcionBFender_Click);
            // 
            // lblPrecioBFender
            // 
            this.lblPrecioBFender.AutoSize = true;
            this.lblPrecioBFender.Location = new System.Drawing.Point(168, 214);
            this.lblPrecioBFender.Name = "lblPrecioBFender";
            this.lblPrecioBFender.Size = new System.Drawing.Size(46, 13);
            this.lblPrecioBFender.TabIndex = 8;
            this.lblPrecioBFender.Text = "$21,435";
            // 
            // lblDescripcionBIbanez
            // 
            this.lblDescripcionBIbanez.AutoSize = true;
            this.lblDescripcionBIbanez.Location = new System.Drawing.Point(168, 297);
            this.lblDescripcionBIbanez.Name = "lblDescripcionBIbanez";
            this.lblDescripcionBIbanez.Size = new System.Drawing.Size(314, 13);
            this.lblDescripcionBIbanez.TabIndex = 9;
            this.lblDescripcionBIbanez.Text = "Bajo Eléctrico Ibanez Electrico Sr De 5 Cuerdas Café Sombreado";
            // 
            // lblPrecioBIbanez
            // 
            this.lblPrecioBIbanez.AutoSize = true;
            this.lblPrecioBIbanez.Location = new System.Drawing.Point(168, 329);
            this.lblPrecioBIbanez.Name = "lblPrecioBIbanez";
            this.lblPrecioBIbanez.Size = new System.Drawing.Size(40, 13);
            this.lblPrecioBIbanez.TabIndex = 10;
            this.lblPrecioBIbanez.Text = "$9,860";
            // 
            // comboBoxBFender
            // 
            this.comboBoxBFender.FormattingEnabled = true;
            this.comboBoxBFender.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxBFender.Location = new System.Drawing.Point(611, 200);
            this.comboBoxBFender.Name = "comboBoxBFender";
            this.comboBoxBFender.Size = new System.Drawing.Size(44, 21);
            this.comboBoxBFender.TabIndex = 11;
            // 
            // comboBoxBIbanez
            // 
            this.comboBoxBIbanez.FormattingEnabled = true;
            this.comboBoxBIbanez.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"});
            this.comboBoxBIbanez.Location = new System.Drawing.Point(611, 310);
            this.comboBoxBIbanez.Name = "comboBoxBIbanez";
            this.comboBoxBIbanez.Size = new System.Drawing.Size(44, 21);
            this.comboBoxBIbanez.TabIndex = 12;
            // 
            // btnAgregarBFEnder
            // 
            this.btnAgregarBFEnder.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAgregarBFEnder.Location = new System.Drawing.Point(673, 198);
            this.btnAgregarBFEnder.Name = "btnAgregarBFEnder";
            this.btnAgregarBFEnder.Size = new System.Drawing.Size(75, 23);
            this.btnAgregarBFEnder.TabIndex = 13;
            this.btnAgregarBFEnder.Text = "Agregar";
            this.btnAgregarBFEnder.UseVisualStyleBackColor = true;
            // 
            // btnAgregarBIbanez
            // 
            this.btnAgregarBIbanez.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAgregarBIbanez.Location = new System.Drawing.Point(673, 308);
            this.btnAgregarBIbanez.Name = "btnAgregarBIbanez";
            this.btnAgregarBIbanez.Size = new System.Drawing.Size(75, 23);
            this.btnAgregarBIbanez.TabIndex = 14;
            this.btnAgregarBIbanez.Text = "Agregar";
            this.btnAgregarBIbanez.UseVisualStyleBackColor = true;
            // 
            // btnPag1
            // 
            this.btnPag1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPag1.Location = new System.Drawing.Point(359, 412);
            this.btnPag1.Name = "btnPag1";
            this.btnPag1.Size = new System.Drawing.Size(27, 23);
            this.btnPag1.TabIndex = 15;
            this.btnPag1.Text = "1";
            this.btnPag1.UseVisualStyleBackColor = true;
            this.btnPag1.Click += new System.EventHandler(this.btnPag1_Click);
            // 
            // lblPag2
            // 
            this.lblPag2.AutoSize = true;
            this.lblPag2.Location = new System.Drawing.Point(392, 417);
            this.lblPag2.Name = "lblPag2";
            this.lblPag2.Size = new System.Drawing.Size(13, 13);
            this.lblPag2.TabIndex = 16;
            this.lblPag2.Text = "2";
            this.lblPag2.Click += new System.EventHandler(this.lblPag2_Click);
            // 
            // btnPag3
            // 
            this.btnPag3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPag3.Location = new System.Drawing.Point(411, 412);
            this.btnPag3.Name = "btnPag3";
            this.btnPag3.Size = new System.Drawing.Size(27, 23);
            this.btnPag3.TabIndex = 17;
            this.btnPag3.Text = "3";
            this.btnPag3.UseVisualStyleBackColor = true;
            this.btnPag3.Click += new System.EventHandler(this.btnPag3_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox4.Image = global::Comercializadora.Properties.Resources._2569f;
            this.pictureBox4.Location = new System.Drawing.Point(720, 407);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(28, 28);
            this.pictureBox4.TabIndex = 18;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // GuitarrasYBajosPag2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 465);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.btnPag3);
            this.Controls.Add(this.lblPag2);
            this.Controls.Add(this.btnPag1);
            this.Controls.Add(this.btnAgregarBIbanez);
            this.Controls.Add(this.btnAgregarBFEnder);
            this.Controls.Add(this.comboBoxBIbanez);
            this.Controls.Add(this.comboBoxBFender);
            this.Controls.Add(this.lblPrecioBIbanez);
            this.Controls.Add(this.lblDescripcionBIbanez);
            this.Controls.Add(this.lblPrecioBFender);
            this.Controls.Add(this.lblDescripcionBFender);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.btnAgregarFenderAcu);
            this.Controls.Add(this.comboBoxFenderAcu);
            this.Controls.Add(this.lblPrecioFenderAcu);
            this.Controls.Add(this.lblDescripcionFenderAcu);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "GuitarrasYBajosPag2";
            this.Text = "GuitarrasYBajosPag2";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblDescripcionFenderAcu;
        private System.Windows.Forms.Label lblPrecioFenderAcu;
        private System.Windows.Forms.ComboBox comboBoxFenderAcu;
        private System.Windows.Forms.Button btnAgregarFenderAcu;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label lblDescripcionBFender;
        private System.Windows.Forms.Label lblPrecioBFender;
        private System.Windows.Forms.Label lblDescripcionBIbanez;
        private System.Windows.Forms.Label lblPrecioBIbanez;
        private System.Windows.Forms.ComboBox comboBoxBFender;
        private System.Windows.Forms.ComboBox comboBoxBIbanez;
        private System.Windows.Forms.Button btnAgregarBFEnder;
        private System.Windows.Forms.Button btnAgregarBIbanez;
        private System.Windows.Forms.Button btnPag1;
        private System.Windows.Forms.Label lblPag2;
        private System.Windows.Forms.Button btnPag3;
        private System.Windows.Forms.PictureBox pictureBox4;
    }
}