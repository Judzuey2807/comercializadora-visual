﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comercializadora
{
    public partial class GuitarrasYBajosPag2 : Form
    {
        public GuitarrasYBajosPag2()
        {
            InitializeComponent();
        }

        private void lblDescripcionBFender_Click(object sender, EventArgs e)
        {

        }

        private void btnPag1_Click(object sender, EventArgs e)
        {
            GuitarrasYBajosPag1 gbp1 = new GuitarrasYBajosPag1();
            gbp1.ShowDialog();
            this.Close();
        }

        private void btnPag3_Click(object sender, EventArgs e)
        {
            GuitarrasYBajosPag3 gbp3 = new GuitarrasYBajosPag3();
            gbp3.ShowDialog();
            this.Close();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Categorias c = new Categorias();
            c.ShowDialog();
            this.Close();
        }

        private void lblPag2_Click(object sender, EventArgs e)
        {
            GuitarrasYBajosPag2 gbp2 = new GuitarrasYBajosPag2();
            gbp2.ShowDialog();
            this.Close();
        }
    }
}
