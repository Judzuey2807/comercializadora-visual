﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comercializadora
{
    public partial class GuitarrasYBajosPag1 : Form
    {
        public GuitarrasYBajosPag1()
        {
            InitializeComponent();
        }

        private void btnPag2_Click(object sender, EventArgs e)
        {
            GuitarrasYBajosPag2 gbp2 = new GuitarrasYBajosPag2();
            gbp2.ShowDialog();
            this.Close();
        }

        private void btnPag3_Click(object sender, EventArgs e)
        {
            GuitarrasYBajosPag3 gbp3 = new GuitarrasYBajosPag3();
            gbp3.ShowDialog();
            this.Close();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Categorias c = new Categorias();
            c.ShowDialog();
            this.Close();
        }

        private void btnPag1_Click(object sender, EventArgs e)
        {
            GuitarrasYBajosPag1 gb1 = new GuitarrasYBajosPag1();
            gb1.ShowDialog();
            this.Close();
        }
    }
}
