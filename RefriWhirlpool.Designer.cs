﻿namespace Comercializadora
{
    partial class RefriWhirlpool
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RefriWhirlpool));
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ComprarRefriWhirlpool1 = new System.Windows.Forms.Button();
            this.AgregarRefriWhirlpool1 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.ComprarRefriWhirlpool2 = new System.Windows.Forms.Button();
            this.AgregarRefriWhirlpool2 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.ComprarRefriWhirlpool3 = new System.Windows.Forms.Button();
            this.AgregarRefriWhirlpool3 = new System.Windows.Forms.Button();
            this.BtRegresarMabe = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Comercializadora.Properties.Resources.Refriwhirlpool3;
            this.pictureBox4.Location = new System.Drawing.Point(24, 611);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(117, 194);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Comercializadora.Properties.Resources.RefriWhirlpool2;
            this.pictureBox3.Location = new System.Drawing.Point(24, 384);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(117, 203);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Comercializadora.Properties.Resources.RefriWhilpoll1;
            this.pictureBox2.Location = new System.Drawing.Point(24, 153);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(117, 212);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Comercializadora.Properties.Resources.whirlpool;
            this.pictureBox1.Location = new System.Drawing.Point(272, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(236, 104);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(198, 153);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(467, 44);
            this.label1.TabIndex = 4;
            this.label1.Text = "Refrigerador 14 Pies Whirlpool con Despachador \r\nAcero inox anti huellas";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(202, 211);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "Marca: WHIRLPOOL";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(202, 237);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "$ 7,909.00";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(199, 272);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(328, 39);
            this.label4.TabIndex = 7;
            this.label4.Text = "Superficie Steel Pro Antifingerprint\r\nDispensador de agua con filtro incluido y c" +
    "apacidad de hasta 4 litros\r\nCuenta con puerta reversible y luz LED para una mayo" +
    "r visibilidad";
            // 
            // ComprarRefriWhirlpool1
            // 
            this.ComprarRefriWhirlpool1.Location = new System.Drawing.Point(202, 342);
            this.ComprarRefriWhirlpool1.Name = "ComprarRefriWhirlpool1";
            this.ComprarRefriWhirlpool1.Size = new System.Drawing.Size(98, 23);
            this.ComprarRefriWhirlpool1.TabIndex = 8;
            this.ComprarRefriWhirlpool1.Text = "Comprar";
            this.ComprarRefriWhirlpool1.UseVisualStyleBackColor = true;
            // 
            // AgregarRefriWhirlpool1
            // 
            this.AgregarRefriWhirlpool1.Location = new System.Drawing.Point(320, 342);
            this.AgregarRefriWhirlpool1.Name = "AgregarRefriWhirlpool1";
            this.AgregarRefriWhirlpool1.Size = new System.Drawing.Size(98, 23);
            this.AgregarRefriWhirlpool1.TabIndex = 9;
            this.AgregarRefriWhirlpool1.Text = "Agregar al carrito";
            this.AgregarRefriWhirlpool1.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(198, 384);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(456, 44);
            this.label5.TabIndex = 10;
            this.label5.Text = "Refrigerador 9 Pies Whirlpool con Despachador \r\nAcero Inox";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(202, 441);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 15);
            this.label6.TabIndex = 11;
            this.label6.Text = "Marca: WHIRLPOOL";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(202, 469);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 15);
            this.label7.TabIndex = 12;
            this.label7.Text = "$ 6,290.00";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(199, 494);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(248, 39);
            this.label8.TabIndex = 13;
            this.label8.Text = "Recomendable para 1-2 personas\r\nMedidas fuera del paquete 168 x 56 x 67\r\nDespacha" +
    "dor de agua con capacidad de 4.2 Litros";
            // 
            // ComprarRefriWhirlpool2
            // 
            this.ComprarRefriWhirlpool2.Location = new System.Drawing.Point(202, 563);
            this.ComprarRefriWhirlpool2.Name = "ComprarRefriWhirlpool2";
            this.ComprarRefriWhirlpool2.Size = new System.Drawing.Size(98, 23);
            this.ComprarRefriWhirlpool2.TabIndex = 14;
            this.ComprarRefriWhirlpool2.Text = "Comprar";
            this.ComprarRefriWhirlpool2.UseVisualStyleBackColor = true;
            // 
            // AgregarRefriWhirlpool2
            // 
            this.AgregarRefriWhirlpool2.Location = new System.Drawing.Point(320, 562);
            this.AgregarRefriWhirlpool2.Name = "AgregarRefriWhirlpool2";
            this.AgregarRefriWhirlpool2.Size = new System.Drawing.Size(98, 23);
            this.AgregarRefriWhirlpool2.TabIndex = 15;
            this.AgregarRefriWhirlpool2.Text = "Agregar al carrito";
            this.AgregarRefriWhirlpool2.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold);
            this.label9.Location = new System.Drawing.Point(202, 611);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(338, 22);
            this.label9.TabIndex = 16;
            this.label9.Text = "WHIRLPOOL REFRIGERADOR 18 P3";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label10.Location = new System.Drawing.Point(202, 649);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(120, 15);
            this.label10.TabIndex = 17;
            this.label10.Text = "Marca: WHIRLPOOL";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(202, 677);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 15);
            this.label11.TabIndex = 18;
            this.label11.Text = "$ 12,746.00";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(202, 705);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(432, 52);
            this.label12.TabIndex = 19;
            this.label12.Text = resources.GetString("label12.Text");
            // 
            // ComprarRefriWhirlpool3
            // 
            this.ComprarRefriWhirlpool3.Location = new System.Drawing.Point(202, 781);
            this.ComprarRefriWhirlpool3.Name = "ComprarRefriWhirlpool3";
            this.ComprarRefriWhirlpool3.Size = new System.Drawing.Size(98, 23);
            this.ComprarRefriWhirlpool3.TabIndex = 20;
            this.ComprarRefriWhirlpool3.Text = "Comprar";
            this.ComprarRefriWhirlpool3.UseVisualStyleBackColor = true;
            // 
            // AgregarRefriWhirlpool3
            // 
            this.AgregarRefriWhirlpool3.Location = new System.Drawing.Point(320, 781);
            this.AgregarRefriWhirlpool3.Name = "AgregarRefriWhirlpool3";
            this.AgregarRefriWhirlpool3.Size = new System.Drawing.Size(98, 23);
            this.AgregarRefriWhirlpool3.TabIndex = 21;
            this.AgregarRefriWhirlpool3.Text = "Agregar al carrito";
            this.AgregarRefriWhirlpool3.UseVisualStyleBackColor = true;
            // 
            // BtRegresarMabe
            // 
            this.BtRegresarMabe.Image = global::Comercializadora.Properties.Resources.regresar3;
            this.BtRegresarMabe.Location = new System.Drawing.Point(0, 1);
            this.BtRegresarMabe.Name = "BtRegresarMabe";
            this.BtRegresarMabe.Size = new System.Drawing.Size(107, 82);
            this.BtRegresarMabe.TabIndex = 142;
            this.BtRegresarMabe.UseVisualStyleBackColor = true;
            this.BtRegresarMabe.Click += new System.EventHandler(this.BtRegresarMabe_Click);
            // 
            // RefriWhirlpool
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(800, 839);
            this.Controls.Add(this.BtRegresarMabe);
            this.Controls.Add(this.AgregarRefriWhirlpool3);
            this.Controls.Add(this.ComprarRefriWhirlpool3);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.AgregarRefriWhirlpool2);
            this.Controls.Add(this.ComprarRefriWhirlpool2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.AgregarRefriWhirlpool1);
            this.Controls.Add(this.ComprarRefriWhirlpool1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Name = "RefriWhirlpool";
            this.Text = "RefriWhirlpool";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button ComprarRefriWhirlpool1;
        private System.Windows.Forms.Button AgregarRefriWhirlpool1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button ComprarRefriWhirlpool2;
        private System.Windows.Forms.Button AgregarRefriWhirlpool2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button ComprarRefriWhirlpool3;
        private System.Windows.Forms.Button AgregarRefriWhirlpool3;
        private System.Windows.Forms.Button BtRegresarMabe;
    }
}