﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Comercializadora
{
    public partial class XboxC4 : Form
    {
        public XboxC4()
        {
            InitializeComponent();
        }

        private void label9_Click(object sender, EventArgs e)
        {
            XboxC3 x = new XboxC3();
            x.Show();
            this.Close();
        }

        private void label8_Click(object sender, EventArgs e)
        {
            XboxC2 x = new XboxC2();
            x.Show();
            this.Close();
        }

        private void label7_Click(object sender, EventArgs e)
        {
            Xbox x = new Xbox();
            x.Show();
            this.Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Games x = new Games();
            x.Show();
            this.Close();
        }

        private void label10_Click(object sender, EventArgs e)
        {
            XboxC5 x = new XboxC5();
            x.Show();
            this.Close();
        }
    }
}
